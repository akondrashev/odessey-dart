/*********************************************************************
 *
 *        Lantronix XPort-Pro (UART to Ethernet) module
 *
 *********************************************************************/
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <gen.h>
#include <rs422.h>
#include <rf.h>
#include <cnf.h>
#include <tlog.h>
#include <timers.h>
#include <status.h>
#include <ability.h>
#include <fbd.h>
#include <sound.h>
#include <display.h>
#include <menu.h>
#include <magcard.h>
#include <misc.h>
#include <mag.h>
#include <magcard.h>
#include <rtc.h>
#include <fare.h>
#include <probe.h>
#include <util.h>
#include <trim.h>
#include <transact.h>
#include "led.h"
#include "j1708.h"

int RFNGetc(char *lpBuf, int bufSize) ;

int  ETH_STATUS = ETH_STATUS_POWER ;          /* Ethernet controller status.  See RF.H for values  */

int  RF_Debug = 0 ;
int  RF_Display = 0 ;
byte MAC_address[8] ;
byte RF_Software_Ver[20] ;
int  RFBaud ;

#define  NextRx(fptr)      (RxState = (fptr))

static void RxIdle(byte c) ;
static void RxWaitPrompt1(byte c) ;

static void (*RxState)(byte) = NULL ;
static int  count = 0 ;

static const char *xml[] = {
"<?xml version=\"1.0\" standalone=\"yes\"?><configrecord>",
"<configgroup name=\"xml import control\">",
   "<configitem name=\"restore factory configuration\"><value>enable</value></configitem>",
   "<configitem name=\"delete cpm groups\"><value>enable</value></configitem>",
   "<configitem name=\"reboot\"><value>enable</value></configitem>",
"</configgroup>",
"<configgroup name=\"ethernet\" instance=\"eth0\">",
   "<configitem name=\"speed\"><value>Auto</value></configitem>",
   "<configitem name=\"duplex\"><value>Auto</value></configitem>",
"</configgroup>",
"<configgroup name=\"interface\" instance=\"eth0\">",
   "<configitem name=\"bootp\"><value>disable</value></configitem>",
   "<configitem name=\"dhcp\"><value>disable</value></configitem>",
   "<configitem name=\"ip address\"><value>%u.%u.%u.%u/%u</value></configitem>",
   "<configitem name=\"default gateway\"><value>%u.%u.%u.%u</value></configitem>",
   "<configitem name=\"primary dns\"><value>%s</value></configitem>",
"</configgroup>",
"<configgroup name=\"line\" instance=\"1\">",
   "<configitem name=\"interface\"><value>RS232</value></configitem>",
   "<configitem name=\"state\"><value>enable</value></configitem>",
   "<configitem name=\"protocol\"><value>Tunnel</value></configitem>",
   "<configitem name=\"baud rate\"><value>115200 bits per second</value></configitem>",
   "<configitem name=\"parity\"><value>None</value></configitem>",
   "<configitem name=\"data bits\"><value>8</value></configitem>",
   "<configitem name=\"stop bits\"><value>1</value></configitem>",
   "<configitem name=\"flow control\"><value>None</value></configitem>",
"</configgroup>",
"<configgroup name=\"tunnel connect\" instance=\"1\">",
   "<configitem name=\"connect mode\"><value>Any Character</value></configitem>",
   "<configitem name=\"local port\"><value>%u</value></configitem>",
   "<configitem name=\"host\" instance=\"1\">",
      "<value name=\"address\">%s</value>",
      "<value name=\"port\">%u</value>",
      "<value name=\"protocol\">UDP</value>",
   "</configitem>",
"</configgroup>",
"<configgroup name=\"tunnel accept\" instance=\"1\">",
   "<configitem name=\"accept mode\"><value>Disable</value></configitem>",
"</configgroup>",
"<configgroup name=\"tunnel packing\" instance=\"1\">",
   "<configitem name=\"packing mode\"><value>Send Character</value></configitem>",
   "<configitem name=\"send character\"><value>&#60;control&#62;M</value></configitem>",
   "<configitem name=\"trailing character\"><value>&#60;None&#62;</value></configitem>",
"</configgroup>",
"</configrecord>\r\n"
} ;


/*****************************************************************
 *
 *             Executed at start-up to wake-up LAN
 *
 *****************************************************************/
void DARTStart(void)
   {
   static const DART_UDP msg = {DART_SIGNATURE, STATUS_POWER_ON, 0, (~STATUS_POWER_ON)|0x80, 0xD} ;

   if (count)
      ETH_STATUS = ETH_STATUS_OK ;
   else
      {
      if (RF_Debug) printf("STATUS_POWER_ON\r\n") ;
      RFNputs((byte*)&msg, (int)sizeof(DART_UDP)) ;
      Init_Tmr_Fn(WI_PORT_OFF_TMR, DARTStart, 5000) ;
      count++ ;
      } ;
   }


#define  MAX_UDP_MESSAGES        10
/*****************************************************************
 *
 *            Sends UDP message to the target
 *     Argument: status flags to send
 *     Returns 0, if successful
 *
 *****************************************************************/
int DARTSendMessage(int code)
   {
   static DART_UDP msg[MAX_UDP_MESSAGES] ;
   static int msgCount = 0 ;
   register DART_UDP *lpMsg ;
   register int i ;

   if (!OF6_RF_PORT) return 1 ;
   if (msgCount == MAX_UDP_MESSAGES) return 2 ;
   lpMsg = &msg[msgCount++] ;
   lpMsg->signature = DART_SIGNATURE ;
   lpMsg->code = (byte)code | 0x80 ;
   lpMsg->unused = 0 ;
   lpMsg->crc = (~lpMsg->code)| 0x80 ;
   lpMsg->padding = 0xD ;
   if (RF_Debug)
      {
      register byte *lpb ;

      printf("Message %u (%02X):\r\n", code, lpMsg->code) ;
      for (i = 0, lpb = (byte*)lpMsg; i < (int)sizeof(DART_UDP); i++, lpb++) printf("%02X ", *lpb) ;
      printf("\r\n") ;
      } ;
   if (ETH_STATUS != ETH_STATUS_OK) return 3 ;
   for (i = 0; i < msgCount; i++) RFNputs((byte*)&msg[i], (int)sizeof(DART_UDP)) ;
   msgCount = 0 ;
   return 0 ;
   }



void InitializeRFProtocol(void)
   {
   return ;
   }

int Check_RF_Baud(byte baud)
   {
   return 1 ;
   }


void KillWIPort(void)
   {
   printf("Kill WiPort\r\n") ;
   OCTALB_RTS_ON() ;
   ETH_STATUS = ETH_STATUS_POWER ;
   }


/* Transmit '!' on xPort UART until we get '!' in response*/
static void RF_EnterPrompt(void)
   {
   if (--count == 0)
      {
      printf("xPort: No response for '!'\r\n") ;
      OCTALB_RTS_ON() ;
      NextRx(NULL) ;
      }
   else
      {
      RFPutc('!') ;
      Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_EnterPrompt, 25) ;
      } ;
   }

/* Exit from power down */
static void RF_PowerOn(void)
   {
   /* Turn power up on xPort */
   OCTALB_RTS_OFF() ;
   if (RF_Debug) printf("XPort power on\r\n") ;
   /* Maximum number of '!' to send */
   count = 12000 / 25 ;
   /* Wait for '!' */
   NextRx(RxWaitPrompt1) ;
   /* Time delay */
   Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_EnterPrompt, 1000) ;
   ETH_STATUS = ETH_STATUS_NO_PROMPT ;
   }


/************************************************************************************
 *
 *  This is main initialization function.  It is called from Prologue() (in main.c).
 *  We try to enter controller's configuration mode by switching power OFF and ON.
 *  After that we upload configuration XML and switch into command waiting mode, so
 *  FBX could send packets out.
 *
 ************************************************************************************/
void Fetch_MAC_address(void)
   {
   word  crc ;

   RF_Debug = 0 ;
   Set_Aflag(A_RF_MAC) ;

#if 1
   crc = crc16((char*)&wiport, sizeof(WIPORT)-2) ;
   swab((char*)&crc, (char*)&crc, 2) ;
   if (crc != wiport.crc)
      {
      printf("WIPORT CRC %X is not valid (%X)\r\n", wiport.crc, crc) ;
      ETH_STATUS = ETH_STATUS_BAD_CONFIG ;
      return ;
      } ;
   if (Tst_Sflag(S_BURNIN))
      {
      printf("Network Configuration:\n") ;
      printf("IP:        %u.%u.%u.%u/%u\n", wiport.ip0, wiport.ip1, wiport.ip2, wiport.ip3, 32 - wiport.netmask) ;
      printf("Gateway:   %u.%u.%u.%u\n", wiport.defaultGateway[0], wiport.defaultGateway[1], wiport.defaultGateway[2], wiport.defaultGateway[3]) ;
      printf("DNS:       %u.%u.%u.%u\n", wiport.dns_ip0, wiport.dns_ip1, wiport.dns_ip2, wiport.dns_ip3) ;
      printf("Target IP: %u.%u.%u.%u\n", wiport.ch1_ip0, wiport.ch1_ip1, wiport.ch1_ip2, wiport.ch1_ip3) ;
      printf("Name:      %s\n\n", wiport.dns_name) ;
      } ;

#else
   wiport.ip0 = 192 ;
   wiport.ip1 = 168 ;
   wiport.ip2 = 1 ;
   wiport.ip3 = 12 ;
   wiport.remote_port = 601 ;
   wiport.ch1_ip0 = 192 ;
   wiport.ch1_ip1 = 168 ;
   wiport.ch1_ip2 = 1 ;
   wiport.ch1_ip3 = 5 ;
   wiport.port = 10004 ;
   wiport.netmask = 8 ;
   wiport.defaultGateway[0] = 192 ;
   wiport.defaultGateway[1] = 168 ;
   wiport.defaultGateway[2] = 1 ;
   wiport.defaultGateway[3] = 1 ;
   wiport.dns_ip0 = 10 ;
   wiport.dns_ip1 = 0 ;
   wiport.dns_ip2 = 1 ;
   wiport.dns_ip3 = 1 ;
   strcpy(wiport.dns_name, "yahoo.com") ;
#endif

   if (!OF6_RF_PORT)
      {
      printf("xPort is disabled by configuration\r\n") ;
      return ;
      } ;
   ETH_STATUS = ETH_STATUS_BAD_CONFIG ;

   if (!wiport.ip0 && !wiport.ip1 && !wiport.ip2 && !wiport.ip3)
      {
      printf("Local IP address was not provided\r\n") ;
      return ;
      } ;
   if (wiport.netmask == 0)
      {
      printf("Network mask was not provided\r\n") ;
      return ;
      } ;

   if (!wiport.ch1_ip0 && !wiport.ch1_ip1 && !wiport.ch1_ip2 && !wiport.ch1_ip3)
      {
      register size_t len ;

      if (!wiport.dns_ip0 && !wiport.dns_ip1 && !wiport.dns_ip2 && !wiport.dns_ip3)
         {
         printf("DNS server address required, but was not provided\r\n") ;
         return ;
         } ;
      len = strlen(wiport.dns_name) ;
      if (len == 0 || len > sizeof(wiport.dns_name) - 1)
         {
         printf("Target DNS name required, but was not provided\r\n") ;
         return ;
         } ;
      } ;

   if (RF_Debug) ShowWiPortConfig() ;

   /* Turn power down on xPort */
   ETH_STATUS = ETH_STATUS_POWER ;
   OCTALB_RTS_ON() ;
   if (RF_Debug) printf("XPort power off\r\n") ;
   /* Reset RX state */
   NextRx(NULL) ;
   /* Set 9600 speed */
   InitializeRFComm(B_9600) ;
   /* Power down time delay */
   Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_PowerOn, 3000) ;
   }



static char inbuf[256] ;
/*************************************************************************************
 *
 *       This function is called, when UART on xPort has some data to read
 *
 *************************************************************************************/
void ProcessRF(void)
   {
   register int   n ;
   register char *lpb ;

   while (1)
      {
      n = RFNGetc(inbuf, (int)sizeof(inbuf)) ;
      if (!n) return ;
      lpb = inbuf ;
      if (RF_Debug)
         {
         lpb[n] = 0 ;
         printf("%s", lpb) ;
         } ;
      if (RxState)
         {
         do
            {
            (*RxState)(*lpb++) ;
            }
         while (--n) ;
         } ;
      } ;
   }


/*  Do nothing.  This is called when we receive UDP packet */
static void RxIdle(byte c)
   {
   if (!RF_Debug) printf("%c", c) ;
   return ;
   }


/*  Unexpected time-out.  Stop xPort  */
static void RF_Rto(void)
   {
   printf("xPort: Timeout\r\n") ;
   OCTALB_RTS_ON() ;
   NextRx(NULL) ;
   } ;


static size_t  nLines = 0 ;
static size_t  NextLine = 0 ;
static char    buf[256] ;

static void EndOfConfig2(char c)
   {
   if (c != '.')
      count = 0 ;
   else
      count++ ;

   if (count != 3) return ;
   printf("\r\nxPort configuration finished\r\n") ;
   Init_Tmr_Fn(WI_PORT_OFF_TMR, DARTStart, 12000) ;
   count = 0 ;
   NextRx(RxIdle) ;
   }

/* End of configuration */
static void EndOfConfig(char c)
   {
   if (c != '.') return ;
   /* Set 115200 speed */
   InitializeRFComm(B_115200) ;
   NextRx(EndOfConfig2) ;
   count = 0 ;
   }


/* Transmit next XML line */
static void RxIdleCount(char c)
   {
   register const char *lpb ;
   char  temp_buf[64] ;

   if (--count) return ;
   if (NextLine == nLines)
      {
      Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_Rto, 3000) ;
      NextRx(EndOfConfig) ;
      return ;
      } ;

   lpb = xml[NextLine++] ;
   if (strstr(lpb, "\"ip address\""))
      {
      /* xPort IP address */
      sprintf(buf, lpb, wiport.ip0, wiport.ip1, wiport.ip2, wiport.ip3, 32 - wiport.netmask) ;
      lpb = buf ;
      }
   else if (strstr(lpb, "\"local port\""))
      {
      /* xPort UDP port */
      if (wiport.remote_port)
         sprintf(buf, lpb, wiport.remote_port) ;
      else
         sprintf(buf, lpb, 601) ;
      lpb = buf ;
      }
   else if (strstr(lpb, "\"default gateway\""))
      {
      if (wiport.defaultGateway)
         sprintf(buf, lpb, wiport.ip0, wiport.ip1, wiport.ip2, 1) ;
      else
         sprintf(buf, lpb, wiport.defaultGateway[0], wiport.defaultGateway[1], wiport.defaultGateway[2], wiport.defaultGateway[3]) ;
      lpb = buf ;
      }
   else if (strstr(lpb, "\"address\""))
      {
      /* Target IP address */
      if (wiport.ch1_ip0 || wiport.ch1_ip1 || wiport.ch1_ip2 || wiport.ch1_ip3)
         {
         sprintf(temp_buf, "%u.%u.%u.%u", wiport.ch1_ip0, wiport.ch1_ip1, wiport.ch1_ip2, wiport.ch1_ip3) ;
         sprintf(buf, lpb, temp_buf) ;
         }
      else
         sprintf(buf, lpb, wiport.dns_name) ;
      lpb = buf ;
      }
   else if (strstr(lpb, "\"port\""))
      {
      /* Target port */
      sprintf(buf, lpb, wiport.port) ;
      lpb = buf ;
      }
   else if (strstr(lpb, "\"primary dns\""))
      {
      if (wiport.dns_ip0 || wiport.dns_ip1 || wiport.dns_ip2 || wiport.dns_ip3)
         {
         sprintf(temp_buf, "%u.%u.%u.%u", wiport.dns_ip0, wiport.dns_ip1, wiport.dns_ip2, wiport.dns_ip3) ;
         sprintf(buf, lpb, temp_buf) ;
         }
      else
         sprintf(buf, lpb, "&#60;None&#62;") ;
      lpb = buf ;
      } ;

   count = (int)strlen(lpb) ;
/*   if (RF_Debug) printf("Line %lu\r\n", NextLine) ; */
   RFNputs(lpb, count) ;
   Load_Tmr(WI_PORT_OFF_TMR, count*4 + 10) ;
   }


/* Got into command line mode */
static void RxGotPrompt(void)
   {
   register const char *lpb ;

   /* Number of lines to upload */
   nLines = sizeof(xml) / sizeof(char*) ;
   if (RF_Debug) printf("Total XML lines: %lu\r\n", nLines) ;
   lpb = xml[0] ;
   count = (int)strlen(lpb) ;
   RFNputs(lpb, count) ;
   /* Do nothing on incoming characters */
   NextRx(RxIdleCount) ;
   NextLine = 1 ;
   Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_Rto, count*4 + 10) ;
   }


/* Wait for '>' */
static void RxWaitPrompt2(byte c)
   {
   /* After first ">" received, wait for timeout */
   if (c == '>')
      {
      Init_Tmr_Fn(WI_PORT_OFF_TMR, RxGotPrompt, 1000) ;
      } ;
   }


/* Wait for '!' */
static void RxWaitPrompt1(byte c)
   {
   if (c == '!')
      {
      RFNputs("xyz", 3) ;
      NextRx(RxWaitPrompt2) ;
      Init_Tmr_Fn(WI_PORT_OFF_TMR, RF_Rto, 1000) ;
      ETH_STATUS = ETH_STATUS_ERROR ;
      } ;
   }
