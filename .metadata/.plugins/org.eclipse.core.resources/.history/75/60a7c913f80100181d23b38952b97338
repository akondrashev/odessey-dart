/*----------------------------------------------------------------------------\
|  Src File:   Hdlc.c                                                         |
|  Version :   2.10                                                           |
|  Authored:   02/24/97, tgh                                                  |
|  Function:   HDLC communication module for probe.                           |
|  Comments:   This module implements the HDLC communication protocol modified|
|              for asynchronous communication.                                |
|                                                                             |
|  Revision:                                                                  |
|  $Log: hdlc.c,v $                                                           |
|      1.00:   08/01/97, tgh  -  Initial release.                             |
|      2.00:   12/19/98, tgh  -  Initial release.                             |
|      2.01:   09/14/99, sjb  -  bugger up to check for ver 4 data system.    |
|      2.02:   11/04/99, sjb  -  bugger up to check for ver 4 data system.    |
|      2.05:   09/04/01, sjb  -  bugger up to check for ver 4 portable probe. |
|      2.06:   07/12/02, sjb  -  add flow control timer to see if on too long |
|      2.07:   05/06/03, sjb  -  modify checking for switching baud rates     |
|      2.08:   08/30/04, sjb  -  increase  frame to tx/rx to 32               |
|                                use Memcpy to copy data                      |
|      2.09:   03/31/05, sjb  -  add diagnostic counter to check comm problems|
|      2.10:   12/13/07, sjb  -  check if baud rate at 9600 when sync for the |
|                                Electronic key has been detected             |
|                             -  modify check of sync characters and count    |
|                                errors to be used for changing baud rates    |
|                                for detecting communications with electronic |
|                                key                                          |
|              Copyright (c) 1993-2007  GFI All Rights Reserved               |
\----------------------------------------------------------------------------*/
/* system headers
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/* project headers
*/
#include <gen.h>
#include <hdlc.h>
#include <timers.h>
#include <probe.h>
#include <cnf.h>
#include <transact.h>
#include <misc.h>
#include <cnf.h>
#include <regs.h>
#include <tlog.h>
#include "ability.h"
#include "fbd.h"
#include "util.h"
#include "ability.h"
#include "status.h"

/* Defines and typedefs
*/
#undef   _DEBUG_COMM                            /* if comm Tlogs wanted    */
#undef   _CYCLE_ALL_BAUDRATES                   /* if all rates checked    */
#define  SYNC              (byte)0xfe           /* sync char. (2 in a row) */
#define  FINAL             (byte)0x10           /* final frame flag        */
#define  SABM              (byte)0x2f           /* set async balanced mode */
#define  RR                (byte)0x01           /* receiver ready          */
#define  RNR               (byte)0x05           /* receiver not ready      */
#define  REJ               (byte)0x09           /* reject                  */
#define  MODULUS           8                    /* 7 frames (8-1) at once  */
#define  BROADCAST_ADDRESS 2                    /* address received is broadcast */
#define  STX               0x02                 /* electronic key message  */
#define  V4SYNC            0x24                 /* $ ver. 4.xx sync char   */
#define  V4PPSYNC          0x7c                 /* '|' ver. 4.xx PP sync   */
#define  V4_PP_SYNC        0x7b                 /* '{' ver. 4.xx PP sync   */
                                                /*  for Pittsburg          */

#define  RX_BUF_SZ         256                  /* read buffer size */
#define  TX_BUF_SZ         256                  /* transmit buffer size */
#define  Q_BUF_SZ          256                  /* queue buffer size */
#define  MAXMSG            20                   /* max. queued messages */
#define  TXBUF_SIZE        MODULUS*TX_BUF_SZ
#define  MAX_NAKS          4 /* 10 */

#define  RESPONSE          500

#define  POLLTIME          MSEC(250)            /* poll time */
#define  RESPTIME          MSEC(RESPONSE)       /* response time */
#define  T3                SECONDS( 2 )         /* offline time (2 sec.) */
#define  TX_FLOWCNTL_TIME  5                    /* tx flow cntl time (sec.) */

/* Flags contained in "Flags" and macros to manipulated them.
*/
#define  F_OFFLINE         0x0001           /* currently not probing       */
#define  F_MEMCLR          0x0002           /* memory clear   pending      */
#define  F_SABM            0x0004           /* SABM           pending      */
#define  F_RNR             0x0008           /* RNR            pending      */
#define  F_REJ             0x0010           /* REJ            pending      */
#define  F_RX_FLOWCNTL     0x0020           /* RX flow control in effect   */
#define  F_TX_FLOWCNTL     0x0040           /* TX flow control in effect   */
#define  F_VER4            0x0080           /* version4 data sytem probing */
#define  F_IDLE_FRAME      0x0100           /* send an idle data frame     */

typedef struct
{
   unsigned char  nrns,
                  count;
}  HEADER;

typedef  struct
{
   byte        broadcast;                       /* bit 7 set if broadcast */
   byte        spare;
   int         sz;                              /* message size */
   byte        b[Q_BUF_SZ];                     /* message buffer */
}  BUF;

typedef  struct
{
   int         next;                            /* index to next */
   int         last;                            /* index to last */
   int         cnt;                             /* count of messages queued */
   BUF         q[MAXMSG];                       /* message queue */
}  QUE;


typedef  struct
{
   byte        type;
   byte        spare;
   long        bus;                            /* message size */
   long        fb_version;                     /* message buffer */
}  IDLE_MESSAGE;

/* Macros
*/
#define  RstNdx()           {Ndx = 0; RxBuf=RxQ.q[RxQ.next].b;}
#define  AppendLrc( c )     (Lrc ^= (c))
#define  IncSeq( n )        ((n) = (++(n)<MODULUS)?(n):0)
#define  NextRx( fptr )     (RxState = (fptr))
#define  SetFlag( s )       (Flags |= (s))
#define  ClrFlag( s )       (Flags &=~(s))
#define  TstFlag( s )       (Flags &  (s))
#define  GetNs( b )         ( ((b) >> 1) & 0x07 );
#define  GetNr( b )         ( ((b) >> 5) & 0x07 );
#define  InitTmr(n, f, t)   Init_Tmr_Fn( (n), (f), (t) );
#define  LoadTmr(n, t)      Load_Tmr( (n), (t) );
#define  StopTmr(n)         Stop_Tmr( (n) );
#define  AddressMask( c )   (c & 0x7f)
#define  BroadCastMask( c ) (c & 0x80)

/* Prototypes
*/
void  IdleState            ( void );
int   SetAsyncBalancedMode ( void );
int   Nq                   ( QUE* );
int   Dq                   ( QUE* );
int   Putc                 ( byte );
/* int   Puts                 ( byte* ); */
int   Nputs                ( byte*, int );
int   PRBPutc              ( byte );
/* int   PRBPuts              ( byte* ); */
int   PRBNputs             ( byte*, int );
int   PRBCnt               ( void );
int   PRBGetc              ( void );
int   ProcFrame            ( void );
int   Frame                ( byte, byte, BUF*, byte );
void  Poll                 ( void );            /* timer functions */
void  RtoFnct              ( void );
void  StoFnct              ( void );
void  ResetBaudRate        ( void );
void  ProcessProbe         ( void );
int   GetSync1             ( byte );            /* receive state prototypes */
int   GetSync2             ( byte );
int   GetNrNs              ( byte );
int   GetCnt               ( byte );
int   GetData              ( byte );
int   GetLrc               ( byte );
int   GetLockCode          ( byte );
void  CheckFlowCntlTimes   ( void );    /* ver 2.06 */
void  CheckErrorRate       ( void );
int   GetAddress           ( byte );
void  DelayedResponse      ( void );
void  IdleFrame            ( void );

/* global data
*/

word      PrbNack;
word      PrbSeqErr;
word      PrbLrcErr;
word      PrbRecFull;
word      PrbRetransmit;
int       Probe_Debug;

/* module data
*/
static   int         FrameActivityF =  0;
static   int         Rto;                       /* response timer */
static   int         Gto;                       /* general timer (key seq.) */
static   int         Sto;                       /* session timer inter-char. */

static   HEADER      Header;
static   int         NackCnt        =  0;
static   int         Retry;
static   int         RtoCnt         =  0;       /* response timeout counter */
static   time_t      Tx_FlowCntl_t  =  0L;      /* time flow control started */

static   byte        S     =  0;                /* current send state */
static   byte        R     =  0;                /* current receive state */
static   byte        Ps    =  0;                /* pending send sequence */
static   byte        Fs    =  0;                /* no. of outstanding frames */

static   int       (*RxState)( byte );          /* receive state */
static   QUE         TxQ;                       /* tx message queue */
static   QUE         RxQ;                       /* rx message queue */

static   int         RxCnt;                     /* No. of data bytes to get */
static   byte        Lrc;                       /* Current calc. LRC */

static   byte        TxBuf[ TXBUF_SIZE ];       /* Transmit Buffer */
static   int         TxNdx =  0;                /* Index into the tx buffer */
static   byte       *RxBuf;                     /* Pointer to receive buffer */
static   int         Ndx   =  0;                /* index into receive buffer */
static   word        Flags =  0;                /* maintained by macros below*/
static   word        br = B_9600;
static   word        PrbSynErr;                  /* sync char. error count */
static   byte        MyAddress;

/*----------------------------------------------------------------------------\
|                                                                             |
|              USER CALLABLE FUNCTIONS                                        |
|                                                                             |
\----------------------------------------------------------------------------*/

int   InitializeHDLCProtocol()
{
   Rto = HDLC_Rto;
   Sto = HDLC_Sto;
   Gto = HDLC_Gto;
   memset( (char*)&TxQ, 0, sizeof(TxQ) );
   memset( (char*)&RxQ, 0, sizeof(RxQ) );
   Flags =  (F_OFFLINE|F_SABM);
   IdleState();
   Poll();
   return 0;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|              LOCAL FUNCTIONS                                                |
|                                                                             |
\----------------------------------------------------------------------------*/
static
void  CheckErrorRate()
{
   static   time_t   last_initialize_time = 0;
/*   static   word     br = B_9600; */
   static   ulong    LastErrCnt;
            ulong    DiffErrCnt;
            ulong    ErrCnt;
            time_t   t;

   ErrCnt = PrbFrmErr + PrbBrkErr + PrbSynErr;  /* total errors */
   if ( ErrCnt && (DiffErrCnt=labs( ErrCnt - LastErrCnt )) > 60L )
   {
   #ifdef _CYCLE_ALL_BAUDRATES
      /* this cycles trough all selectable baudrates */
      Tlog( "DiffErrCnt = %lu\n", DiffErrCnt );
      switch( br )
      {
         case  B_9600:                           /* 9600 */
            br = B_19200;
            Tlog( "try 19200 baud\n" );
            break;
         case  B_19200:                          /* 19.2k */
            br = B_38400;
            Tlog( "try 38400 baud\n" );
            break;
         case  B_38400:                          /* 38.4k */
            br = B_57600;
            Tlog( "try 57600 baud\n" );
            break;
         case  B_57600:                          /* 57.6k */
            br = B_115200;
            Tlog( "try 115200 baud\n" );
            break;
         case  B_115200:                         /* 115.2K */
            br = B_230400;
            Tlog( "try 230400 baud\n" );
            break;
         case  B_230400:                         /* 230.4K */
         default:
            br = B_9600;
            Tlog( "try 9600 baud\n" );
            break;
      }
   #else
      /* this cycles only the default and 9600 baud electronic key */
      switch( br )
      {
         case  B_9600:
            br = BaudRate;
            Tlog( "try default baud\n" );
            break;
         default:
            br = B_9600;
            Tlog( "try 9600 baud\n" );
            break;
      }
   #endif
      initialize_prb_comm( br );
      IdleState();
   }
   else
   {
      /* if no errors then just reset the default
         baudrate every 5 seconds.
      */
      time( &t );
      if ( labs( t - last_initialize_time ) > 5L )
      {
         last_initialize_time = t;
         initialize_prb_comm( 0 );            /* default rate */
         br = BaudRate;                         /* keep track */

         if ( Probe_Debug )        /* offline ? */
            SetFlag( F_IDLE_FRAME );
/*    printf("flags %4x\n", Flags ); */
      }
   }
   LastErrCnt = ErrCnt;
}


static
void  Poll()
{
   SetFlag( F_OFFLINE );

   CheckErrorRate();
   ProbeIdle();

   if ( TstFlag(F_IDLE_FRAME ))
   {
     IdleFrame();
   }
   else
   {
     if ( !PROBE_ENABLE || (PROBE_ENABLE && !tmr_expired( MAIN_LOG_TMR )) )
     {
       TransmitStatus();
       RtoCnt = 0;
     }
     InitTmr( Rto, Poll, POLLTIME );
   }
   Clr_A2flag( A_STATION_ONLINE );
}

/*----------------------------------------------\
|  Function:   FrameActivity()                  |
|  Purpose :   Indicate comm. activity.         |
|  Synopsis:   int   FrameActivity( void );     |
|  Input   :   none                             |
|  Output  :   int - none zero if frames        |
|              are sent or received.            |
|  Comments:   Added for windows implementaion. |
\----------------------------------------------*/
void  IdleFrame()
{
   IDLE_MESSAGE p;

   p.type        = FB_PROBE_STATUS;
   p.spare       = 0;
   p.bus         = Ml.bus_n;
   p.fb_version  = Ml.fbx_v;
   QFrameRaw( (char*)&p, (int)(sizeof(IDLE_MESSAGE)), NO_BROADCAST );
   SendFrames();
}

/*----------------------------------------------\
|  Function:   FrameActivity()                  |
|  Purpose :   Indicate comm. activity.         |
|  Synopsis:   int   FrameActivity( void );     |
|  Input   :   none                             |
|  Output  :   int - none zero if frames        |
|              are sent or received.            |
|  Comments:   Added for windows implementaion. |
\----------------------------------------------*/
int   FrameActivity()
{
   int   E = FrameActivityF;

   FrameActivityF =  0;

   return E;
}

/*----------------------------------------------\
|  Function:   QFrame();                        |
|  Purpose :   Queue up data frame.             |
|  Synopsis:   void  QFrame( t, bn, bs, p, n ); |
|  Input   :   int     t - frame type           |
|              int     bn- block count/number   |
|              long    bs- block/total size     |
|              unchar *p - data pointer         |
|              int     n - data bytes           |
|              int     c - broadcast            |
|  Output  :   int -1 - errors                  |
|              int >=0 - no. of frames pending  |
|  Comments:   High level function to queue up  |
|              frames of data using the HDR.    |
|              Note that the bn and bs fields   |
|              have different meanings depending|
|              on context of frame type.  The   |
|              word and long fields in bn & bs  |
|              are converted to motorola format.|
\----------------------------------------------*/
int QFrame(int t, int bn, long bs, byte *p, int n, int c )
{
   int   E = -1, sz;
   HDR   h;

   if ( TxQ.cnt >= MAXMSG )
      goto function_end;

   memset( (char*)&h, 0, sizeof( HDR ) );       /* build header */
   h.type      =  t;
   if ( bn )                                    /* if block number present */
   {
      h.block  =  bn;
      swab( (char*)&h.block, (char*)&h.block, 2 );
   }
   if ( bs )                                    /* if block size present */
   {
      h.size   =  bs;
      swabl( (char*)&h.size, (char*)&h.size, 4 );
   }
   sz          =  sizeof( HDR );
   memcpy( TxQ.q[TxQ.next].b, (char*)&h, sz );
   if ( p != NULL && n > 0 )                    /* any data */
   {
      Memcpy( TxQ.q[TxQ.next].b+sizeof(HDR), p, (ulong)n );
      sz += n;
   }
   TxQ.q[TxQ.next].sz = sz;
   TxQ.q[TxQ.next].broadcast = c;
   Nq( &TxQ );

   E = 0;

   function_end:
   return E;
}

/*----------------------------------------------\
|  Function:   QFrameRaw();                     |
|  Purpose :   Queue up data frame.             |
|  Synopsis:   void  QFrameRaw( p, n );         |
|  Input   :   unchar *p - data pointer         |
|              int     n - data bytes           |
|              int     c - broadcast            |
|  Output  :   int -1 - errors                  |
|              int >=0 - no. of frames pending  |
|  Comments:   Queue up a block of data of any  |
|              type not dependant on the use of |
|              HDR struct.                      |
\----------------------------------------------*/
int QFrameRaw(byte *p, int n, int c)
   {
   if (TxQ.cnt >= MAXMSG) return -1 ;
   if (Tst_Sflag( S_SPARES2)) printf("TX data type %d\n", *p) ;
   Memcpy(TxQ.q[TxQ.next].b, (char*)p, (ulong)n) ;
   TxQ.q[TxQ.next].sz        = n;
   TxQ.q[TxQ.next].broadcast = c;    /* broadcast bit */
   Nq( &TxQ );
   return 0 ;
   }


int   DqFrame()
{
   return( Dq( &RxQ ) );
}

int   TxQCnt()
{
   return TxQ.cnt;
}

int   RxQCnt()
{
   return RxQ.cnt;
}


int GetFrame(byte **p)
   {
   int   n;

   if ((n = RxQ.cnt))
      {
      if (p) *p = RxQ.q[RxQ.last].b ;
      n = RxQ.q[RxQ.last].sz;
      }
   return n;
   }


void  ProcessHDLC()
{
   static   byte  buf[4096];
            int   i, n;

   if ( VER_4 && TstFlag( F_VER4 ) )
   {
      ProcessProbe();
      LoadTmr( Rto, T3 );
   }
   else
   {
     for( i=0, n=PRBCnt(); i<n; i++ )
     {
        buf[i] = PRBGetc();
        /*putchar( buf[i] );*/
     }
     ParseHDLC( buf, n );
   }
}


int  ParseHDLC(byte *s,  int n)
   {
   int   i,j;

   #ifdef _DEBUG_COMM
   Tlog( "RX : " );
   for( i=0; i<n; i++ )
      Tlograw( "%02x", s[i] );
   Tlograw( "\n" );
   #endif

   if ( Serial_Debug )
   {
     printf( "RX : " );
     j = n;
     if ( j > 8 )
       j = 8;
     for( i=0; i<j; i++ )
        printf( "%02x", s[i] );
     putchar( '\n' );
   }

   for( i=0; i<n; i++ )
   {
     if ( Ndx >= RX_BUF_SZ )
       IdleState();
     if ( RxState )
        (*RxState)( s[i] );
   }
   return 0;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|              LOCAL FUNCTIONS                                                |
|                                                                             |
\----------------------------------------------------------------------------*/

static int BumpQndx(int *p)
   {
   if ( *p < MAXMSG-1 )
      (*p)++;
   else
      *p = 0;

   return *p;
   }


static int Nq(QUE *p)
   {
   if ( p->cnt >= MAXMSG )
      return -1;

   BumpQndx( &p->next );

   if ( p->cnt < MAXMSG )                       /* bump message count */
      p->cnt++;
   else
      BumpQndx( &p->last );

   return 0;
}


static int Dq(QUE *p)
   {
   p->q[p->last].sz = 0;                        /* set the size to zero */
   p->q[p->last].broadcast = 0;                 /* clear broadcast flag */

   if ( p->cnt <= 0 )
   {
      p->cnt = 0;
      goto function_end;
   }

   p->cnt--;
   BumpQndx( &p->last );

   if ( p == &TxQ )                             /* bump S if transmit queue */
   {
      IncSeq( S );
   }

   function_end:
   return p->cnt;
}


int   TxDq()
{
   TxQ.q[TxQ.last].sz = 0;                      /* set the size to zero */

   if ( TxQ.cnt <= 0 )
   {
      TxQ.cnt = 0;
      goto function_end;
   }

   TxQ.cnt--;
   BumpQndx( &TxQ.last );

   function_end:
   return TxQ.cnt;
}

static
int   Putc( byte c )
{
   #ifdef _DEBUG_COMM
   Tlog( "TX : %02x\n", c );
   #endif

   return PRBPutc( c );

}
#if 0
static
int   Puts( s )
byte       *s;
{
   #ifdef _DEBUG_COMM
   int   n = strlen( s );
   int   i;

   Tlog( "TX : " );
   for( i=0; i<n; i++ )
      Tlograw( "%02x", s[i] );
   Tlograw( "\n" );
   #endif

   return PRBPuts( s );
}
#endif

static int Nputs(byte *s, int n)
   {
   #ifdef _DEBUG_COMM
   int   i;

   Tlog( "TX : " );
   for( i=0; i<n; i++ )
      Tlograw( "%02x", s[i] );
   Tlograw( "\n" );
   #endif

   return PRBNputs( s, n );
}

static
void  IdleState()
{
   NextRx( GetSync1 );
   RstNdx();
   ClrFlag( F_VER4 );
   MyAddress = FALSE;
}

static
int   ProcFrame()
{
   RxQ.q[RxQ.next].sz = Ndx;                    /* save no. of bytes received */
   return Nq( &RxQ );                           /* queue up frame received */
}

/*----------------------------------------------\
|  Function:   TransmitStatus()                 |
|  Purpose :   send special frame when no data  |
|  Synopsis:   int   TransmitStatus();          |
|  Input   :   None.                            |
|  Output  :   always zero.                     |
|  Comments:                                    |
\----------------------------------------------*/
static int TransmitStatus(void)
   {
   static   char  b[5];
            byte  Nr    = R;
   static   unsigned char toggle;
   int      i = 2;

   if ( VER_4 )            /* configured for version 4 */
   {
    /* check if time to check if probed by ver 4/7 data system */
     if ( MyAgency != AGENCY_SEATTLE )
     {
       if ( toggle )
         toggle = 0;      /* version 7 */
       else
         toggle = TRUE;   /* version 4 */
     }
     else
       toggle = TRUE;     /* don't toggle if Seattle */
   }

   if ( VER_4 && toggle ) /* check if ver 4 poll */
   {
      ClrFlag( F_VER4 );
      NextRx( GetSync1 );
      Putc('$');
      InitTmr( Rto, RtoFnct, RESPTIME );
   }
   else
   {
      if ( TstFlag( F_SABM ) )                     /* SABM pending ? */
      {
         b[i] = SABM;
         SetAsyncBalancedMode();
      }
      else if ( TstFlag( F_RNR ) )                      /* RNR pending ? */
      {
         b[i] = (Nr<<5) | RNR | FINAL;
         ClrFlag( F_RNR );
         SetFlag( F_RX_FLOWCNTL );
         Tlog( "tx RNR\n" );/*fixme*/
      }
      else if ( TstFlag( F_REJ ) )                      /* REJ pending ? */
      {
         b[i] = (Nr<<5) | REJ | FINAL;
         ClrFlag( F_REJ );
         ClrFlag( F_RX_FLOWCNTL );
         Tlog( "tx REJ\n" );/*fixme*/
      }
      else
      {
        b[i] = (Nr<<5) | RR;                  /* default to receive reader */
      }

      b[0] = b[1] = SYNC;
      b[3] = b[2];

      Nputs( b, i+2 );
      InitTmr( Rto, RtoFnct, RESPTIME );
   }
   return 0;
}

static
int   SetAsyncBalancedMode()
{
   ClrFlag( F_SABM );
   S = R = Fs = 0;
   return 0;
}

/*-------------------------------------------------------------\
|  Function:   Frame( S, R, p, f );                            |
|  Purpose :   Put queued up frames into the transmit buffer.  |
|  Synopsis:   int error = frame( S, R, p, f );                |
|              p - (BUF *) points to the data                  |
|              S - (byte) send sequence number (N(s))          |
|              R - (byte) receive sequence number (N(r))       |
|              f - (byte) final frame flag                     |
|  Return  :                                                   |
|                                                              |
|  Comments:   The "lrc" will be calculated as well as the     |
|              mapping of the N(s), N(r) sequence numbers.     |
\-------------------------------------------------------------*/
static
int   Frame( byte S, byte R, BUF *p, byte f )
{
   int   i;
   byte *s;
   byte  lrc=0;

   R &= 0x07;                                   /* keep significant bits */
   S &= 0x07;

   TxBuf[ TxNdx++ ] = SYNC;                     /* synchronization */
   TxBuf[ TxNdx++ ] = SYNC;
   lrc ^= TxBuf[ TxNdx++ ] =                    /* N(r) N(s) */
      (R<<5)|(S<<1)|((f) ? FINAL:0);

   lrc ^= TxBuf[ TxNdx++ ] = p->sz;             /* byte count */

   for( i=(p->sz)+TxNdx, s=p->b; TxNdx<i; TxNdx++ ) /* copy data */
      lrc ^= TxBuf[ TxNdx ] = *s++;

   TxBuf[ TxNdx++ ] = lrc;                      /* LRC */
/*
   if ( p->b[0] == RECV_CARD_SN )
     printf("send card\n");
*/
   return 0;
}

static
int   SendFrames()
{
   int   fs,                                    /* count of frames sent */
         fq,                                    /* count of frames queued */
         ndx;                                   /* queue entry index */
   word  resp;                                  /* response time in  */
   byte  Ns =  S;                               /* send sequence number */
   byte  Nr =  R;                               /* receive sequence number */

   if ( TstFlag( F_SABM|F_RNR|F_REJ|F_TX_FLOWCNTL ) )
   {
      if ( TstFlag( F_OFFLINE ) )
      {
        fs = 0;
        goto function_end;
      }
   }

   for( fs=0, fq=TxQ.cnt, ndx=TxQ.last; fq; fq-- )
   {
      fs++;                                     /* bump frames sent */
      if ( fq <= 1 || fs >= MODULUS-1 )         /* last entry ? */
      {
         Frame( Ns, Nr, &TxQ.q[ndx], 1 );       /* put frame into tx buffer */
         break;
      }
      else
      {
         Frame( Ns, Nr, &TxQ.q[ndx], 0 );       /* put frame into tx buffer */
      }
      BumpQndx( &ndx );
      IncSeq( Ns );
   }

   if ( fs )                                    /* anything to send */
   {
      IncSeq( Ns );
      Ps = Ns;
      Nputs( TxBuf, TxNdx );                    /* transmit the buffer */
      resp = TxNdx + RESPONSE;
      InitTmr( Rto, RtoFnct, MSEC( resp ) );
      TxNdx = 0;
   }

   function_end:
   return fs;
}


/*----------------------------------------------------------------------------\
|                                                                             |
|                             TIMER FUNCTIONS                                 |
|                                                                             |
\----------------------------------------------------------------------------*/
static
void  StoFnct()
{
   SetFlag( F_OFFLINE );
   ClrFlag( F_TX_FLOWCNTL|F_IDLE_FRAME );
   IdleState();
}

static
void  ResetBaudRate()
{
   initialize_prb_comm( 0 );                    /* reset to default baudrate */
   StoFnct();
   Poll();
}


static
void  RtoFnct()
{
   if ( ++RtoCnt > 3 )
   {
      if (  TstFlag( F_IDLE_FRAME ))
      {
        if (TxQ.q[TxQ.last].broadcast || TstFlag( F_IDLE_FRAME ) )
        {
          if ( TxQ.cnt )
          {
            Dq( &TxQ );        /* dequeue them */
          }
          ClrFlag( F_IDLE_FRAME );
        }
      }
      Poll();
   }
   else
   {
      if ( ! (Fs=SendFrames()) )
      {
        TransmitStatus();
      }
   }
}

static
void CheckFlowCntlTimes()     /* ver 2.03 */
{
    time_t   t;

    if ( TstFlag( F_TX_FLOWCNTL ))
    {
       time( &t );

       if ( labs( t - Tx_FlowCntl_t ) > TX_FLOWCNTL_TIME )
         ClrFlag( F_TX_FLOWCNTL );
    }
}
/*----------------------------------------------------------------------------\
|                                                                             |
|                             RECEIVE STATES                                  |
|                                                                             |
\----------------------------------------------------------------------------*/

static
int   GetSync1( byte c )
{
   if ( !PROBE_ENABLE || (PROBE_ENABLE && !tmr_expired( MAIN_LOG_TMR )) )
   {
      switch( c )
      {
         case SYNC:
            if ( PROBE_ENABLE )
               Init_Tmr_Fn( MAIN_LOG_TMR, 0, SECONDS( 30 ) );
            NextRx( GetSync2 );
            InitTmr( Rto, RtoFnct, T3 );
            ClrFlag( F_OFFLINE );
            InitTmr( Sto, StoFnct, MSEC(5) );
            break;
         case STX:                              /* electronic key message */
            if ( br == B_9600 )                 /* version 2.10 check baud */
            {
              RxCnt =  7;                       /* always 7 characters long */
              Ndx   =  0;
              RxBuf =  ElKeyBuff;
              NextRx( GetLockCode );
              LoadTmr( Rto, T3 );
              InitTmr( Sto, ResetBaudRate, MSEC(32) );
            }
            break;
         default:
            if ( VER_4 )
            {
              switch( c )
              {
              case '#':                      /* version 4  */
              case '{':                      /* version 4, Pittsburg PP  */
              case '|':                      /* version 4  */
                 SetFlag( F_VER4 );
                 Ver4Protocol();
                 Putc('$');
                 LoadTmr( Rto, T3 );
                 InitTmr( Sto, StoFnct, MSEC(5) );
                 break;
              default:
                 ++PrbSynErr;    /* 2.10 count sync error */
                 break;
              }
            }
            else
            {
              ++PrbSynErr;       /* 2.10 count sync error */
            }
            break;
      }
   }
   return 0;
}

static
int   GetSync2( byte c )
{
   if ( c == SYNC )
   {
      MyAddress = TRUE;         /* pretend it's ours */
      NextRx( GetNrNs );
      Lrc = 0;
      LoadTmr( Sto, MSEC(5) );
   }
   else
   {
      ++PrbSynErr;
      CheckErrorRate();
   }
   return 0;
}


static
int   GetAddress( byte c )
{
   if ( AddressMask( c ) == (byte)FbxCnf.gateno+8 )
   {
      MyAddress = TRUE;
   }
   else if (BroadCastMask( c ) )
   {
     MyAddress = BROADCAST_ADDRESS;
   }
   else
   {
     MyAddress = FALSE;
   }
   NextRx( GetNrNs );
   AppendLrc( c );
   LoadTmr( Sto, MSEC(10));

   return 0;
}

static
int   GetNrNs( byte c )
{
   AppendLrc( c );
   Header.nrns = c;
   if ( c & 0x01 )                              /* special frame */
      NextRx( GetLrc );
   else
      NextRx( GetCnt );
   LoadTmr( Sto, MSEC(5) );
   return 0;
}

static
int   GetCnt( byte c )
{
   AppendLrc( c );
   RxCnt = c;
   RstNdx();
   NextRx( GetData );
   LoadTmr( Sto, MSEC(5)*c );
   return 0;
}

static
int   GetData( byte c )
{
   AppendLrc( c );

   if ( RxQ.cnt < MAXMSG )                      /* save if space available */
      if ( Ndx < Q_BUF_SZ )
         RxBuf[Ndx++] = c;

   if ( --RxCnt == 0 )
      NextRx( GetLrc );
   return 0;
}

static
int   GetLockCode( byte c )
{
   RxBuf[Ndx++] = c;

   if ( --RxCnt == 0 )
   {
      Electronic_Key_Check();
      IdleState();
      InitTmr( Sto, ResetBaudRate, MSEC(32) );
   }

   return 0;
}

static
int   GetLrc( byte c )
{
   static   int   i;
            byte  Ns, Nr;

   StopTmr( Sto );

   if ( !MyAddress )                            /* is it for us ??? */
   {
     InitTmr( Rto, RtoFnct, T3 );
     IdleState();                               /* not for us */
     return 0;
   }

   if ( c == Lrc )                              /* good lrc */
   {
      RtoCnt = 0;
      Clist.PRIVATE.OpFlags &= ~C_VER4;          /* set to version 6 */

      FrameActivityF = 1;
      Ns = GetNs( Header.nrns );                /* get control fields */
      Nr = GetNr( Header.nrns );
      if ( (Header.nrns & 0x01) )               /* if S-frame */
      {
         if ( Header.nrns == SABM )             /* request balanced mode ? */
         {
            Tlog( "Rx F_SABM\n" );/*fixme*/
            SetAsyncBalancedMode();
            goto function_end;
         }
         switch( Header.nrns & 0x0f )
         {
            case  RR:                           /* receive ready */
               break;
            case  RNR:                          /* receive not ready */
               Tlog( "rx RNR\n" );/*fixme*/
               SetFlag( F_TX_FLOWCNTL );
               time( &Tx_FlowCntl_t );
               NackCnt = 0;
               break;
            case  REJ:                          /* reject */
               Tlog( "rx REJ\n" );/*fixme*/
               ClrFlag( F_TX_FLOWCNTL );
               NackCnt = 0;
               break;
            default:
               break;
         }
      }
      else                                      /* I-frame */
      {
         if ( Ns == R  || MyAddress == BROADCAST_ADDRESS ) /* sequence number match  or broadcast */
         {
            if ( RxQ.cnt < MAXMSG )             /* space available */
            {
               ProcFrame();                     /* process frame received */
               if ( MyAddress == TRUE )
                 IncSeq( R );                   /* and bump receive number */
            }
            else
            {
               if ( MyAddress == TRUE )
                 SetFlag( F_RNR );              /* flag RNR pending */
            }
         }
         else
         {
            ++PrbSeqErr;
         }
      }

      if ( Fs )                                 /* outstanding frames ? */
      {
         if ( Nr == Ps )                        /* all frames accepted */
         {
            NackCnt = 0;
            while( Fs-- > 0 )                   /* while outstanding frames */
            {
               if ( Dq( &TxQ ) <= 0 )           /* dequeue them */
                  break;
            }
            Fs = 0;                             /* make sure it's zero */
         }
         else
         {
            i = (Ps<Nr) ? (Ps + MODULUS) - Nr : Ps - Nr;
            i = (Fs>i) ? Fs - i : 0;
            if ( !i )                           /* no frames acked */
            {
               ++PrbNack;
               if ( !TstFlag( F_TX_FLOWCNTL ) )
               {
                  Tlog( "NAK\n" );/*fixme*/
                  if ( ++NackCnt >= MAX_NAKS )
                  {
                     Tlog( "F_SABM\n" );/*fixme*/
                     NackCnt = 0;
                     SetFlag( F_SABM );
                  }
               }
               else
                  Tlog( "F_TX_FLOWCNTL\n" );/*fixme*/
            }
            else
            {
               NackCnt = 0;
               while( i-- )                     /* for frame acked */
               {
                  Fs--;
                  if ( Dq( &TxQ ) <= 0 )        /* dequeue them */
                     break;
               }
            }
         }
      }
   }/* good lrc */
   else
   {
      Tlog("LRC %02x <> %02x, Ndx=%d, NrNs=%02x\n", c, Lrc, Ndx, Header.nrns);
      IdleState();
      ++PrbLrcErr;
      return 0;
   }

   function_end:

   CheckFlowCntlTimes();                        /* flow control too long ? */

   if ( (Header.nrns & 0x11) )                  /* final or S-frame bit set */
   {
      if ( TstFlag( F_RX_FLOWCNTL ) )           /* rx flow control in effect */
      {
         if ( RxQ.cnt < LOW_WATER )             /* space available ? */
         {
            SetFlag( F_REJ );
            ++PrbRecFull;
         }
      }

#ifndef _MSC_VER
      InitTmr( Gto, ProcessProbe, 1 );          /* trigger processing */
#endif

      if ( !(Fs=SendFrames()) )                 /* no data to send */
      {
#ifdef _MSC_VER
         if ( RxQCnt() )
            TransmitStatus();
         else
         {
            InitTmr( Gto, TransmitStatus, 1 );
         }
#else
         TransmitStatus();
#endif
      }
   }

   IdleState();
   return 0;
}


/*----------------------------------------------\
|  Function:   DelayedResponse();               |
|  Purpose :   Process RS485 port data.         |
|  Synopsis:   void  DelayedResponse( void );   |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:   delay response to allow 485      |
|              turn around at farebox           |
\----------------------------------------------*/
static
void  DelayedResponse()
{
   if ( !(Fs=SendFrames()) )   /* none zero if data to send */
      TransmitStatus();        /* else no data           */
}



