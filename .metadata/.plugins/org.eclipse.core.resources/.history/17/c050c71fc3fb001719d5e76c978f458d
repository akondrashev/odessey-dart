/*----------------------------------------------------------------------------\
|  Src File:   ability.h                                                      |
|  Authored:   03/15/95, tgh                                                  |
|  Function:   Defines & macros for ability flags.                            |
|  Comments:   "Aflags" are allocated in "main_vxx.c".                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/15/95, tgh  -  Initial release.                             |
|      1.01:   11/22/96, sjb  -  change ability flag defines to unsigned longs|
|      1.02:   10/26/98, sjb  -  relmove A_PROCESSING flag for credit card    |
|                                to process                                   |
|                             -  remove A_FAREHOLD and put in process.h       |
|                                                                             |
|           Copyright (c) 1994, 1995 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _ABILITY_H
#define  _ABILITY_H

/*----------------------------------------------------------------------------\
|                         ABILITY FLAGS & MACROS                              |
\----------------------------------------------------------------------------*/

#define  A_HALFHOUR_RR   (ulong)0x00000001     /* store � hour r/r           */
#define  A_QUARHOUR_RR   (ulong)0x00000002     /* store � hour r/r           */
#define  A_DIS_PASSBACK  (ulong)0x00000004     /* disable passback           */
/*                                       cleared when ALL_AFLAGS initialized */
#define  A_BIG_FARES     (ulong)0x00000008     /* large fare values          */
#define  A_J1708         (ulong)0x00000010     /* j1708                      */
#define  A_PROBE_BYPASS  (ulong)0x00000040     /* probed when in bypass      */
#define  A_BL_CKSUM      (ulong)0x00000080
#define  A_NO_COIN       (ulong)0x00000100
#define  A_NO_BILL       (ulong)0x00000200
#define  A_CARDQUEST     (ulong)0x00000400
#define  A_POWER         (ulong)0x00000800
#define  A_P_DETECT      (ulong)0x00001000
#define  A_PROBE_WD      (ulong)0x00002000     /* probe time out             */
#define  A_V2_FTBLE      (ulong)0x00004000     /* fare table withn pre/post sounds */
#define  A_VIEW_BILL     (ulong)0x00008000
#define  A_DVR_TBL_OK    (ulong)0x00010000     /* driver list crc ok & enabled */
#define  A_SPC_TBL_OK    (ulong)0x00020000     /* Special card table ok */
#define  A_RTE_TBL_OK    (ulong)0x00040000     /* rte list crc ok & enabled */
#define  A_HOLD          (ulong)0x00080000     /* Fare Hold */
#define  A_RF_MENU       (ulong)0x00100000     /* rf menu  */
#define  A_LOST_CARD     (ulong)0x00200000     /* lost card possibly detecded */
#define  A_WP_KEY        (ulong)0x00400000     /* display wp key */
#define  A_RF_MAC        (ulong)0x00800000     /* rf menu  */
#define  A_USE_PRINTER   (ulong)0x01000000     /* rf menu  */
#define  A_RF_UPDATE     (ulong)0x02000000     /* rf menu  */
#define  A_RF_RESPONSE   (ulong)0x04000000     /* rf response */
#define  A_UNHOLD_CARD   (ulong)0x08000000     /* fare released from previous hold */
#define  A_SMART_TRIM    (ulong)0x10000000     /* TRiM unit is a SmartCard Dispens*/
#define  A_WIPORT_POWER  (ulong)0x20000000
#define  A_WIPORT_PROBE  (ulong)0x40000000
#define  A_WIPORT_UPDATE (ulong)0x80000000

#define  ALL_AFLAGS     (unsigned long)\
                        (A_HALFHOUR_RR|A_QUARHOUR_RR|A_BIG_FARES|\
                         A_J1708|A_PROBE_BYPASS|A_NO_COIN|A_NO_BILL|\
                         A_CARDQUEST|A_POWER|A_P_DETECT|A_PROBE_WD|\
                         A_VIEW_BILL|A_DVR_TBL_OK|A_SPC_TBL_OK|\
                         A_RTE_TBL_OK|A_SMART_TRIM|A_HOLD|A_V2_FTBLE)

extern unsigned long  Aflags;
#define  Set_Aflag( f )          ( Aflags |= (f) )
#define  Clr_Aflag( f )          ( Aflags &=~(f) )
#define  Tst_Aflag( f )          ( Aflags &  (f) )


#define  A_SRC_AND_DST    (ulong)0x00000001      /* Set source & dest at login  */
#define  A_GGFARE_TBL     (ulong)0x00000002
#define  A_OTI6500        (ulong)0x00000004      /* using OTI 6500 with display */
#define  A_EXTEND_DISP    (ulong)0x00000008      /* display condition & user profile on OCU */
#define  A_TRIM_SC_PEND   (ulong)0x00000010
#define  A_DOOR_ACCESS    (ulong)0x00000020      /* used as a door access system */
#define  A_GATE           (ulong)0x00000040      /* configured as gate */
#define  A_HG_GATE        (ulong)0x00000080      /* configured as handicap gate */
#define  A_OPENCLOSE_CMD  (ulong)0x00000100      /* received gate open/close command */
#define  A_SCR_DISABLED   (ulong)0x00000200      /* disable reader but maintain type */
#define  A_GATE_NOEXIT    (ulong)0x00000400      /* no exit on gate */
#define  A_GATE_ID_LITE   (ulong)0x00000800      /* toggle entry/exit lights for "show ID */
#define  A_XFER_DISPLAY   (ulong)0x00001000      /* used now as flag when transfe is bein displayed */
#define  A_SHOWID_RETURN  (ulong)0x00002000
#define  A_SHOWID_AUTO    (ulong)0x00004000
#define  A_MONITOR_MODE   (ulong)0x00008000
#define  A_DELAY_REQSTAT  (ulong)0x00010000
#define  A_STARTUP        (ulong)0x00020000      /* used only at startup, don't put in ALL_A2FLAGS */
#define  A_STATION_ONLINE (ulong)0x00040000
#define  A_HG_BAR_OPEN    (ulong)0x00080000
#define  A_ROUTE_MATRIX   (ulong)0x00100000
#define  A_OTI6000        (ulong)0x00200000
#define  A_CBID_FLT       (ulong)0x00400000      /* cashbox fault wle door closed detected */
#define  A_TSC_PROCESSING (ulong)0x00800000
#define  A_RUN_TBL_OK     (ulong)0x01000000     /* run list crc ok            */
#define  A_TRIP_TBL_OK    (ulong)0x02000000     /* trip list crc ok           */
#define  A_RANGE_TBL_OK   (ulong)0x04000000     /* range list crc ok          */
#define  A_STAND_ALONE    (ulong)0x08000000
#define  A_AVL_OFFLINE    (ulong)0x10000000
#define  A_EXPANDED_SAL   (ulong)0x20000000
#define  A_SMART_CAP_TST  (ulong)0x40000000
#define  A_SMART_ISS_TST  (ulong)0x80000000

#define  ALL_A2FLAGS      (unsigned long)\
                          (A_SRC_AND_DST|A_GGFARE_TBL|A_OTI6500|\
                           A_TRIM_SC_PEND|A_DOOR_ACCESS|A_GATE|A_SCR_DISABLED|\
                           A_GATE_NOEXIT|A_GATE_ID_LITE|A_SHOWID_RETURN|\
                           A_SHOWID_AUTO|A_HG_GATE|A_ROUTE_MATRIX|A_OTI6000|A_CBID_FLT|\
                           A_RUN_TBL_OK|A_TRIP_TBL_OK|A_RANGE_TBL_OK|A_AVL_OFFLINE|\
                           A_STAND_ALONE|A_EXPANDED_SAL)

extern unsigned long  A2flags;
#define  Set_A2flag( f )          ( A2flags |= (f) )
#define  Clr_A2flag( f )          ( A2flags &=~(f) )
#define  Tst_A2flag( f )          ( A2flags &  (f) )


/*
 * A3flags
 */
#define  A_SMALL_CB       (unsigned long)0x00000001   // Use 31" cb full thresholds (both for 41")
#define  A_MEDIUM_CB      (unsigned long)0x00000002   // Use 36" cb full thresholds (both for 41")
#define  A_FASTFARE_E     (unsigned long)0x00000004   // Fast Fare-e (turns off H/W not present)
#define  A_BADLIST_IN_PCM (unsigned long)0x00000008   // Bad list in PCM flash file (FF and FF-e)
#define  A_J1708_MID_2    (unsigned long)0x00000010   // secondary J1708 MID enabled

#define  ALL_A3FLAGS      (unsigned long)(A_SMALL_CB|A_MEDIUM_CB|A_FASTFARE_E|A_BADLIST_IN_PCM|A_J1708_MID_2)


extern unsigned long  A2flags;

#define  Set_A3flag( f )      ( A3flags |= (f) )
#define  Clr_A3flag( f )      ( A3flags &=~(f) )
#define  Tst_A3flag( f )      ( A3flags &  (f) )

#endif /*_ABILITY_H*/
