/*----------------------------------------------------------------------------\
|  Src File:   octal.c                                                        |
|  Version :   1.00                                                           |
|  Authored:   04/22/96, tgh                                                  |
|  Function:   Process octal portb - b                                        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: 232p.c,v $                                                           |
|     1.00: 09/21/96, tgh - Initial release.                                  |
|                                                                             |
|           Copyright (c) 1993-2001  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

/* Project headers
*/
#include <gen.h>
#include <led.h>
#include <process.h>
#include <misc.h>
#include <probe.h>
#include <status.h>
#include <ability.h>


#define  RX_BUF_SZ      128                     /* read buffer size */
#define  TX_BUF_SZ      128                     /* transmit buffer size */

   /* Protocol defines */
int   RxIdle            ( byte );               /* protocol receive states */
int   GetData           ( byte );
int   Nputs             ( byte*, int );
int   Octal_bNputs      ( byte*, int );


#define  STX            0x02                    /* start of text */
#define  CR             0x0d                    /* end of text */
#define  RstNdx()       ( Ndx = 0 )
#define  NextRx( fptr ) (RxState    = (fptr))

static int     (*RxState)( byte )   = RxIdle;   /* current receive state */

#option sep_on class xdata

static byte    RxBuf[RX_BUF_SZ];                /* receive buffer */
static int     Ndx         =  0;                /* index into receive buffer */

#option sep_off

/*-------------------------------------------------------------------------\
|  Function:   Process_octalb();                                           |
|  Purpose :   Process debug port data.                                    |
|  Synopsis:   void  Process_Dbg( void );                                  |
|  Input   :   None.                                                       |
|  Output  :   None.                                                       |
|  Comments:   Called via system timer.                                    |
|                                                                          |
\-------------------------------------------------------------------------*/
void  Process_octalb()
{
   int   ch, n;

   n  = Octal_bCnt();
   n %= RX_BUF_SZ;

   while ( n-- )
   {
      ch = Octal_bGetc();
      if ( Ndx >= RX_BUF_SZ )
      {
         Tlog( "Rxbuf[%d] overrun\n", Ndx );
         IdleState();
      }
      (*RxState)( ch );
   }

}

static
int   Nputs( s, n )
byte        *s;
int             n;
{

   int   i;
/*
   Tlog( "TRANSMIT [" );
   for( i=0; i<n; i++ )
      if ( s[i] != CR )
         Tlograw( "%c", s[i] );
   Tlograw( "]\n" );
*/

   if ( Tst_Sflag( S_SPARES1 ) )
   {
      printf( "TRANSMIT [" );
      for( i=0; i<strlen(s); i++ )
        if ( s[i] != CR )
          printf( "%c", s[i] );
      printf( "]\n" );
   }

   return Octal_bNputs( s, n );

}


static
int   IdleState()
{
   NextRx( RxIdle );
   RstNdx();
   return 0;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|                P R O T O C O L   R E C E I V E   S T A T E S                |
|                                                                             |
\----------------------------------------------------------------------------*/
static
int   RxIdle( byte c )
{
   switch( c )
   {
      case  STX:
         NextRx( GetData );
         RstNdx();
         break;
      default:
         break;
   }
   return 0;
}

static
int   GetData( byte c )
{
   int   i;

   if ( c == CR )
   {
      printf("octl port ");
      for ( i = 0; i < Ndx; i++ )
        printf(" %X", RxBuf[i]);
      printf("\n");
      IdleState();
   }
   else
   {
      if ( Ndx < RX_BUF_SZ )
        RxBuf[Ndx++] = c;
      else if (c == STX )
        IdleState();
   }
}

int   OCTL_Msg()
{
   byte     buf[24];
   int      i;
   char     *p;

   memset( &buf, 0, sizeof(buf));
   p = &buf;

   *(p)     = STX;                 /* start with STX */
   *(p+1)   = 'H';
   *(p+2)   = 'E';
   *(p+3)   = 'L';
   *(p+4)   = 'L';
   *(p+5)   = 'O';
   *(p+6)   = ' ';
   *(p+7)   = 'O';
   *(p+8)   = 'U';
   *(p+9)   = 'T';
   *(p+10)  = ' ';
   *(p+11)  = 'T';
   *(p+12)  = 'H';
   *(p+13)  = 'E';
   *(p+14)  = 'R';
   *(p+15)  = 'E';
   *(p+16)  =  CR;
   i = strlen(p);
   Nputs( p, i );

}

/*-------------------------------------------------------------------------\
|  Function:   Process_octalc();                                           |
|  Purpose :   Process debug port data.                                    |
|  Synopsis:   void  Process_Dbg( void );                                  |
|  Input   :   None.                                                       |
|  Output  :   None.                                                       |
|  Comments:   Called via system timer.                                    |
|                                                                          |
\-------------------------------------------------------------------------*/
void  Process_octalc()
{
/*
   char          ch,c;
 */

}

