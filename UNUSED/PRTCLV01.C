/*----------------------------------------------------------------------------\
|  Src File:   prtclv01.c                                                     |
|  Authored:   08/28/01, tgh                                                  |
|  Function:   Protocol module.                                               |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   08/28/01, tgh  -  Initial release.                             |
|                                                                             |
|              Copyright (c) 2001-2002  GFI All Rights Reserved               |
\----------------------------------------------------------------------------*/
/* system headers
*/
#include <stdio.h>
#include <time.h>

/* windows headers
*/
#include <windows.h>

/* project headers
*/
#include <gen.h>
#include <tlog.h>
#include <timers.h>

/* Defines and typedefs
*/
#define  _DEBUG_COMM

   /* Protocol defines */
#define  STX               0x02                 /* start of text */
#define  ETX               0x03                 /* end of text */
#define  EOT               0x04                 /* end of transmission */
#define  ENQ               0x05                 /* enquiry */
#define  ACK               0x06                 /* acknowllege */
#define  NAK               0x15                 /* negative acknowllege */
#define  SYN               0x16                 /* start of msg. from D.S. */
#define  BELL              0x07                 /* line feed */
#define  LF                0x0a                 /* line feed */
#define  CR                0x0d                 /* carriage return */
#define  MAX_RETRIES       4                    /* protocol retries */
#define  RSPLEN            6                    /* response data length */
#define  T1                MSEC(500)            /* response time .5 sec. */
#define  T3                MSEC(250)            /* session timeout */
#define  POLLTIME          MSEC(500) 

#define  RX_BUF_SZ         81                   /* read buffer size */
#define  TX_BUF_SZ         81                   /* transmit buffer size */
#define  Q_BUF_SZ          81                   /* queue buffer size */
#define  MAXMSG            8                    /* max. queued messages */
#define  TXBUF_SIZE        MAXMSG*TX_BUF_SZ

/* Flags contained in "Flags" and macros to manipulate them.
*/
#define  F_OFFLINE         0x0001               /* key reader offline */


typedef  struct
{
   byte        b[Q_BUF_SZ];                     /* message buffer */
   int         sz;                              /* message size */
}  BUF;

typedef  struct
{
   BUF         q[MAXMSG];                       /* message queue */
   int         next;                            /* index to next */
   int         last;                            /* index to last */
   int         cnt;                             /* count of messages queued */
}  QUE;


/* Macros
*/
#define  RstNdx()          {Ndx = 0; RxBuf=RxQ.q[RxQ.next].b;}
#define  NextRx( fptr )    (RxState = (fptr))
#define  NextProc( fptr )  (ProcState = (fptr))
#define  SetFlag( s )      (Flags |= (s))
#define  ClrFlag( s )      (Flags &=~(s))
#define  TstFlag( s )      (Flags &  (s))


/* Prototypes
*/
void  IdleState            ( void );
int   Nq                   ( QUE* );
int   Dq                   ( QUE* );
int   Putc                 ( byte );
int   Puts                 ( byte* );
int   Nputs                ( byte*, int );
int   ProcFrame            ( void );
int   Frame                ( byte, byte, BUF*, byte );
void  Poll                 ( void );
void  RtoFnct              ( void );
void  RxIdle               ( byte );            /* receive state prototypes */
void  GetLF                ( byte );
void  GetData              ( byte );
void  ExpectSend           ( byte *p, int n );
void  IntoMainMenu         ( byte *p, int n );
void  IntoMenu0            ( byte *p, int n );
void  ExitNoSave           ( byte *p, int n );


/* external data
*/
extern   char  szProgName[];


/* global data
*/
         int         CommDebugFlag     =  0;
         int         CommDebugRXFlag   =  0;
         int         CommDebugTXFlag   =  0;

/* module data
*/
static   HANDLE      CommDev;                   /* communication device ID */
static   OVERLAPPED  ol;
static   int         Rto;                       /* response timer */
static   int         Sto;                       /* session timer inter-char. */
static   int         PollTmr;                   /* poll timer */

static   int         NackCnt        =  0;
static   int         PollTimeoutCnt =  0;
static   int         Retry;

static   void      (*RxState)();                /* receive state */
static   void      (*ProcState)( byte*, int ) = ExpectSend;/* process state */
static   int         ESndx          =  0;       /* index into Exp/Snd struct */
static   QUE         TxQ;                       /* tx message queue */
static   QUE         RxQ;                       /* rx message queue */

static   int         RxCnt;                     /* No. of data bytes to get */
static   byte        ChkSum;                    /* Current calc. checksum */

static   byte        TxBuf[ TXBUF_SIZE ];       /* Transmit Buffer */
static   int         TxNdx =  0;                /* Index into the tx buffer */
static   byte       *RxBuf;                     /* Pointer to receive buffer */
static   int         Ndx   =  0;                /* index into receive buffer */
static   word        Flags =  0;                /* maintained by macros below*/


/*----------------------------------------------------------------------------\
|                                                                             |
|              USER CALLABLE FUNCTIONS                                        |
|                                                                             |
\----------------------------------------------------------------------------*/

int   InitializeDevProtocol( dev )
HANDLE                       dev;
{
   CommDev  =  dev;
   memset( (char*)&ol, 0, sizeof(OVERLAPPED) );
   ol.hEvent=  CreateEvent( NULL, TRUE, FALSE, NULL );
   Rto      =  MkTmr();
   Sto      =  MkTmr();
   PollTmr  =  MkTmr();
   memset( (char*)&TxQ, 0, sizeof(TxQ) );
   memset( (char*)&RxQ, 0, sizeof(RxQ) );
   Flags =  F_OFFLINE;
   IdleState();
   InitTmr( PollTmr, Poll, POLLTIME );
   return 0;
}


int   DqFrame()
{
   return( Dq( &RxQ ) );
}

int   TxQCnt()
{
   return TxQ.cnt;
}

int   RxQCnt()
{
   return RxQ.cnt;
}

int   GetFrame( p )
byte          **p;
{
   int   n;

   if ( (n=RxQ.cnt) )
   {
      if ( p )
         *p = RxQ.q[RxQ.last].b;
      n  = RxQ.q[RxQ.last].sz;
   }

   return n;
}

int   ParseDev( s, n )
byte           *s;
int                n;
{
   int   i;

   #ifdef _DEBUG_COMM
   if ( CommDebugFlag || CommDebugRXFlag )
   {
      Tlog( "RX :\n" );
      TlogAsciiHexDump( s, n );
   }
   #endif

   for( i=0; i<n; i++ )
   {
      if ( Ndx >= RX_BUF_SZ )
         IdleState();
      if ( RxState )
         (*RxState)( s[i] );
   }

   return 0;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|              LOCAL FUNCTIONS                                                |
|                                                                             |
\----------------------------------------------------------------------------*/

static
int   BumpQndx( p )
int            *p;
{
   if ( *p < MAXMSG-1 )
      (*p)++;
   else
      *p = 0;

   return *p;
}

static
int   Nq( p )
QUE      *p;
{
   BumpQndx( &p->next );

   if ( p->cnt < MAXMSG )                       /* bump message count */
      p->cnt++;
   else
      BumpQndx( &p->last );

   return 0;
}

static
int   Dq( QUE *p )
{
   int   E = -1;

   p->q[p->last].sz = 0;                        /* set the size to zero */

   if ( p->cnt <= 0 )
   {
      p->cnt = 0;
      goto function_end;
   }

   p->cnt--;
   BumpQndx( &p->last );

   if ( p == &TxQ )                             /* bump S if transmit queue */
   {
      /*IncSeq( S ); */
   }

   function_end:
   return p->cnt;
}

int   TxDq()
{
   int   E = -1;

   TxQ.q[TxQ.last].sz = 0;                      /* set the size to zero */

   if ( TxQ.cnt <= 0 )
   {
      TxQ.cnt = 0;
      goto function_end;
   }

   TxQ.cnt--;
   BumpQndx( &TxQ.last );

   function_end:
   return TxQ.cnt;
}

int   Putc( byte c )
{
   DWORD bw;

#ifdef _DEBUG_COMM
   if ( CommDebugFlag || CommDebugTXFlag )
   {
      Tlog( "TX : %02x\n", c );
   }
#endif

   return WriteFile( CommDev, &c, 1, &bw, &ol );
}

int   Puts( s )
byte       *s;
{
   int   bw;
   int   n = lstrlen( s );

#ifdef _DEBUG_COMM
   if ( CommDebugFlag || CommDebugTXFlag )
   {
      Tlog( "TX :\n" );
      TlogAsciiHexDump( s, n );
   }
#endif

   return WriteFile( CommDev, s, n, &bw, &ol );
}

int   Nputs( s, n )
byte        *s;
int             n;
{
   int   bw;

#ifdef _DEBUG_COMM
   if ( CommDebugFlag || CommDebugTXFlag )
   {
      Tlog( "TX :\n" );
      TlogAsciiHexDump( s, n );
   }
#endif

   return WriteFile( CommDev, s, n, &bw, &ol );
}

static
void  IdleState()
{
   NextRx( RxIdle );
   RstNdx();
   LoadTmr( PollTmr, 1 );
}

static
int   ProcFrame()
{
   RxQ.q[RxQ.next].sz = Ndx;                    /* save no. of bytes received */
   Nq( &RxQ );                                  /* queue up frame received */

   return 0;
}

/*----------------------------------------------\
|  Function:   Poll()                           |
|  Purpose :   poll key reader.                 |
|  Synopsis:   void  Poll( void );              |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
static
void  Poll()
{
   if ( ESndx == 0 && !TmrRunning( Sto ) )
   {
      Putc( 'x' );
   }
   InitTmr( Rto, RtoFnct, T1 );
}


static
byte  CalcChkSum( p, n )
byte             *p;
int                  n;
{
   int   i;
   byte  cs=0;

   for( i=0; i<n; i++ )
     cs += *(p+i);

   return( cs );
}

static
byte  IntToBCDunchar( w )
word                  w;
{
   byte  result   =  0,
         n        =  (byte)w;

   if ( n > 99 ) goto function_end;

   result   =  n % 10;
   n       /=  10;
   result  |=  (n%10) << 4;

   function_end:
   return result;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|                             TIMER FUNCTIONS                                 |
|                                                                             |
\----------------------------------------------------------------------------*/
void  StoFnct()
{
   if ( Ndx >= 0 )
   {
      ProcFrame();
      ProcessFrames();
      RstNdx();
   }
   IdleState();
}

void  RtoFnct()
{
   if ( ++PollTimeoutCnt > MAX_RETRIES )
   {
      if ( !TstFlag( F_OFFLINE ) )
      {
         SetFlag( F_OFFLINE );
         /*Tlog( "%s: key reader offline\n", szProgName );*/
      }
      PollTimeoutCnt = 0;
   }
   IdleState();
}


/*----------------------------------------------------------------------------\
|                                                                             |
|                             RECEIVE STATES                                  |
|                                                                             |
\----------------------------------------------------------------------------*/

static
void  RxIdle( byte c )
{
   StopTmr( Rto );
   PollTimeoutCnt = 0;
   if ( TstFlag( F_OFFLINE ) )
   {
      ClrFlag( F_OFFLINE );
      /*Tlog( "%s: online\n", szProgName );*/
   }

   InitTmr( PollTmr, Poll, POLLTIME );

   switch( c )
   {
      case  CR:
      case  LF:
      case  BELL:
         break;
      default:
         RstNdx();
         RxBuf[Ndx++] = c;
         RxCnt = 0;
         NextRx( GetData );
         InitTmr( Sto, StoFnct, T3 );
         break;
   }
}


static
void  GetData( c )
byte           c;
{
   switch( c )
   {
      case  CR:
      case  LF:
      case  '?':
         StopTmr( Sto );
         ProcFrame();
         RstNdx();
         IdleState();
         break;
      default:
         RxBuf[Ndx++] = c;
         LoadTmr( Sto, T3 );
         break;
   }
   LoadTmr( Sto, T3 );
}



/*----------------------------------------------------------------------------\
|                                                                             |
|                          HIGH LEVEL RECEIVE STATES                          |
|                                                                             |
\----------------------------------------------------------------------------*/



typedef struct
{
   char *e;                                     /* expect string */
   char *s;                                     /* send   string */
}  EXPECTSEND;


#if 1

static   char  SSID[32] = "SPOKANE\r";
static   char  IP1[5]   = "10\r";
static   char  IP2[5]   = "1\r";
static   char  IP3[5]   = "3\r";
static   char  IP4[5]   = "103\r";

static   EXPECTSEND  ExpSnd[] =
   {
      "Press Enter",                   "\r",    /* get into setup mode */
      "Your choice",                   "0\r",   /* menu - 0 Server */
      "Network mode:",                 "1\r",   /* wireless */
      ")",                             IP1,     /* IP 1 */
      ")",                             IP2,     /* IP 2 */
      ")",                             IP3,     /* IP 3 */
      ")",                             IP4,     /* IP 4 */
      "Gateway",                       "\r",    /* gate address */
      "Netmask",                       "0\r",   /* default netmask */
      "Change telnet config password", "\r",    /* telnet password */
      "Your choice",                   "4\r",   /* menu - 4 WLAN */
      "Topology:",                     "1\r",   /* add-hoc */
      "Network name",                  SSID,    /* SSID */
      "Channel",                       "\r",    /* channel 11 */
      "Security suite:",               "0\r",   /* no security */
      "TX Data rate:",                 "0\r",   /* fixed TX rate */
      "TX Data rate:",                 "0\r",   /* 1 Mbit */
      "Your choice",                   "8\r",   /* exit without save */
      NULL,                            NULL
   };

#else

static   EXPECTSEND  ExpSnd[] =
   {
      "Press Enter",                   "\r",    /* get into setup mode */
      "Your choice",                   "0\r",   /* menu - 0 Server */
      "Network mode:",                 "1\r",   /* wireless */
      ")",                             "1\r",   /* IP 1 */
      ")",                             "1\r",   /* IP 2 */
      ")",                             "3\r",   /* IP 3 */
      ")",                             "103\r", /* IP 4 */
      "Gateway",                       "\r",    /* gate address */
      "Netmask",                       "0\r",   /* default netmask */
      "Change telnet config password", "\r",    /* telnet password */
      "Your choice",                   "4\r",   /* menu - 4 WLAN */
      "Topology:",                     "1\r",   /* add-hoc */
      "Network name",                  "SPOKANE\r", /* SSID */
      "Channel",                       "\r",    /* channel 11 */
      "Security suite:",               "0\r",   /* no security */
      "TX Data rate:",                 "0\r",   /* fixed TX rate */
      "TX Data rate:",                 "0\r",   /* 1 Mbit */
      "Your choice",                   "8\r",   /* exit without save */
      NULL,                            NULL
   };

#endif



static
void  ExpectSend( byte *p, int n )
{
   if ( strstr( p, ExpSnd[ESndx].e ) )
   {
      Puts( ExpSnd[ESndx].s );
      ESndx++;
   }
   if ( ExpSnd[ESndx].e == NULL && ExpSnd[ESndx].s == NULL )
   {
      InitializeProcState();
   }
}


int   InitializeProcState()
{
   NextProc( ExpectSend );
   ESndx = 0;
   return 0;
}


int   ProcessFrames()
{
   int   n;
   char *s;
   char  buf[256];

   while( RxQCnt() )
   {
      n     =  GetFrame( &s );
      s[n]  =  '\0';
      strncpy( buf, s, sizeof(buf)-1 );
      Tlog( "%s: [%s]\n", szProgName, buf );
      DqFrame();
      if ( ProcState )
         (*ProcState)( buf, n );
   }

   return 0;
}



