/*----------------------------------------------------------------------------\
|  Src File:   data.h                                                         |
|  Authored:   11/29/94, tgh                                                  |
|  Function:   Defines & prototypes for misc. data functions.                 |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   11/29/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _DATA_H
#define  _DATA_H

#include <time.h>

/* Typedefs
*/

typedef struct
{
  word  transfer  : 4;
  word  fare      :12;
} FARE;

/* Defines
*/
#define  SIGNITURE 0xfeedf00d

#pragma sep_on segment S_Cnf

#pragma sep_off

/* Prototypes
*/

#endif /*_DATA_H*/
