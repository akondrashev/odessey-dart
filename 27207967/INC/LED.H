/*----------------------------------------------------------------------------\
|  Src File:   led.h                                                          |
|  Authored:   01/11/96, tgh                                                  |
|  Function:   Defines & prototypes for led port.                             |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/11/96, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993-1996  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _LED_H
#define  _LED_H

#define  LED_WATCHDOG   0x08
#define  LED_THREE      0x04
#define  LED_TWO        0x02
#define  LED_ONE        0x01 
#define  LED_ALL        0x0f

#define  BATVD          0x40 
#define  BATT_CHECK     0x40

#define  A21            0x80 /* pc low/hi address bit for 4 meg */
#define  PC_A21         0x80 

#endif
