*-----------------------------------------------------------------------------+
*  Src File:   fpga.inc                                                       |
*  Authored:   10/15/96, sjb                                                  |
*  Function:   fpga equates for new farebox logic board.                      |
*  Comments:                                                                  |
*                                                                             |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   10/15/96, sjb  -  Initial release.                             |
*                                                                             |
*           Copyright (c) 1993-1996  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

***********   FLASH RAM equates   **************************************

          flash:        equ     $000000
          boot_beg:     equ     flash           ;boot block start address
          param1_beg:   equ     flash+$4000     ;param block 1 start address
          param2_beg:   equ     flash+$6000     ;param block 2 start address
          main1_beg:    equ     flash+$8000     ;main block 1 start address
          main2_beg:    equ     flash+$20000    ;main block 2 start address
          main3_beg:    equ     flash+$40000    ;main block 3 start address
          main4_beg:    equ     flash+$60000    ;main block 4 start address

          
*****    FPGA output port equates    *****          
          
          
          led:    equ     $600003
          dispdat:equ     $600004
          dispad: equ     $600005
          fpga2:  equ     $600006
          encdr:  equ     $600001

          key:    equ     $700004
          card:   equ     $700003

*********  QSPI transmit RAM equates, Brute Force Method  ******
          tr0:    equ     tran_ram
          tr01:   equ     tr0+$001
          tr1:    equ     tr0+$002
          tr2:    equ     tr0+$004
          tr3:    equ     tr0+$006
          tr4:    equ     tr0+$008
          tr5:    equ     tr0+$00a
          tr6:    equ     tr0+$00c
          tr7:    equ     tr0+$00e
          tr8:    equ     tr0+$010
          tr9:    equ     tr0+$012
          tr10:   equ     tr0+$014
          tr11:   equ     tr0+$016
          tr12:   equ     tr0+$018
          tr13:   equ     tr0+$01a
          tr14:   equ     tr0+$01c
          tr15:   equ     tr0+$01e

********  QSPI Command RAM equates, Brute Force Method  ******



          cram0:  equ     comd_ram
          cram1:  equ     cram0+$001
          cram2:  equ     cram0+$002
          cram3:  equ     cram0+$003
          cram4:  equ     cram0+$004
          cram5:  equ     cram0+$005
          cram6:  equ     cram0+$006
          cram7:  equ     cram0+$007
          cram8:  equ     cram0+$008
          cram9:  equ     cram0+$009
          cram10: equ     cram0+$00a
          cram11: equ     cram0+$00b
          cram12: equ     cram0+$00c
          cram13: equ     cram0+$00d
          cram14: equ     cram0+$00e
          cram15: equ     cram0+$00f




        chopcnt:  equ     $135000
        cboxidcnt:equ     $135002
        cboxidtmr1:equ    $135004
        cboxidtmr2:equ    $135006
