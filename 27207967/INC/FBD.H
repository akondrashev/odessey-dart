/*----------------------------------------------------------------------------\
|  Src File:   fbd.h                                                          |
|  Authored:   01/01/97, tgh                                                  |
|  Function:   Defines, prototypes & macros for farebox data (aggregate).     |
|  Comments:   Field names are composed of a prefix followed by an            |
|              underscore and 1 character field type, where :                 |
|                 _c  = count   (current count)                               |
|                 _e  = errors  (current errors)                              |
|                 _m  = map     (bitmap)                                      |
|                 _n  = number  (i.e. bus number, fbx number, ...)            |
|                 _r  = revenue                                               |
|                 _t  = total   (cumulative fields, may be added to any field)|
|                 _x  = cumulative errors                                     |
|                 _v  = version number                                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/01/97, tgh  -  Initial design.                              |
|      1.01:   01/20/97, sjb  -  add driver no. to event structure            |
|                                                                             |
|           Copyright (c) 1993-1997  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _FBD_H
#define  _FBD_H

#include "gen.h"
#include "transact.h"

/* defines and typedefs
*/

/* #define  MAX_EV         250                        /* maximum no. of events */
#define  MAX_EV         500                        /* maximum no. of events */
#define  EV_SZ          100                        /* raw event size */

#define  Ev_IDLE_EVENT       0
#define  Ev_CB_OUT           1 /* Cashbox out                     */
#define  Ev_CB_IN            2 /* Cashbox in                      */
#define  Ev_DRIVER_NO        3 /* Driver number entered           */
#define  Ev_DRIVER_OUT       4 /* Driver log out                  */
#define  Ev_MIDNIGHT         5 /* Midnight                        */
#define  Ev_RR_OVERFLOW      6 /* Route/Run overflow              */
#define  Ev_IN_BYPASS        7 /* Coin placed in bypass           */
#define  Ev_BYPASS_OFF       8 /* Coin removed from bypass        */
#define  Ev_UAN_DOOR_OPEN    9 /* Unauthorized door open          */
#define  Ev_UAN_DOOR_CLSE   10 /* Unauthorized door closed        */
#define  Ev_UAN_CB_OUT      11 /* Unauthorized Cashbox out        */
#define  Ev_ALARM_TMEOUT    12 /* Door open or CB out too long    */
#define  Ev_COLD_START      13 /* Cold Start                      */
#define  Ev_BAD_CLOCK       14 /* Battery Bad                     */
#define  Ev_EL_KEY          15 /* Electronic key used             */
#define  Ev_TIME_ERROR      16 /* */
#define  Ev_POWER_RESTORED  17 /* Power restored                  */
#define  Ev_CB_NOT_REMOVED  18 /* Cashbox not removed             */
#define  Ev_CB_NO_REV       19 /* Cashbox out no revenue          */
#define  EV_TRIM_OFFLINE    20 /* TRIM unit offline               */
#define  Ev_CBID_MEMCLR     21 /* Memory clear with Cashbox       */
#define  Ev_NO_CBID_MEMCLR  22 /* Memory clear no Cashbox         */
#define  Ev_CB_FULL         23 /* Cashbox full                    */
#define  EV_TRIM_ONLINE     24 /* TRIM unit online                */
#define  Ev_MAIN_ID         25 /* Maintenance                     */
#define  Ev_FTBLE_CHANGE    26 /* Faretable change                */
#define  Ev_BILL_M_ERR      27 /* Bill mech error                 */
#define  Ev_COIN_M_ERR      28 /* coin mech error                 */
#define  Ev_PART_MEMCLR     29 /* */
#define  Ev_DOOR_CLOSED     30 /* Cashbox door closed             */
#define  Ev_PDU_PROBED      31 /* Probed by PDU                   */
#define  Ev_BAD_LST_PASS    32 /* Badlisted pass used             */
#define  Ev_DOOR_FAULT      33 /* Probed, Cashbox door not open   */
#define  Ev_AUT_MEMCLR      34 /* Automatic memory clear          */
#define  Ev_PRB_BYPASS_ON   35 /* Probed while in bypass          */
#define  Ev_AUT_LOG_OFF     36 /* Auto log off                    */
#define  Ev_CLOCK_FAULT     37 /* Clock fault detected            */
#define  Ev_MAIN_CRD        38 /* Maintenance Card used           */
#define  Ev_MAIN_CRD_CLR    39 /* */
#define  Ev_DRIVER_CRD      40 /* Driver card used                */
#define  Ev_CBID_LOST_COM   43 /* Lost comm w/CBID while powered  */
#define  Ev_CBID_LOST_PWUP  44 /* No CBID detect on powerup door closed */
#define  Ev_PULLER_ID       45 /* Puller ID card used             */
#define  Ev_TRIMSTOCK_LOW   46 /* Low TRIM stock                  */
#define  Ev_TRIM_BYPASSON   47 /* TRIM placed in bypass           */
#define  Ev_TRIM_BYPASSOFF  48 /* TRIM removed from bypass        */
#define  Ev_MANUAL_EVENT    49 /* Manual Route/Run created        */
#define  Ev_HOURLY_EVENT    50 /* Hourly event created            */
#define  Ev_MENU_ENTRY_EVT  51 /* Menu entry                      */
#define  Ev_PROBE_EVENT     52 /* Probed                          */
#define  Ev_TRIMSTOCK_ADD   53 /* TRIM stock added                */
#define  Ev_LID_OPEN        54 /* Farebox lid opened              */
#define  Ev_LID_CLOSED      55 /* Farebox lid closed              */
#define  Ev_PERIODIC        56 /* Periodic event                  */
#define  Ev_DOOR_MEMCLR     57 /* CB door open when memory clear  */
#define  Ev_LID_MEMCLR      58 /* Lid open when memory clear      */
#define  Ev_BYPASS_MEMCLR   59 /* In bypass when memory clear     */
#define  Ev_NEWFARE         60 /* Future Fareset change           */
#define  Ev_BAD_DRIVER_NO   61 /* Bad driver number entered       */
#define  Ev_BAD_ROUTER_NO   62 /* Bad route number entered        */
#define  Ev_LOST_CARD       63 /* TRIM card lost - Cleveland      */
#define  Ev_BCR_OFFLINE     64 /* bar code reader offline         */
#define  Ev_BCR_ONLINE      65 /* bar code reader online          */
#define  Ev_BCP_OFFLINE     66 /* bar code printer offline        */
#define  Ev_BCP_ONLINE      67 /* bar code printer online         */
#define  Ev_NO_TRIMSTOCK    68 /* No TRIM stock                   */
#define  Ev_FFTBLE_CHNAGE   69 /* Future Faretable change         */

typedef struct
{
   long     cbx_n;                  /* cashbox ID                            */
   long     prb_t;                  /* cummulative probe count               */
   long     warm_c;                 /* warm starts                           */
   long     cold_c;                 /* cold starts                           */
   long     coin_c[7];              /* TS, dim, pny, nkl, qrt, SBA, hlf      */
   long     coin_e[10];             /* DD, DP, PP, DN, PN, DQ, PQ, NQ, QQ, TB*/
   long     coin_t;                 /* cummulative coin count                */
   long     bill_t;                 /* cummulative bill count                */
   long     bill_c[4];              /* 1's, 5's, 10's, 20's                  */
   long     pass_t;                 /* cummulative passes                    */
   long     pass_x;                 /* cummulative pass misreads             */
   long     coin_x;                 /* cummulative coin errors               */
   long     evnt_c;                 /* event count                           */
   long     est_r;                  /* estimated revenue                     */
   long     mag_r;                  /* magnetic revevnue                     */
   long     tot_r;                  /* total revenue                         */
   long     fare_t[FS];             /* cummulative full fare count/fare set  */
   long     key_t[KEYS];            /* cummulative keys 1 - D                */
   long     ttp_t[TTP];             /* cummulative ttps                      */
   long     _1bill_c;               /* dollar bill count                     */
   long     trim_v;                 /* trim version                          */
   long     trim_n;                 /* trim number                           */
   long     tot_rt;                 /* cummulative total revenue ??          */
   long     diff_r;                 /* differential revenue                  */
   long     uncl_r;                 /* unclassified (dump) revenue           */
   long     dump_c;                 /* dump count                            */
   long     est_rt;                 /* cummulative estimated revenue         */
   long     mag_rt;                 /* cummulative magnetic revenue          */
   long     fare_r;                 /* full fare count                       */
   long     fare_c[KEYS];           /* full fare key count / fare set        */
   long     ttp_c[TTP];             /* ttp count                             */
   long     totbill_c;              /* total bill count                      */
   long     pass_c;                 /* pass count                            */
   long     pass_e[5];              /* msrd, psbk, invld, exp, bdlst         */
   long     fbx_v;                  /* farebox version                       */
   long     bus_n;                  /* bus number/gate number                */
   long     fbx_n;                  /* farebox number                        */
}  ML;

typedef struct
{
   time_t   t;                      /* event time stamp */
   long     evnt_n;                 /* event number */
   long     driver_n;               /* driver number */
   long     maint_n;                /* maint number */
   long     route_n;                /* route number */
   long     run_n;                  /* run   number */
   long     trip_n;                 /* trip  number */
   long     stop_n;                 /* stop  number */
   long     city_n;                 /* city  number */
   byte     cut_t[2];               /* cut time     */
   byte     cut_t2[2];              /* cut time     */
   word     curr_r;                 /* current revenue */
   word     est_r;                  /* estimated revenue */
   word     mag_r;                  /* magnetic revenue */
   word     uncl_r;                 /* unclassified (dump) revenue */
   byte     dir;                    /* current direction */
   byte     fs;                     /* current fareset */
   byte     type;                   /* event type */
   byte     dump_c;                 /* dump count */
   byte     fare_c;                 /* full fare count */
   byte     key_c[KEYS];            /* key counts */
   byte     ttp_c[TTP];             /* ttp counts */
   byte     spare[4];               /* spare      */
   byte     bill_c;                 /* bill count */
}  EV;

typedef struct
{
   word     penny;
   word     nickel;
   word     dime;
   word     quarter;
   word     half;
   word     dollar;
   word     two_dollar;
   word     tokens;
   word     bill_1;
   word     bill_2;
   word     bill_5;
   word     bill_10;
   word     bill_20;
   word     bill_50;
   word     bill_100;
   word     tickets;
} CASH_BOX;

/* external data
*/
#pragma sep_on segment S_MlEv                   /* defined in "data_vxx.c" */
extern  ML               Ml;                    /* current master list     */
extern  char             Fs_v[3];
extern  EV               Ev;                    /* current event record    */
extern  int              MaxEv;
extern  int              RS485baud;
extern  int              SC_READER;
extern  int              Trim_Baud;
extern  int              EV_Time_fb;       /* 1 = use fb event time */
extern  int              FB_EV_time;       /* time at farebox for ev time */
extern  int              Ev_Timer;
extern  int              Ev_Timer_Counter;
extern  int              Door_time;        /** AAT - 10/17/08: Door access **/
extern  ulong            Crc_cs;
extern  int              Crc_lp;
extern  CASH_BOX         Cbox;
extern  ulong            GateEntries;
extern  ulong            GateExits;
extern  int              StationBaud;
extern  int              FB_Auto_Accept;
#pragma sep_off

#pragma sep_on segment Flash_Type          /* defined in "data_vxx.c" */
extern  word     Flash_Select;
extern  word     Mfg_ID;
#pragma sep_off

extern  CB_Out;

/* Prototypes
*/
int      ClrMlEv        ( int );
int      NewEv          ( int, long );
long     MlEvPtrs       ( char**, char** );
int      InitializeCnf  ( void );
EV      *EvPtr          ( int );
int      EvCnt          ( void );
byte     Event_Type     ( int );

#endif /*_FBD_H*/


