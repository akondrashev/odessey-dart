/*----------------------------------------------------------------------------\
|  Src File:   HID.h                                                          |
|  Authored:   06/01/09, aat                                                  |
|  Function:                                                                  |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:                                                                  |
|                                                                             |
|           Copyright (c) 2009  GFI All Rights Reserved                       |
\----------------------------------------------------------------------------*/
#ifndef  _HID_H
#define  _HID_H

/* System headers
*/
#include <stdio.h>

/* Project headers
*/


/* Defines
*/
#define  UNIT_ID               0x00
/* #define  STX    0x02  /* Start of Text             - Defined in Scard.h */
/* #define  ETX    0x03  /* End of Text               - Defined in Scard.h */
/* #define  ACK    0x06  /* Possitive Acknowledgement - Defined in Scard.h */
/* #define  NAK    0x15  /* Negative Acknowledgement  - Defined in Scard.h */

#define  HID_STX              "\x2"
#define  STATION_ID           0x01  /* use 0xff to broad case to all readers */
                                    /* if multiple readers, then need to use */
                                    /* additional message to get all ID's    */


/*
** Commands
*/

/* Common Commands */
#define  HID_TEST             "!"   /* Test continuous read / Check KTT upload status */
#define  HID_POLL             "c"   /* Continuous Read */
#define  HID_STOP_POLL        "."   /* Abort continuous read */
#define  HID_LED_GRN          "dg"  /* Switch on LED Green, LED Red off */
#define  HID_LED_RED          "dr"  /* Switch on LED Red, LED Green off */
#define  HID_LED_OFF          "dn"  /* Switch both LED"s off */
#define  HID_DES_ENCRYPT      "ds"  /* DES encryption / decryption of data */
#define  HID_GET_ID           "g"   /* Get readers "Station ID" */
#define  HID_HIGH_SPEED_SEL   "k"   /* */
#define  HID_MULTITAG_SEL     "m"   /* */
/** Page 48: Include/Exclude/Set tag type **/
#define  HID_SET_CONFIG_FLAGS "of"
#define  HID_SET_CONFIG_REG   "og"
#define  HID_READ_ALL_REG     "ox"
#define  HID_ANTENNA_OFF      "poff"
#define  HID_ANTENNA_ON       "pon"
#define  HID_USER_PORT_READ   "pr"
#define  HID_USER_PORT_WRITE  "pw"
#define  HID_QUIET            "q"
#define  HID_RESEND_LAST_ANSW "ra"
#define  HID_READ_BLOCK       "rb"  /* or "r" */
#define  HID_READ_MULTIPLE    "rd"
#define  HID_READ_EEPROM_REG  "rp"  /* Read EEPROM registers */
#define  HID_SELECT           "s"
#define  HID_GET_VER          "v"
#define  HID_WRITE_BLOCK      "wb"  /* or "w" */
#define  HID_WRITE_MULTIPLE   "wd"
#define  HID_WRITE_EEPROM_REG "wp"
#define  HID_RESET            "x"
#define  HID_FIELD_RESET      "y"
/* ISO 14443 Type A (MIFARE) only commands */
#define  HID_INC_VAL_BLOCK    "+"
#define  HID_DEC_VAL_BLOCK    "-"
#define  HID_COPY_VAL_BLOCK   "="
#define  HID_LOGIN            "l"
#define  HID_READ_VAL_BLOCK   "rv"
#define  HID_WRITE_VAL_BLOCK  "wv"
/* Key Management */
#define  HID_AUTHENTICATE     "ar"
#define  HID_GET_KEY_ACESS    "ia"
#define  HID_GET_KEY_STATUS   "it"
#define  HID_RESET_KEY_TABLE  "rt"
#define  HID_UPDATE_KEY_ACCESS"ua"
#define  HID_CHANGE_KEY_TYPE  "uc"
#define  HID_UPDATE_KEY       "uk"
/* my-d Secure commands */
#define  HID_CHECK_KTT_UPLOAD "!" /* Check KTT upload status / Test continuous read */
#define  HID_ABORT_KTT_UPLOAD "*"
#define  HID_AUTHENT_SECTOR   "as"
#define  HID_ISSUE_TRANS_KEY  "ik"
#define  HID_PREP_FOR_KTT     "ut"
#define  HID_MY_D_COMMAND     "z"

/*
** Error Codes 
*/
#define  HID_ERR_UNKNOWN_CMD  '?'
#define  HID_ERR_COLLISION    'C'
#define  HID_ERR_GEN_FAILURE  'F'
#define  HID_ERR_INV_VAL_FORMT'I'
#define  HID_ERR_NO_TAG       'N'
#define  HID_ERR_OPER_MODE_FAIL'O'
#define  HID_ERR_CMD_PARAMETER'R'
#define  HID_ERR_AUTH_FAILURE 'X'
/*
#define  HID_""
#define  HID_""
#define  HID_""
*/

#define  REG_DEVICE_ID        0x00
#define  REG_ADMIN_DATA       0x05
#define  REG_STATION_ID       0x0a
#define  REG_PROTOCOL_CFG1    0x0b
   #define  AUTO_START     0x01
   #define  EXTENDED_PROTO 0x02
   #define  MULTI_TAG      0x04
   #define  NEW_SN_MODE    0x08
   #define  LED_CTRL       0x10
   #define  SINGLESHOT     0x20
   #define  ISO14443_4     0x40
   #define  EXTENDED_ID    0x80

#define  REG_BAUDRATE         0x0c     /* 8.3.5 - Baud Rate                   */
#define  REG_CMD_GUARD_TM     0x0d     /* 8.3.6 - Command Guard Time          */
#define  REG_OPMODE           0x0e     /* 8.3.7 - Operating Mode Register     */
   /* Supported "tag" types */
   #define  ISO_14443A     0x01        /* Innovision Jewel part of ISO14443A  */
   #define  ISO_14443B     0x02
   #define  SR176          0x04
   #define  ICODE          0x08
   #define  ISO_15693      0x10
   #define  ICODE_EPC      0x20
   #define  ICODE_UID      0x40
   #define  RFU            0x80

#define  REG_PROTOCOL_CFG2    0x13
#define  REG_PROTOCOL_CFG3    0x1b

/*
** Responses
*/
#define  HID_LF               0x0a
#define  HID_CR               0x0d
#define  KEY_A                0xaa
#define  KEY_B                0xbb
#define  KEY_F                0xff

/**/

/*
** Block Definitions
*/
#define  MGF_BLOCK             0


extern word  SupportedTags;

/* Prototype
*/
void  HID_reset         ( void );

#endif /* __HID_H_ */
