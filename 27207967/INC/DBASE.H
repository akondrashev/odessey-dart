/*----------------------------------------------------------------------------\
|  Src File:   dbase.h                                                        |
|  Authored:   10/18/93, rz, cj                                               |
|  Function:   Defines & prototypes for database interface.                   |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|      1.01:   11/02/00, sjb  -  modify Rat to handle extended memory         |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _DBASE_H
#define  _DBASE_H


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#if !defined(byte)
typedef unsigned char byte;
#endif

#if !defined(word)
typedef unsigned int word;
#endif


/*
** Macros
*/
#define DB_SIG   0xDEED
#define DB_SYNC  (char)0xFE            /* First byte of stored transaction */
#define OVERHEAD 3                     /* 3 bytes, DB_SYNC, Tsize and LRC  */

/*
** External Data Definitions
*/
#option sep_on segment S_DBase
   extern word RatIdx;
   extern long Rat[];             /* version 1.01 */
   extern word DBsig;
#option sep_off


/*
** Function Prototypes
*/
int  DBinit( void );
void RATinit( void );
void DBreinit( void );
int  DBadd( void *T, int Tsize );
long DBfreemem( void );
long DBgetptrs( char **top, char **bottom );
void DBsetptrs( char *cur );
int  DBfilling( int percent );
int  db_restore_card( void *serial );
byte DBlrc ( byte * s, word l );
ulong TranactionSize( void );

#endif /*_DBASE_H*/
