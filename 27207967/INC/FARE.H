/*----------------------------------------------------------------------------\
|  Src File:   fare.h                                                         |
|  Authored:   03/04/96, sjb                                                  |
|  Function:   Defines & macros for processing fares.                         |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/96, sjb  -  Initial release.                             |
|           Copyright (c) 1994 - 1996 GFI All Rights Reserved                 |
\----------------------------------------------------------------------------*/
#ifndef  _FARE_H
#define  _FARE_H

extern    word    MagneticValue;  /* magnetic fare value from card     */
extern    word    Magval;         /* magnetic fare processed from card */
extern    word    XferVal;        /* magnetic xfer val from card */
extern    int     CurrentFare;    /* current selected fare ( full, key, pass)  */
extern    word    MagFormat;
extern    word    ShortFareMagFormat; /* media type on short fare */
extern    int     MagGroup;       /* magnetic group (period, stored ride ..ect */
extern    int     DumpFlag;
extern    int     ByPassKey;      /* used to process full fares in bypass      */
extern    int     OverRideKey;    /* transfer override key */
extern    int     ReadCardKey;    /* transfer override key */
extern    int     RestoreCardKey; /* transfer override key */
extern    word    Xfer_Key;       /* transfer issued via xfer key */
extern    int     Pending_xfer;   /* pending xfer from trim card */
extern    word    MultiFareVal;
extern    int     TallyDumpFlag;   /* used on issue xfer after fare paid */

extern    word    Dump_Revenue;
extern    int     CardPending;     /* card pending from trim bypass */

extern    B_SERIAL  PassSerial;
extern    B_SERIAL PassSerial_ID;  /* Photo id serial number */
extern    int      Prime_ID;       /* photo id ( 0 - 99)     */
extern    word     Photo_ID_index; /* photo id index (ttp)   */
extern    int      Pass_Photo_ID;  /* photo id ( 0 - 99)     */
                                   /* used to validate next card */
extern    BFARE    PendingFare;
extern    T_FARE   t_fare;

extern    word     LastFarePaid;
extern    int      FareDealPending;
extern    int      SC_Pending_Xfer;
extern    int      SC_Fare_index;

typedef struct
{
   int  OrgVal;
   int  ValDed;
}  SC_FARE;

extern    SC_FARE   SC_Fare;

#define   MAX_ESCROW     10000       /* maximum drivers' revenue befor dumping */
#define   DUMP_OVERFLOW  -2          /* dump overflow of driver's revenue      */
#define   T_DUMP_C       -1          /* Tally dump and clear driver's revenue  */

/* #define PROTOTYPE */
void RejectFare       ( void );
void FareProcess      ( void );
void CountFare        ( int );
void AdjustValue      ( int, word, int );
void AdjustVal        ( void*, word, int );
void Check_Lite_Sound ( void* );
void DisplayDelay     ( int );
void PreSound         ( void* );
void PostSound        ( void* );
void Check_Lites_Only ( void* );
void IssueTransfer    ( void* );

#endif /*_FARE_H*/



