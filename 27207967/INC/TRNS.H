/*----------------------------------------------------------------------------\
|  Src File:   trns.h                                                         |
|  Authored:   08/19/97, tgh                                                  |
|  Function:   version 7 transactions.                                        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   08/19/97, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1994-1997  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef _TRNS_H
#define _TRNS_H

/* system headers
*/

/* project headers
*/

/* defines
*/
               /* transaction codes */

#define TC_PowerUp            0x01              /* power up                  */
#define TC_Sync               0x02              /* midnight sync             */
#define TC_TimeStamp          0x03              /* hourly time stamp         */
#define TC_Driver             0x04              /* driver number change      */
#define TC_Fareset            0x05              /* fare set change           */
#define TC_Route              0x06              /* route change              */
#define TC_Run                0x07              /* run change                */
#define TC_Trip               0x08              /* trip change               */
#define TC_Direction          0x09              /* direction change          */
#define TC_Stop               0x0a              /* stop change               */
#define TC_Cut                0x0b              /* cut time change           */
#define TC_ReadyForRevenue    0x0c              /* driver / employee login   */
#define TC_MaintenanceCode    0x0d              /* maintenance               */
#define TC_StoredRide         0x0e              /* stored ride card          */
#define TC_PeriodPass         0x0f              /* period pass card          */
#define TC_StoredValue        0x10              /* stored value card         */
/* #define "not defined"      0x11              /* not defined               */
#define TC_ReceiveTransfer    0x12              /* receive transfer          */
#define TC_GotFare            0x13              /* got fare                  */
#define TC_BypassDmp          0x14              /* bypass dump               */
#define TC_EscrowDmp          0x15              /* escrow dump               */
#define TC_EscrowTmo          0x16              /* escrow time out           */
#define TC_GPT                0x17              /* general purpose           */
#define TC_BadCard            0x18              /* badlist card              */
#define TC_LogIn              0x19              /* gen. purpose              */
#define TC_RestoreCard        0x1a              /* restore card              */
#define TC_ReadCard           0x1b              /* read card                 */
#define TC_IssueTransfer      0x1c              /* issue transfer            */
/* #define "not defined"      0x1d              /* not defined               */
#define TC_CashBox            0x1e              /* cash box                  */
#define TC_ElectronicKey      0x1f              /* electronic key            */
#define TC_POST               0x20              /* power on self test        */
#define TC_TTP                0x21              /* single TTPs               */
#define TC_RevStat            0x22              /* revenue status            */
#define TC_MasterList         0x79              /* master list               */

   /* misc. */
#define  MSNS                 8                 /* mag. serial number size   */
#define  FE                   15                /* fare entries              */

   /* flags for magnetic media transactions */
#define  mf_PASSBACK          0x0001            /* passback attempt          */
#define  mf_AGENCY            0x0002            /* bad agency                */
#define  mf_SECURITY          0x0004            /* bad security code         */
#define  mf_BADLIST           0x0008            /* badlisted pass            */
#define  mf_UNLISTED          0x0010            /* unlisted pass             */
#define  mf_DISABLED          0x0020            /* disabled via media list   */
#define  mf_INVFMT            0x0040            /* invalid format            */
#define  mf_EXPIRED           0x0080            /* expired                   */
#define  mf_PEAK              0x0100            /* peak/off peak attempt     */
#define  mf_SPARE1            0x0200            /* spare                     */
#define  mf_WEEKDAY           0x0400            /* weekday/weekend attempt   */
#define  mf_SPARE2            0x0800            /* spare                     */
#define  mf_SPARE3            0x1000            /* spare                     */
#define  mf_HOLIDAY           0x2000            /* holiday attempt           */
#define  mf_OVERRIDE          0x4000            /* override                  */
#define  mf_3RDPARTY          0x8000            /* store 3rd party of zero   */

#ifdef _MSC_VER

#pragma  pack(1)

#endif

/* typedefs
*/
typedef struct                                  /* common transaction header */
{
   byte     type;                               /* transaction type          */
   byte     seq;                                /* sequence number           */
   time_t   t;                                  /* timestamp                 */
}  TS_HDR;

typedef struct                                  /* driver change             */
{
   TS_HDR   h;                                  /* header                    */
   long     n;                                  /* driver number             */
}  TS_Driver;

typedef struct                                  /* fare set change           */
{
   TS_HDR   h;                                  /* header                    */
   byte     n;                                  /* fare set                  */
}  TS_Fareset;

typedef struct                                  /* route change              */
{
   TS_HDR   h;                                  /* header                    */
   long     n;                                  /* route number              */
}  TS_Route;

typedef struct                                  /* run change                */
{
   TS_HDR   h;                                  /* header                    */
   long     n;                                  /* run number                */
}  TS_Run;

typedef struct                                  /* trip change               */
{
   TS_HDR   h;                                  /* header                    */
   long     n;                                  /* trip number               */
}  TS_Trip;

typedef struct                                  /* direction change          */
{
   TS_HDR   h;                                  /* header                    */
   byte     n;                                  /* direction                 */
}  TS_Direction;

typedef struct                                  /* ready for revenue         */
{
   TS_HDR   h;                                  /* header                    */
}  TS_ReadyForRevenue;

typedef struct                                  /* period pass               */
{
   TS_HDR   h;                                  /* header                    */
   word     mf;                                 /* media flags(defined above)*/
   byte     sn[MSNS];                           /* Serial Number (8 bytes)   */
}  TS_PeriodPass;

typedef struct                                  /* revenue status            */
{
   TS_HDR   h;                                  /* header                    */
   byte     key[FE];                            /* key counts                */
   word     rev;                                /* revenue                   */
}  TS_RevStat;

#ifdef _MSC_VER

#pragma  pack()

#endif

/* prototypes
*/

#endif
