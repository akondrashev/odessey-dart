/*----------------------------------------------------------------------------\
|  Src File:   SCmenu.h                                                         |
|  Authored:   09/20/94, cj, rz                                               |
|  Function:   Defines & prototypes for diagnostic menu.                      |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  changed header.                              |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _SCMENU_H
#define  _SCMENU_H


extern int MenuState;

void menu(void);

/* I don't know what this is */
#ifdef IS_MENU

/*
** Function Prototypes
*/
void menu(void);
void menu_usage( void );

void change_date_time( void );
void memory_dump( void );
void fs_dump( void );
void crc16_block( void );
void enter_notes( void );
void view_diag( void );
void clear_delta( void );
void data_base_stat( void );
void farebox_dump_on( void );
void farebox_dump_off( void );
void dummy_fare( void );

/*
** External Refrences
*/
extern SCMenu SystemMenu[];
extern SCMenu CardMenu[];
extern SCMenu MaintMenu[];

#endif /*IS_SCMENU*/

#endif /*_MENU_H*/
