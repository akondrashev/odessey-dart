/*----------------------------------------------------------------------------\
|  Src File:   rs422.h                                                        |
|  Authored:   03/14/95, tgh                                                  |
|  Function:   Defines, prototypes & macros for rs422/485 comm.               |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/24/95, tgh  -  changed header.                              |
|                                                                             |
|           Copyright (c) 1994, 1995 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _RS422_H
#define  _RS422_H

/* System headers
*/

/* Project headers
*/

/* Defines
*/
#define  COIN_TST       0x03
#define  COIN_IN        0x0c
#define  COIN_ACC       0x0d

#define  MAX_NODES         4

typedef struct
{
   unsigned char  type;                /* message type                       */
   unsigned char  spare;               /* for alignment                      */
   long           bus_n;               /* bus number                         */
   long           driver;
   long           route;               /* bus number                         */
   long           run;                 /* last memory clear time             */
   long           trip;
   long           city;
   char           fareset;
   char           dir;
   byte           OCU_Disabled;
   byte           spares[15];
}  CQ_LOGIN;


typedef struct
{
   unsigned char  type;                /* message type                       */
   unsigned char  spare;               /* for alignment                      */
   long           bus_n;               /* bus number                         */
   time_t         t;                   /* farebox date and time              */
   word           FScrc;               /* fare structure CRC-16              */
   word           BLver;               /* bad list (ext) ver                 */
   word           BLcrc;               /* bad list (ext) CRC-16              */
   word           CBcrc;               /* conf. block    CRC-16              */
   word           SPCT_crc;            /* special card table CRC-16          */
   word           Friendlycrc;         /* friendly agencies crc              */
   word           TRIMCNFcrc;          /* trim config crc                    */
   long           driver;
   long           route;
   long           run;
   long           trip;
   long           city;
   char           fareset;
   char           dir;
   byte           OCU_Disabled;
   byte           spares[17];
}  CQ_STATUS;

/* Global data
*/
extern    word   RS422Baud;

/* Prototypes
*/
int   InitializeRS422Comm    ( word );
int   RS422Cnt               ( void );
int   RS422Nputs             ( char*, int );
int   RS422Puts              ( char* );
int   RS422Putc              ( char );
int   RS422Getc              ( void );
int   CCMsg                  ( byte );  /* send Coin message */
void  RemoteSendDateTime     ( word );
void  RemoteSendConfiguration( word );
void  RemoteSendTRIMConfig   ( word );
void  RemoteSendStatus       ( void );
void  RemoteSendDlist        ( word );
void  RemoteSendSpecialCardTable( word );
void  RemoteSendLogin        ( word );
void  RemoteSendBadList      ( word );
void  RemoteSendCardData     ( void*, int );
void  RemoteSendMemoryClear  ( word, int );
void  RemoteSendFriendly     ( word );

void  RemoteRequestStatus    ( word );


void  Check_CardQuest_Comm   ( void );
byte  CardQuest_CommErr      ( void );
void  CQ_StatusRequest       ( void );

void  NodeDebug              ( void );
void  InitializePPSProtocol  ( void );

extern int     RS422Debug;
extern int     RS422TXDebug;
extern int     RS422RXDebug;

#pragma sep_on segment S_REMOTE
extern int     StartRemote;
extern int     EndRemote;

#pragma sep_off

#endif /*_RS422_H*/


