/*----------------------------------------------------------------------------\
|  Src File:   dbgp.c                                                         |
|  Version :   2.36                                                           |
|  Authored:   01/11/96, tgh                                                  |
|  Function:   Process debug port I/O.                                        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: dbgp.c,v $                                                           |
|      1.00:   01/11/96, tgh  -  Initial release.                             |
|      2.00:   08/24/97, tgh  -  check for pc card                            |
|      2.01:   12/23/98, sjb  -  check battery on pc card                     |
|      2.05:   05/10/00, sjb  -  display configuration                        |
|      2.06:   06/14/00, sjb  -  add hooks to get trim diagnostics            |
|      2.09:   02/16/01, sjb  -  add hooks to enter friendly agencies         |
|      2.10:   02/16/01, sjb  -  add hooks to modify operational flags        |
|      2.11:   03/06/01, sjb  -  add hooks to modify selftest                 |
|      2.12:   05/31/01, aat  -  add hooks to switch between OCU w/Menus and  |
|                                OCU w/3 Big Displays                         |
|      2.14:   07/20/01, aat  -  correct loading of Boot Block to PC-Card.    |
|      2.15:   08/09/01, aat  -  add hooks to program smart cards.            |
|                             -  set OF enable flag when changing ocu displays|
|      2.16:   08/09/01, aat  -  add hooks to program smart cards.            |
|      2.18:   08/09/01, aat  -  booger up to dump Clist2.                    |
|      2.20:   01/07/02, aat  -  added ability to create a "farebox config."  |
|                                PCMCIA card.  Copies ( Dlist, Clist, Clist2, |
|                                TrimCnf, and OldFriendly ) onto the PC-card. |
|      2.25:   05/22/02, sjb  -  modify for J1708 debugging                   |
|      2.26:   05/22/02, sjb  -  modify for Smart Card debugging              |
|      2.27:   01/29/03, sjb  -  add debug of OCU                             |
|                             -  modify moduel to load configuration to fb    |
|                             -  fix saving friendly agencies                 |
|              05/28/03, jks  -  set PC card code size to the max. possible:  |
|                                  0x00077ffc                                 |
|      2.29:   04/16/03, sjb  -  enable/disable TRIM wathcdog led             |
|      2.30:   04/26/03, sjb  -  add debug of IRMA ( people counter )         |
|      2.31:   02/19/04, sjb  -  add 230400 for probe baud rate               |
|      2.32:   08/24/04, sjb  -  display Smart Card Diagnostic data           |
|                                add hooks to step through really big bad list|
|      2.33:   08/30/04, sjb  -  fix cal of crc on big bad list               |
|      2.34:   08/30/04, sjb  -  add hooks to cal crc on big/small Clist2     |
|      2.35:   08/30/04, sjb  -  correct cal crc on big/small Clist2          |
|      2.36    08/19/08, sjb  -  add Keylist when creating Configuration card |
|           Copyright (c) 1993-2003  GFI Genfare All Rights Reserved          |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

/* Project headers
*/
#include "timers.h"
#include "rtc.h"
#include "coin.h"
#include "gen.h"
#include "display.h"
#include "trim.h"
#include "rs485.h"
#include "rs422.h"
#include "pps.h"
#include "util.h"
#include "fbd.h"
#include "status.h"
#include "regs.h"
#include "led.h"
#include "comm.h"
#include "lcddisp.h"
#include "pccard.h"
#include "port.h"
#include "menu.h"
#include "misc.h"
#include "cnf.h"
#include "transact.h"
#include "trim.h"
#include "ability.h"
#include "sound.h"
#include "diag.h"
#include "ocu.h"
#include "scmd.h"
#include "scard.h"
#include "j1708.h"
#include "bill.h"
#include "process.h"
#include "pbq.h"
#include "sclk.h"
#include "probe.h"
#include "dbase.h"
#include "text.h"
#include "rf.h"
#include "magcon.h"
#include "dll.h"
#include "data.h"
#include "oti.h"
#include "autolist.h"
#include "rs422.h"
#include "omniprnt.h"

/* Defines
*/
#define  TESTSET_CHAR   0xfe

#define  _UPPER     2
#define  _U_LEFT    1
#define  _U_CENTER 24
#define  _U_RIGHT  48
#define  _LOWER    17
#define  _L_LEFT    1
#define  _L_CENTER 24
#define  _L_RIGHT  48

#define   _LEFT      1
#define   _CENTER   24
#define   _RIGHT    48
#define   _RIGHT2   48

#define   _CENTER_H 27
#define   _RIGHT_H  54

#define  HID_LED_GRN          'dg'  /* 0x - Switch on LED Green, LED Red off */

/* Module data
*/


/* Global data
*/
int   RetryCount;
int   NakCount;
int   RetryMax;
ulong StartTime;

int   RetryCount232;
int   NakCount232;
int   RetryMax232;
ulong StartTime232;

int   Action_Debug;
int   AL_Debug;
int   COMM_Debug;
int   CRC_Debug;
int   Debug_TX232;
int   Debug_RX232;
int   SORT_Debug;
int   Prnt_Debug;
int   Serial_Debug;
int   UL_Debug;
int   Zone_Debug;

int   Device_ID;

ulong Printed_ID[2];

extern   word   OCU_ver;
extern   ulong  OCU_Part_No;
extern   int    DTM_Debug;
extern   int    Chopper_Test_Count;
extern   word   __RamTest;
extern   int    OCU_Backup;
extern   int    IRMA_Poll;

extern   word     RS485_rnr;
extern   word     RS485_rej;
extern   word     RS485_lrc;
extern   word     RS485_nak;
extern   word     RS485_sabm;
extern   word     RS485_seq;
extern   word     RS485_sto;
extern   word     RS485_rto;
extern   word     RS485_err;
extern   word     RS485_lost;

extern   word     trim_RS485_rnr;
extern   word     trim_RS485_rej;
extern   word     trim_RS485_lrc;
extern   word     trim_RS485_nak;
extern   word     trim_RS485_sabm;
extern   word     trim_RS485_seq;
extern   word     trim_RS485_sto;
extern   word     trim_RS485_rto;
extern   word     trim_RS485_err;
extern   word     trim_RS485_lost;
extern   word     PrbDebug;
extern   ulong    PrbTimer2;
extern   word     ReadyCount;
extern   int      Door_time;
extern   int      RS232_Debug;

extern   byte     DallasTM[10];

extern   byte     AES_key_version_index;

/** AAT - For Development **/

int      DuplicateCard;

/** **/

/* Prototypes
*/
void  CurrentInfo();
void  HelpScreen();
word  GetFriendlyAgency( int );
word  GetOldFriendlyAgency( int );
byte  FareboxTest();
void  GetTrimCommands();
word  GetProcessCmds( int );
void  GetTrimDebug();
void  UTime();
void  Issue_New_( void );
void  convert_unix_time( int);
void  Display_J1708_Diag( void );
void  Display_Trim_Diag( void );
void  Display_Auto_Que( int );
void  SetPrinterBaudRate( void );
void  DataStatus( void );
void  ATlog_Debug_Settings( void );

extern   int   RS422Putc   ( int );
extern   int   RS422Puts   ( char* );

extern Boot_Code();
extern Boot_CodeEnd();
extern long*  _main;

static   unsigned short int Status = 0;
#define  set_status( s )   (Status |= (s))
#define  clr_status( s )   (Status &=~(s))
#define  tst_status( s )   (Status &  (s))

static   word *FlashType   = (word*)0x00004000;
static   word *FlashMfg_ID = (word*)0x00004002;

/* Probing Status and macros to maintain.
*/
#define  EDIT_MESSAGE       0x0001    /* edit message pending */

static
void  TestRS232Timeout()
{
   printf( "RS422ERR\n" );
   Set_Pflag( P_RS232 );
}

void  TestRS232()
{
   if ( tmr_expired( TESTSET_TMR ) )
   {
      RS422Putc( TESTSET_CHAR );
      Init_Tmr_Fn( TESTSET_TMR, TestRS232Timeout, MSEC( 500 ) );
   }
}

void  Test()
{
   register unsigned char *p  = (unsigned char*)0x070000bL;
   register unsigned long  l  =  0L;
   register unsigned int   i  =  0;
   register unsigned char  uc =  0x0f;
   ulong      *src, *dst;

   for( i=0, src=(ulong*)0x00804000, dst=(ulong*)&Dlist;
        i<sizeof(DownLoadList)/4L;
        i++ )
   {
      dst[i] = src[i];
   }

   *p =  uc;

   for(;;)
   {
      swsr.b.swsr =  0x55;
      swsr.b.swsr =  0xaa;
      if ( (++l % 0x8000) == 0L )
      {
         *p  = uc;
         uc ^= 0x0f;
      }
   }
}

void  PlaceHolder()
{
}

void  Test2()
{
   register unsigned char *p  = (unsigned char*)0x070000bL;
   register unsigned long  l  =  0;
   register unsigned int   i  =  0;
   register unsigned int   j  =  0;
   register unsigned char  uc =  0x0f;
   int     *src, *dst;

   for( i=0, j = 0, src=(int*)0x00804000;
        i<sizeof(FRIENDLYAGY)/2; i++ )
   {
      j += src[i];
   }

   if ( j != src[i] )
      uc = 0x0a;
   else
   {
      for( i=0, src=(int*)0x00804000, dst=(int*)&FriendlyAgy;
           i<sizeof(FRIENDLYAGY)/2; i++ )
      {
         dst[i] = src[i];
      }
   }

   *p =  uc;

   for(;;)
   {
      swsr.b.swsr =  0x55;
      swsr.b.swsr =  0xaa;
      if ( (++l % 0x8000) == 0L )
      {
         *p  = uc;
         uc ^= 0x0f;
      }
   }
}

void  Place_Holder()
{
}

void  Test3()
{
   register unsigned char *p  = (unsigned char*)0x070000bL;
   register unsigned long  l  =  0;
   register unsigned int   i  =  0;
   register unsigned int   j  =  0;
   register unsigned char  uc =  0x0f;
   int     *src, *dst, sz;

   sz  = sizeof(DownLoadList)/2;
   sz += sizeof(GFIconfig)/2;
   sz += sizeof(GFIconfig2)/2;
   sz += sizeof(FRIENDLYAGY)/2;
   sz += sizeof(TRIMCNF)/2;
   sz += sizeof(PRB_REQ_PARMS)/2;
   sz += (sizeof(SpecCard)/2);   /* Special card table plus crc */
   sz += (sizeof(DriverList)/2); /* Driver List table plus crc */
   sz += (sizeof(RouteList)/2);  /* Route List table plus crc */
   sz += (sizeof(KeyList)/2);    /* key Text List table plus crc */

   for( i=0, j = 0, src=(int*)0x00804000;
        i<sz; i++ )
   {
      j += src[i];
   }

   if ( j != src[i] )
      uc = 0x0a;
   else
   {
      for( i=0, src=(int*)0x00804000, dst=(int*)&Dlist;
           i<sizeof(DownLoadList)/2; i++ )
      {
         dst[i] = src[i];
      }

      for( j=0, dst=(int*)&Clist;
           j<sizeof(GFIconfig)/2;
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&Clist2;
           j<sizeof(GFIconfig2)/2;
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&FriendlyAgy;
           j<sizeof(FRIENDLYAGY)/2;
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&TrimCnf;
           j<sizeof(TRIMCNF)/2;
           j++, i++ )

      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&Prb_Req_Parms;
           j<sizeof(PRB_REQ_PARMS)/2;
           j++, i++ )

      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&SpecCard;
           j<(sizeof(SpecCard)/2);
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&DriverList;
           j<(sizeof(DriverList)/2);
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&RouteList;
           j<(sizeof(RouteList)/2);
           j++, i++ )
      {
         dst[j] = src[i];
      }

      for( j=0, dst=(int*)&KeyList;
           j<(sizeof(KeyList)/2);
           j++, i++ )
      {
         dst[j] = src[i];
      }
   }

   *p =  uc;

   for(;;)
   {
      swsr.b.swsr =  0x55;
      swsr.b.swsr =  0xaa;
      if ( (++l % 0x8000) == 0L )
      {
         *p  = uc;
         uc ^= 0x0f;
      }
   }

}

void  Place_Holder3()
{
}

void BadList()
{
   register unsigned char *p  = (unsigned char*)0x070000bL;
   register unsigned long  l  =  0;
   register unsigned long  i  =  0;
   register unsigned long  j  =  0;
   register unsigned char  uc =  0x0f;
   ulong                   sz;
   int                    *src, *dst;


   sz  = ( sizeof(BBlist) + sizeof(BBIndex) + 2 )/2;

   for( i=0, j = 0, src=(int*)0x00804000;
        i<sz; i++ )
   {
      j += src[i];
   }

   if ( (int)j != src[i] )
   {
      uc = 0x0a;
   }
   else
   {
      for( j=0, src=(int*)0x00804000, dst=(int*)&BBlist; j<sz;  j++ )
      {
         dst[j] = src[j];
      }
   }

   for(;;)
   {
      swsr.b.swsr =  0x55;
      swsr.b.swsr =  0xaa;
      if ( (++l % 0x8000) == 0L )
      {
         *p  = uc;
         uc ^= 0x0f;
      }
   }
}

void  BL_PlaceHolder()
{
}

void BadList_FS()
{
   register unsigned char *p  = (unsigned char*)0x070000bL;
   register unsigned long  l  =  0;
   register unsigned long  i  =  0;
   register unsigned long  j  =  0;
   register unsigned char  uc =  0x0f;
   ulong                   sz;
   int                    *src, *dst;


   sz  =  sizeof(BBlist)/2;
   sz += sizeof(DownLoadList)/2;

   for( i=0, j = 0, src=(int*)0x00804000;
        i<sz; i++ )
   {
      j += src[i];
   }

   if ( (int)j != src[i] )
   {
      uc = 0x0a;
   }
   else
   {
      for( i = 0, src=(int*)0x00804000, dst=(int*)&BBlist; i<(sizeof(BBlist)/2);  i++ )
      {
         dst[i] = src[i];
      }

      for( j=0, dst=(int*)&Dlist; j<sizeof(DownLoadList)/2; j++, i++ )
      {
         dst[j] = src[i];
      }
   }

   for(;;)
   {
      swsr.b.swsr =  0x55;
      swsr.b.swsr =  0xaa;
      if ( (++l % 0x8000) == 0L )
      {
         *p  = uc;
         uc ^= 0x0f;
      }
   }
}

void  BL_FS_PHolder()
{
}

void  ClearDebugStatus()
{
   clr_status( EDIT_MESSAGE );
   ATlog_Debug =
   BurnIn_Debug =
   BV_Debug  =
   COMM_Debug =
   CON_Debug =
   CRC_Debug =
   CRD_Debug =
   DTM_Debug =
   J1708_Debug =
   OCU_Debug =
   Prnt_Debug =
   RF_Debug  =
   RF_Display =
   RF_enable =
   Serial_Debug =
   SORT_Debug =
   Sound_Debug =
   SSC_Debug =
   Text_Debug =
   Tlog_Debug  =
   TRANS_Debug =
   TRM_Debug =
   WI_Debug =
   Probe_Debug =
   Zone_Debug =
   AL_Debug =
   UL_Debug =
   RS422Debug =
   RS422TXDebug =
   Action_Debug = 0;
}

/*-------------------------------------------------------------------------\
|  Function:   Process_Dbg();                                              |
|  Purpose :   Process debug port data.                                    |
|  Synopsis:   void  Process_Dbg( void );                                  |
|  Input   :   None.                                                       |
|  Output  :   None.                                                       |
|  Comments:   Called via system timer.                                    |
|                                                                          |
\-------------------------------------------------------------------------*/
void  Process_Dbg()
{
   static int    i,j;
   static byte   *q;
   static word   *index;

   char          ch,c;
   long          *p;
   word          crc;
   int           n,x,y;
   time_t        t,l;
   char          buf[20];
   char          b[2];
   byte          *d;
/*   long          big_crc; */
   void          *r;
   long          m,f;
   ulong         dd[2];

   struct tm     *tm;

   if ( tst_status( EDIT_MESSAGE ) )
      return;

   while( kbhit() )
   {
      ch=getchar();
      switch( ch )
      {
      case '!':
        if( Tst_Sflag( S_SPARES1 ) )
          MyAgency = Mlist.Config.AgencyCode = Clist.PUBLIC.AgencyCode = 4094;
        else if( Tst_Sflag( S_SPARES2 ) )
        {
            j = GFI_KEYS;

            printf( "                 KeyATbl            KeyBTbl\n" );
            for( n=0; n<MAX_1k_SECTORS; n++)
            {
               printf( "Sector %02d - ",n );
               for( i=0; i<6; i++ )
               {
                 printf( "%x ", KeyATbl[j][n][i] );
               }
               for( i=0; i<6; i++ )
               {
                 printf( " %x", KeyBTbl[j][n][i] );
               }
               putchar('\n');
            }
        }
        else if( Tst_Sflag( S_SPARES3 ) )
        {
        }
        else
        {
        }
        break;

      case '@':
         if (Tst_Sflag( S_SPARES3 ))
         {
            ATlog_Debug_Settings();
         }
        else if( Tst_Sflag( S_SPARES2 ) )
        {
        }
        else if( Tst_Sflag( S_SPARES1 ) )
        {
           /*strcpy( Trim_DisplayMsg.Message1, "  GCRTA " );*/
           TrimDisplayMsg( 0 );
           /*TrimCmd( TRIM_DISPLAY_MSG, &Trim_DisplayMsg, sizeof(TRIM_DISPLAY_TEXT) );*/
        }
        else
        {
            if( DuplicateCard )
            {
               printf( "Process Cards\n" );
               DuplicateCard = FALSE;
            }
            else
            {
               printf( "Don't Process Cards (used to create duplicate)\n" );
               DuplicateCard = TRUE;
            }
        }
        break;

      case '#':
        if( Tst_Sflag( S_SPARES3|S_SPARES1 ) )
        {
            if ( Tst_Sflag( S_SPARES3 ))
               x = 0;
            else
               x = 1;
            set_status(  EDIT_MESSAGE );
            convert_unix_time(x);
            clr_status(  EDIT_MESSAGE );
        }
        else if( Tst_Sflag( S_SPARES2 ) )
          UTime();
        else if ( ( Ml.trim_v > 201 ) && !Tst_Sflag( S_TRIMOFFLINE ))
        {
          printf("Current TRIM temperature %03.1f�C\n", TRIM_Temp );
          TrimCmd( TRIM_TEMPERATURE, NULL, 0 );
        }
        break;

      case '$':
         if ( Tst_Sflag( S_SPARES2 ) )
         {
            set_device_id();
         }
         else
         {
           if ( Probe_Debug )
           {
             printf( " turn off Probe_Debug flag\n");
             Probe_Debug = FALSE;
           }
           else
           {
             printf( " turn on Probe_Debug flag\n");
             Probe_Debug = TRUE;
           }
         }
        break;
      case '%':
        if ( Action_Debug )
        {
           printf("turn off Action_Debug\n");
           Action_Debug = FALSE;
        }
        else
        {
           printf("turn on Action_Debug\n");
           Action_Debug = TRUE;
        }
/*** test code  remove when done ***
        if ( UnBadList )
        {
          UnBadList = FALSE;
          printf("turn off UnBadList\n");
        }
        else
        {
          UnBadList = TRUE;
          printf("UnBadList Card\n");
        }
***********************************/
        break;

      case '^':
        i = 0;
      case '&':
        if ( i > Ml.evnt_c )
           i = 0;
/*
        Event_Type( i );
        printf("Event no %d type %d\n", i, Event_Type( i ));
        ++i;
*/
        if ( Tst_Sflag( S_SPARES3 ))
        {
           NodeDebug();
        }
        else
        {
            ascii_hex_dump( (char*)&Clist2, (int)sizeof(GFIconfig2) );
        }
        break;

      case '+':
        if ( Tst_Sflag( S_BURNIN ) )
        {
           printf("break badlist by setting ver & crc to 0\n");
           BBlist.BBL_ver = BBlist.BBL_crc = 0;
           Set_S2flag( S2_BAD_BAD_LIST );
           memset( &BBlist, 0, sizeof(BBlist));
           if ( !Tst_Pflag( P_BIG_BADLIST ))
              *(int*)&BBlist.BadList[BBL_SSIZE] = 0;
        }
        else if ( Tst_Sflag( S_SPARES3 ))
        {
           if ( RS422Debug )
           {
              RS422Debug = FALSE;
              printf("turn off RS422 debug\n");
           }
           else
           {
              RS422Debug = TRUE;
              printf("turn on RS422 debug\n");
           }
        }
        else if ( Tst_Sflag( S_SPARES1 ))
        {
           if ( RS422TXDebug )
           {
              RS422TXDebug = FALSE;
              printf("turn off RS422 TX debug\n");
           }
           else
           {
              RS422TXDebug = TRUE;
              printf("turn on RS422 TX debug\n");
           }
        }
        else
        {
           if ( Tst_Aflag( A_SMART_TRIM ))
             printf("SMART CARD TRIM ENABLED\n");
           else
             printf("SMART CARD TRIM DISABLED\n");

           if ( Tst_Sflag( S_SPARES1 ))
           {
              if ( Tst_S2flag( S2_SC_PRINT_ENB ))
                 Clr_S2flag( S2_SC_PRINT_ENB );
              else
                 Set_S2flag( S2_SC_PRINT_ENB );
           }
           if ( OF15_SC_PRINT_ON || Tst_S2flag( S2_SC_PRINT_ENB ))
              printf("print on SMART CARDS\n");
           else
              printf("don't print on SMART CARDS\n");
        }
        break;

      case '*':
        if ( Tst_Sflag( S_SPARES3 ))
        {
        }
        else if( Tst_Sflag( S_SPARES1 ) )
        {
          TrimCmd( TRIMBURNIN, NULL, 0 );
        }
        else
        {
           TrimCmd( TRIM_REQSFLAGS, NULL, 0 );
        }
        break;
     case '=':
        switch(Prnt_Debug)
        {
           case 0:
              ++Prnt_Debug;
              printf(" Prnt_Debug =  RX\n" );
              break;
           case 1:
              ++Prnt_Debug;
              printf(" Prnt_Debug =  TX\n" );
              break;
           case 2:
              ++Prnt_Debug;
              printf(" Prnt_Debug =  RX & TX\n" );
              break;
           default:
              Prnt_Debug = 0;
              printf(" Prnt_Debug OFF\n" );
              break;
        }
        break;

     case '|':
        if ( Tst_Sflag( S_BURNIN ) )
           SetPrinterBaudRate();
        else
        {
           for ( i = 0; i < HL_SIZE; i++ )
              printf( " index %d: %d/%d \n", i, Dlist.HolidayList[i].Month, Dlist.HolidayList[i].Day );
        }
        break;

     case '[':
        set_status(  EDIT_MESSAGE );
        memset( buf, 0, sizeof( buf ));
        printf("convertalphnumeric to a ulong long\n");
        if( (n=Ngetn(buf, 16)) )
        {
           i = strlen( buf );
           StoDouble( buf, &dd, i );
           printf("\n data %08lx %08lx\n", dd[0], dd[1] );
        }
        else
        {
          printf("\n nothing entered\n" );
        }
        clr_status(  EDIT_MESSAGE );
        break;

	  case ']':
       if ( RS232_Debug )
		 {
		 	 printf("RS232_Debug = Off\n");
			 RS232_Debug = FALSE;
		 }
       else
       {
		 	 printf("RS232_Debug = ON\n");
			 RS232_Debug = TRUE;
       }
       break;

	  case '0':
       if ( Tst_Sflag( S_SPARES3 ))
		 {
			 if( Tlog_Debug == 0 )
			 {
				 printf("Tlog_Debug = On\n");
				 Tlog_Debug = 1;
			 }
			 else
			 {
				 printf("Tlog_Debug = Off\n");
				 Tlog_Debug = 0;
			 }
		 }
       else if ( Tst_Sflag( S_SPARES1 ))
       {
          if ( WI_Debug )
          {
             WI_Debug = FALSE;
				 printf("wiport debug off\n");
          }
          else
          {
             WI_Debug = TRUE;
				 printf("wiport debug on\n");
          }
       }
       else if ( Tst_Sflag( S_SPARES2 ))
       {
          q = (byte*)&SpecCard;
          y = sizeof(SpecCard);
          printf("Special card size %d\n", y );

          for ( x=0; x < y; ++x, ++q )
          {
            if (x !=0 )
              if ( (x % 16 ) == 0 )
                putchar('\n');
            printf(" %02x", *q );
          }
          putchar('\n');
       }
       else
          {
          RF_Debug = ~RF_Debug ;
          printf("RF_Debug=%u\n", RF_Debug) ;
          } ;
		 break;

	  case '1':
       if( Tst_Sflag( S_SPARES2 ) )
       {
          printf( "Lock Code ....: ");
          for ( i = 0; i < 5; i++ )
            printf( "%c", Dlist.LockCode[i] );
          /*
          printf( "\nOld Lock code: ");
          for ( i = 0; i < 5; i++ )
            printf( "%c", OldLockCode[i] );
            */
          printf("\n");
       }
       else if( Tst_Sflag( S_SPARES3 ))
		 {
          switch( BV_Debug )
          {
             case 0:
                BV_Debug = 1;
                printf("Bill Validator Debug = ERROR DATA ONLY\n");
             break;

             case 1:
                BV_Debug = 2;
                printf("Bill Validator Debug = On low\n");
             break;

             case 2:
                BV_Debug = 4;
                printf("Bill Validator Debug = On med.\n");
             break;

             case 4:
                BV_Debug = 5;
                printf("Bill Validator Debug = On high\n");
             break;

             case 5:
                BV_Debug = 3;
                printf("Bill Validator Debug = DATA ONLY\n");
             break;

             case 3:
             default:
                BV_Debug = 0;
                printf("Bill Validator Debug = Off\n");
             break;
          }
		 }
       else
       {
         /*
         if ( Tst_Sflag( S_BURNIN ) )
         {
           memset( &Unwanted, 0, sizeof(UNWANTED));
           Unwanted.Signiture = SIGNITURE;
           printf("Init Unwanted bills\n");
         }
         printf("receved  CP - %05u   $1 - %05u   $2 - %05u   $5 - %05u\n",
           Unwanted.Recv[0], Unwanted.Recv[1], Unwanted.Recv[2],
           Unwanted.Recv[3] );
         printf("        $10 - %05u  $20 - %05u  $50 - %05u $100 - %05u\n",
           Unwanted.Recv[4], Unwanted.Recv[5], Unwanted.Recv[6],
           Unwanted.Recv[7] );

         printf("stacked  CP - %05u   $1 - %05u   $2 - %05u   $5 - %05u\n",
           Unwanted.Stacked[0], Unwanted.Stacked[1], Unwanted.Stacked[2],
           Unwanted.Stacked[3] );
         printf("        $10 - %05u  $20 - %05u  $50 - %05u $100 - %05u\n",
           Unwanted.Stacked[4], Unwanted.Stacked[5], Unwanted.Stacked[6],
           Unwanted.Stacked[7] );
         */
       }
		 break;
	  case '2':
       if ( Tst_Sflag( S_SPARES3 ))
		 {
			 if( CRD_Debug == 0 )
			 {
				 printf("Card Reader Debug = On\n");
				 CRD_Debug = 1;
			 }
			 else if( CRD_Debug == 1 )
			 {
				 printf("Card Reader Additional Debug = On\n");
				 CRD_Debug = 2;
			 }
			 else if (CRD_Debug == 2 )
			 {
				 printf("Card Reader Data only\n");
				 CRD_Debug = 4;
			 }
			 else if (CRD_Debug == 4 )
			 {
				 printf("Card Raw Data\n");
				 CRD_Debug = 8;
			 }
			 else
			 {
				 printf("Card Reader Debug = Off\n");
				 CRD_Debug = 0;
			 }
		 }
       else if ( Tst_Sflag( S_SPARES1 ))
       {
          if ( Serial_Debug )
          {
            printf("Serial debug off\n");
            Serial_Debug = 0;
          }
          else
          {
            printf("Serial debug on\n");
            ++Serial_Debug;
          }
       }
       else if ( Tst_Sflag( S_SPARES2 ))
       {
/*FIXME*/
          if ( Tst_Aflag( A_CARDQUEST ) )
          {
             if ( Tst_Sflag( S_BURNIN ))
             {
                printf("break BL  by setting ver & crc to 0\n");
                BBlist.BBL_ver = BBlist.BBL_crc = 0;
                memset( &BBlist, 0, sizeof(BBlist));
                Set_S2flag( S2_BAD_BAD_LIST );
                if ( !Tst_Pflag( P_BIG_BADLIST ))
                   *(int*)&BBlist.BadList[BBL_SSIZE] = 0;
             }
             printf("break Dlist by setting crc to 0\n");
             memset( &Dlist, 0, sizeof(Dlist));
          }
       }
       else if ( Tst_Sflag( S_BURNIN ))
       {
         printf("break BL  by setting ver & crc to 0\n");
         BBlist.BBL_ver = BBlist.BBL_crc = 0;
         memset( &BBlist, 0, sizeof(BBlist));
         if ( !Tst_Pflag( P_BIG_BADLIST ))
           *(int*)&BBlist.BadList[BBL_SSIZE] = 0;
       }
       else
       {
          printf(" RS422Debug: %s RS422TXDebug: %s\n",
          (RS422Debug ? " ON":"OFF"), (RS422TXDebug ? " ON":"OFF") );
       }
		 break;
	  case '3':
       if ( Tst_Sflag( S_SPARES2 ))
       {
       }
       if ( Tst_Sflag( S_SPARES1 ))
       {
       }
       else if ( Tst_Sflag( S_SPARES3 ))
		 {
			 if( CON_Debug == 0 )
			 {
				 printf("Coin Validator Debug = On\n");
				 CON_Debug = 1;
			 }
			 else
			 {
				 printf("Coin Validator Debug = Off\n");
				 CON_Debug = 0;
			 }
		 }
       else
       {
       }
		 break;
	  case '4':
       if ( Tst_Sflag( S_SPARES3 ))
		 {
			 if( OCU_Debug == 0 )
			 {
				 printf("OCU Debug = On\n");
				 OCU_Debug = 1;
			 }
			 else if( OCU_Debug == 1 )
			 {
				 printf("OCU Debug = On w/Errors\n");
				 OCU_Debug = 2;
			 }
			 else if( OCU_Debug == 2 )
			 {
				 printf("OCU Debug = RX\n");
				 OCU_Debug = 3;
			 }
			 else if( OCU_Debug == 3 )
			 {
				 printf("OCU Debug = TX\n");
				 OCU_Debug = 4;
			 }
			 else if( OCU_Debug == 4 )
			 {
				 printf("OCU Debug = TX/RX\n");
				 OCU_Debug = 5;
			 }
			 else
			 {
				 printf("OCU Debug = Off\n");
				 OCU_Debug = 0;
			 }
       }
       else if ( Tst_Sflag( S_BURNIN ))
       {
          if ( RF_Debug )
          {
            printf("turn off RF_Debug\n");
            RF_Debug = FALSE;
          }
          else
          {
            printf("turn on RF_Debug\n");
            RF_Debug = TRUE;
          }
       }
       else if ( Tst_Sflag( S_SPARES2 ))
       {
          if ( RF_Display )
          {
            printf("turn OFF RF init display \n");
            RF_Display = FALSE;
          }
          else
          {
            printf("turn ON RF init display \n");
            RF_Display = TRUE;
          }
       }
       else if ( Tst_Sflag( S_SPARES1 ))
       {
         if ( Tst_A2flag( A_MONITOR_MODE ))
         {
            printf("exit monitor mode\n");
            Clr_A2flag( A_MONITOR_MODE );
         }
         else
         {
            printf("set to monitor mode\n");
            Set_A2flag( A_MONITOR_MODE );
         }
       }
       else
       {
          clrscr();
          printf(" wireless ds configuration");
          ascii_hex_dump( (char*)&wiport, (int)sizeof(wiport) );
          printf(" wireless port configuration");
          ascii_hex_dump( (char*)&wp_config, (int)sizeof(wiport) );
       }
		 break;
	  case '5':
       if ( Tst_Sflag( S_SPARES3 ))
       {
         switch( SSC_Debug )
         {
           case 0:
             printf("SmartCard General Debug = On\n");
             SSC_Debug = 1; /* SSC_DEBUG_ON */
             break;
           case 1:
             printf("SmartCard Commands Only = On\n");
             SSC_Debug = 2; /* SSC_COMMANDS */
             break;
           case 2:
             printf("SmartCard General & Commands = On\n");
             SSC_Debug = 3; /* (SSC_DEBUG_ON|SSC_COMMANDS) */
             break;
           case 3:
             printf("SmartCard Formatted Data Only = On\n");
             SSC_Debug = 4; /* SSC_FORMATTED */
             break;
           case 4:
             printf("SmartCard General & Formatted Data = On\n");
             SSC_Debug = 5; /* (SSC_DEBUG_ON|SSC_FORMATTED) */
             break;
           case 5:
             printf("SmartCard Commands & Formatted Data = On\n");
             SSC_Debug = 6; /* (SSC_COMMANDS|SSC_FORMATTED) */
             break;
           case 6:
             printf("SmartCard General & Commands & Formatted Data = On\n");
             SSC_Debug = 7; /* (SSC_DEBUG_ON|SSC_COMMANDS|SSC_FORMATTED) */
             break;
           case 7:
             printf("SmartCard Raw Data = On\n");
             SSC_Debug = 8; /* SSC_DATA */
             break;
           case 8:
             printf("Commands & Formatted Data & Raw Data = On\n");
             SSC_Debug = 14; /* (SSC_COMMANDS|SSC_FORMATTED|SSC_DATA) */
             break;
           case 14:  /* 0x0E */
             printf("SmartCard DESFire Errors = On\n");
             SSC_Debug = 0x10; /* (SSC_DESFIRE_ERRORS) */
             break;
           case 16:  /* 0x10 */
             printf("SmartCard DESFire Commands = On\n");
             SSC_Debug = 0x20; /* (SSC_DESFIRE_COMMANDS) */
             break;
           case 32:  /* 0x20 */
             printf("SmartCard DESFire Errors & Commands = On\n");
             SSC_Debug = 0x30; /* (SSC_DESFIRE_ERRORS|SSC_DESFIRE_COMMANDS) */
             break;
           case 48:  /* 0x30 */
             printf("SmartCard DESFire Detail = On\n");
             SSC_Debug = 0x40; /* (SSC_DESFIRE_DETAILS) */
             break;
           case 64:  /* 0x40 */
             printf("SmartCard DESFire Data = On\n");
             SSC_Debug = 0x80; /* (SSC_DESFIRE_DATA) */
             break;
           case 128:  /* 0x80 */
             printf("SmartCard DESFire Details & Data = On\n");
             SSC_Debug = 0xC0; /* (SSC_DESFIRE_DETAILS|SSC_DESFIRE_DATA) */
             break;
           case 192:  /* 0xC0 */
             printf("SmartCard DESFire Errors & Commands & Data = On\n");
             SSC_Debug = 0xB0; /* (SSC_DESFIRE_ERRORS|SSC_DESFIRE_COMMANDS|SSC_DESFIRE_DATA) */
             break;
           case 176:  /* 0xB0 */
             printf("SmartCard DESFire Errors & Commands & Details & Data = On\n");
             SSC_Debug = 0xF0; /* (SSC_DESFIRE_ERRORS|SSC_DESFIRE_COMMANDS|SSC_DESFIRE_DETAILS|SSC_DESFIRE_DATA) */
             break;
           default:
             printf("SmartCard Debug = Off\n");
             SSC_Debug = 0;
             break;
         }
       }
       else if ( Tst_Sflag( S_SPARES1 ))
       {
          if ( i >= MAX_AL_MSG )
            i = 0;
          Display_Auto_Que( i );
          if ( ++i >= Auto_Que.cnt )
            i = 0;
       }
       else if ( Tst_Sflag( S_SPARES2 ))
       {
          printf( "Add_Product user_type         %u\n", Add_Product.user_type );
          printf( "Add_Product.prod_type        %u\n", Add_Product.prod_type );
          printf( "Add_Product.desig            %u\n", Add_Product.desig );
          printf( "Add_Product.exp_type         %u\n", Add_Product.exp_type );
          printf( "Add_Product.details          %u\n", Add_Product.details );
          printf( "Add_Product.prod_id          %u\n", Add_Product.prod_id );

          printf( "AdD_Product fare id          %u\n", Add_Product.fare_id );
          printf( "Add_Product.start_date       %u\n", Add_Product.start_date );
          printf( "Add_Product.end_date         %u\n", Add_Product.end_date );

          printf( "Add_Product.add_time         %u\n", Add_Product.add_time );
          printf( "Add_Product.value            %u\n", Add_Product.value );

       }
       else
       {
          if ( UL_Debug )
          {
             UL_Debug = FALSE;
             printf("turn off UL debug\n");
          }
          else
          {
             UL_Debug = TRUE;
             printf("turn on UL debug\n");
          }
       }
       break;

     case '6':
       if ( Tst_Sflag( S_SPARES3 ))
       {
         switch( TRM_Debug )
         {
           case 0:
             TRM_Debug = 1;
             printf("TRiM General Debug = On\n");
             break;
           case 1:
             TRM_Debug = 2;
             printf("TRiM Commands Only = On\n");
             break;
           case 2:
             TRM_Debug = 3;
             printf("TRiM General & Commands = On\n");
             break;
           case 3:
             TRM_Debug = 4;
             printf("TRiM Formatted Data Only = On\n");
             break;
           case 4:
             TRM_Debug = 5;
             printf("TRiM General & Formatted Data = On\n");
             break;
           case 5:
             TRM_Debug = 6;
             printf("TRiM Commands & Formatted Data = On\n");
             break;
           case 6:
             TRM_Debug = 7;
             printf("TRiM General & Commands & Formatted Data = On\n");
             break;
           case 7:
             TRM_Debug = 8;
             printf("TRiM RAW Data = On\n");
             break;
           case 8:
             TRM_Debug = 12;
             printf("TRiM Formatted & RAW Data = On\n");
             break;
           default:
             printf("TRiM Debug = Off\n");
             TRM_Debug = 0;
             break;
         }
       }
       break;

	  case '7':
        if ( Tst_Sflag( S_SPARES1 ))
        {
           if ( SORT_Debug )
           {
              SORT_Debug = FALSE;
              printf("Turn off SORT_Debug\n");
           }
           else
           {
              SORT_Debug = TRUE;
              printf("Turn ON SORT_Debug\n");
           }
        }
        else if ( Tst_Sflag( S_SPARES3 ))
        {
        }
        else
        {
           tm = gmtime( &FutureFare );
           printf("logoff timer %ld\n", tmr[AUTO_LOG_TMR].t/1000 );
           printf("probe delay tmr %ld\n", tmr[PROBE_DELAY_TMR].t/1000);
           printf("main tmr %ld\n", tmr[MAIN_LOG_TMR].t/1000);
           printf("future time %02d:%02d:%02d  %02d/%02d/%02d\n",
              tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_mon+1, tm->tm_mday, tm->tm_year%100 );
           time( &t );
           tm = gmtime( &t );
           printf("current time %02d:%02d:%02d  %02d/%02d/%02d\n",
              tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_mon+1, tm->tm_mday, tm->tm_year%100 );

           crc = crc16( (char*)&FDlist, sizeof(DownLoadList)-2 );
           swab( (char*)&crc, (char*)&crc, 2);

           printf( "Future FDlist ver %d crc %X cal %X\n",
              FDlist.ParameterTableVersion, FDlist.CRC16, crc );
        }
        break;

	  case '8':
       if ( Tst_Sflag( S_SPARES3 ))
		 {
           if( TRANS_Debug == 0 )
           {
             printf("TRANSACTION Debug = On\n");
             TRANS_Debug = 1;
           }
           else
           {
             printf("TRANSACTION Debug = Off\n");
             TRANS_Debug = 0;
           }
		 }
       else if ( Tst_Sflag( S_SPARES2 ))
       {
         /*HID_LED_GRN          'dg'*/
       }
       else
       {
          tm = gmtime( &RF_Data.clear_time );
          printf("Last RF cleared time %02d:%02d:%02d  %02d/%02d/%02d  %ld\n",
            tm->tm_hour, tm->tm_min, tm->tm_sec,
            tm->tm_mon+1, tm->tm_mday, tm->tm_year%100, RF_Data.clear_time );
          printf("RF clear cmde  %04x\n", RF_Data.clearcmd );
          printf("RF event cnt   %lu\n",  RF_Data.event );
          printf("RF trans cnt   %lu\n",  RF_Data.transaction );
          printf("RF alarm flags %04x\n", RF_Data.alarmflags );
       }
		 break;
   	case '9':
       if ( Tst_Sflag( S_SPARES3 ))
		 {
			if( BurnIn_Debug == 0 )
			{
			   printf("BurnIn_Debug = On\n");
			   BurnIn_Debug = 1;
			}
			else
			{
			   printf("BurnIn_Debug = Off\n");
			   BurnIn_Debug = 0;
			}
		 }
       else
         crc_table32();
		 break;

      case 'A':
         CurrentInfo();
         break;

      case 'a':
        Display_Trim_Diag();
        break;

      case 'B':
        c = 0;
        if ( Tst_Sflag( S_SPARES1 ))
        {
            printf( "Key Repeate Disabled - Debug\n" );
            Clist2.logon |= NO_KEY_REPEAT;
            if ( ONE_KEYLOG_OFF )
               c |= OCU_BLUE_OPTION;      /* modified BLUE key options      */
            memcpy( b, UcharToPhex( c ), 2 );   /* disable OCU key repeat feature */
            OCU_Msg( OID_DCU, OM_KEYOPTION, b, 2 );
        }
        else if ( Tst_Sflag( S_SPARES2 ))
        {
            printf( "Key Repeate Enabled - Debug\n" );
            Clist2.logon &= ~NO_KEY_REPEAT;
            c = OCU_KEYREPEAT_DIS;        /* disable OCU key repeat feature */
            if ( ONE_KEYLOG_OFF )
               c |= OCU_BLUE_OPTION;      /* modified BLUE key options      */
            memcpy( b, UcharToPhex( c ), 2 );   /* enable OCU key repeat feature */
            OCU_Msg( OID_DCU, OM_KEYOPTION, b, 2 );
        }
        else if ( Tst_Sflag( S_SPARES3 ))
        {
          if ( !Tst_Sflag( S_OCU_OFFLINE ))
          {
            switch( OCU_DownLoad( PCMCIA_OCUBK_ID ) )
            {
               case 0:
                  printf( "\nattemp to send backup code to OCU\n" );
                  break;
               case 1:
                  printf( "\nno pc card, don't sent backup OCU code\n" );
                  break;
               case 2:
                  printf( "\ninvalid id, don't sent backup OCU code\n" );
                  break;
               case 3:
                  printf( "\n bad checksum on backup OCU code\n" );
                  break;
               default:
                  printf( "\nno OCU backup download, unknown return\n" );
                  break;
            }
          }
          else
            printf( "\nOCU Offline\n" );
        }
        else
        {
           printf("OCU Ready count %u\n", ReadyCount );
        }
        break;

      case 'b':
         if ( Tst_Sflag( S_SPARES1 ))
         {
           if( Tst_Sflag( S_TRIMBYPASS ) )
           {
             printf("Turn Bypass OFF\n");
             TrimCmd( TRIMBYPASSOFF, NULL, 0 );
           }
           else
           {
             printf("Turn Bypass ON\n");
             TrimCmd( TRIMBYPASSON, NULL, 0 );
           }
         }
         else if ( Tst_Sflag( S_SPARES2 ))
         {
            if ( OOS_BILL )
            {
               Clist2.OOS_coin_bill &= 0xfd;
               printf("turn bill off\n");
            }
            else
            {
               Clist2.OOS_coin_bill |= 0x02;
               printf("leave bill on\n" );
            }
            Clist2_crc();
         }
         else if ( Tst_Sflag( S_BURNIN ))
         {
            if ( OLD_DALLAS )
            {
                printf("new Dallas chip\n");
                OLD_DALLAS = FALSE;
            }
            else
            {
               printf("old Dallas chip\n");
               OLD_DALLAS = TRUE;
            }
         }
         else
         {
            DTM_RxIdle( 0 );
         }
         break;

      case 'C':
         if ( Tst_Sflag( S_SPARES1 ))
         {
           printf("Clear Print Buffer\n");
           TrimCmd( TRIMCLRPRINTBUF, NULL, 0 );
         }
         else if ( Tst_Sflag( S_SPARES2 ))
         {
           printf("Turn AutoAccept ON\n");
           TrimCmd( TRIM_AUTO_ACCEPT_ENB, NULL, 0 );
         }
         else if ( Tst_Sflag( S_SPARES3 ))
         {
           printf("Turn AutoAccept OFF\n");
           TrimCmd( TRIM_AUTO_ACCEPT_DIS, NULL, 0 );
         }
         else
         {
           printf("Send TRiM process COMPLETE");
           TrimCmd( TRIM_PROCESS_COMPLETE, NULL, 0 );
           Clr_Tflag( TRIM_CARDINPROCESS );
         }
         break;

      case  'c':
         if ( Tst_Sflag( S_SPARES1 ))
         {
           TrimCmd( TRIMCLRJAM, NULL, 0 );
         }
         else
           DBdebug();
         break;

      case 'D':
         if( Tst_Sflag( S_SPARES1 ) )
         {
            memset( &SC_Diag, 0, sizeof(SC_DIAG));
            SC_Diag.Signiture = SC_SIGNATURE;
         }

         printf("\n      SMART CARD Diagnostics  \n");
         printf("SC ReadCount        %6u     SC Read Errors        %6u\n",
            SC_Diag.ReadCount, SC_Diag.ReadError  );

         printf("SC Read Blocks      %6u     SC ReadErr NoRecover  %6u\n",
            SC_Diag.ReadBlockCount, SC_Diag.ReadErrNoRecover  );

         printf("SC Read Recovery    %6u     SC Read Recove Err    %6u\n",
            SC_Diag.ReadRecovery, SC_Diag.ReadRecoverError  );


         printf("SC WriteCount       %6u     SC Write Errors       %6u\n",
            SC_Diag.WriteCount, SC_Diag.WriteError );

         printf("SC Write Blocks     %6u     SC Write BlkErr       %6u\n",
             SC_Diag.WriteBlockCount, SC_Diag.WriteBlockError );

         printf("SC C_Reads        %8lu     SC C_Writes         %8lu\n",
             SC_Diag.C_ReadCount, SC_Diag.C_WriteCount );


         printf("SC offline          %6u     SC retry              %6u\n",
             SC_Diag.offline, SC_Diag.retry );

         printf("SC comm err         %6u\n", SC_Diag.com_err );
         break;

      case 'd':
        if( Tst_Sflag( S_BURNIN ) )
        {
           printf("TRiM Clear Memory\n");
           TrimCmd( TRIM_CLRMEM, NULL, 0 );
        }
        else if( Tst_Sflag( S_SPARES1 ) )
        {
           printf("Host will verify\n");
           TrimCmd( TRIM_HOST_VERIFY, NULL, 0 );
        }
        else if( Tst_Sflag( S_SPARES2 ) )
        {
           printf("Host has final verify\n");
           TrimCmd( TRIM_HOST_FINAL_V, NULL, 0 );
        }
        else
        {
          if ( DTM_Debug )
          {
             DTM_Debug = FALSE;
             printf("DTM debug off\n");
          }
          else
          {
             DTM_Debug = TRUE;
             printf("DTM debug on\n");
          }
        }
        break;

      case 'E':
         if ( Tst_Sflag( S_SPARES1 ))
         {
         }
         else if ( Tst_Sflag( S_SPARES2 ))
         {
           if( Text_Debug )

             Text_Debug = FALSE;
           else
             Text_Debug = TRUE;

           printf( Text_Debug ? "Text Debug ON\n":"Text Debug OFF\n" );
         }
         else if( Tst_Sflag( S_SPARES3 ) )
         {
           if( OF3_CANADA_USA )
             Clist2.OptionFlags3 &= ~CANADA_ENB;
           else
             Clist2.OptionFlags3 |= CANADA_ENB;

           printf( OF3_CANADA_USA ? "CANADA\n":"USA\n" );
           ConfigureEntry( 1 );
         }
         break;

      case 'e':
         if( Tst_Sflag( S_SPARES1 ) )
         {
            ascii_hex_dump( (char*)&Clist2, (int)sizeof(Clist2) );
         }
         else if (  Tst_Sflag( S_SPARES3 ) &&
            ( Clist.PRIVATE.PartNo == D27207 ))
         {
            if ( AL_Debug )
            {
               AL_Debug = FALSE;
               printf("turn off AutoList debug \n");
            }
            else
            {
               AL_Debug = TRUE;
               printf("turn on AutoList debug \n");
            }
         }
         else
         {
           if ( !Tst_Sflag( S_OCU_OFFLINE ))
           {
              switch( OCU_DownLoad( PCMCIA_OCU_ID ) )
              {
                 case 0:
                    printf( "\nattent to send code to OCU\n" );
                    break;
                 case 1:
                    printf( "\nno pc card, don't sent OCU code\n" );
                    break;
                 case 2:
                    printf( "\ninvalid id, don't sent OCU code\n" );
                    break;
                 case 3:
                    printf( "\n bad checksum on OCU code\n" );
                    break;
                 default:
                    printf( "\nno OCU download, unknown return\n" );
                    break;

              }
           }
           else
              printf( "\nOCU Offline\n" );
         }
         break;

      case 'F':
         DataStatus();
         break;

      case 'f':
        if ( Tst_Sflag( S_SPARES1 ) && Tst_Sflag( S_SPARES2 ))
        {
          printf("Trim Transfer control stuff \n");
          for( j = 0; j < MAX_TC; j++ )
          {
            memset( buf, 0, sizeof( buf ));
            memcpy( buf, TrimDlist.TransferControl[j].Text, TC_TS );
            printf("%Type %02x Attr %02x",
                    TrimDlist.TransferControl[j].Type, TrimDlist.TransferControl[j].Attr );
            printf( " trip %d expoff %04x  Text: %s\n",
                    TrimDlist.TransferControl[j].NumTrips, TrimDlist.TransferControl[j].ExpOff, buf );
          }
        }
        else if ( Tst_Sflag( S_SPARES1 ))
        {
          printf("Transfer control stuff \n");
          for( j = 0; j < MAX_TC; j++ )
          {
            memset( buf, 0, sizeof( buf ));
            memcpy( buf, Dlist.TransferControl[j].Text, TC_TS );
            printf("%Type %02x Attr %02x",
                    Dlist.TransferControl[j].Type, Dlist.TransferControl[j].Attr );
            printf( " trip %d expoff %04x  Text: %s\n",
                    Dlist.TransferControl[j].NumTrips, Dlist.TransferControl[j].ExpOff, buf );
          }
        }
        else if ( Tst_Sflag( S_SPARES3 ))
        {
          if ( j >= ML_SIZE  || j < 0 )
            j = 0;

          printf("Media stuff \n");
          for( n = 0; n < 16; n++, j++ )
          {
            memset( buf, 0, sizeof( buf ));
            memcpy( buf, Dlist.MediaList[j].Text, MEDIA_TS );
            printf( "Index %02d media %02x attr %02x Text: %s\n",
                    j, Dlist.MediaList[j].Media, Dlist.MediaList[j].Attr, buf );
            if( j >= (ML_SIZE-1) )
              n = 16;
          }
        }
        else if ( Tst_Sflag( S_SPARES2 ))
        {
          if ( j > 9 || j < 0 )
             j = 0;
          printf("TRIM fare stuff %d\n", j);
          q = (char*)&TrimDlist.FareTbl[j];
          for( i = 0; i < 256; i++ )
          {
            if ( i )
            {
              if ( ( i % 16 ) == 0 )
                putchar('\n');
              else if (  ( i % 2 ) == 0 )
                printf("  ");
            }
            printf("%02x", *q );
            q++;
          }
          j++;
          printf("\n");
        }
        else
        {
          if ( j > 9 || j < 0 )
             j = 0;
          printf("fare stuff %d\n", j);
          q = (char*)&Dlist.FareTbl[j];
          for( i = 0; i < 384; i++ )
          {
            if ( i )
            {
              if ( ( i % 24 ) == 0 )
                putchar('\n');
              else if (  ( i % 6 ) == 0 )
                printf("  ");
            }
            printf("%02x", *q );
            q++;
          }
          j++;
          printf("\n");
        }
        break;

      case 'G':
        if ( Tst_Sflag( S_SPARES1 ))
        {
           printf("clear OCU Ready count\n" );
           ReadyCount = 0;
        }
        else if ( Tst_Sflag( S_SPARES2 ))
        {
           set_status(  EDIT_MESSAGE );
           GetNewLEDTime();
           clr_status(  EDIT_MESSAGE );
        }
        else if ( Tst_Sflag( S_SPARES3 ))
        {
           set_status(  EDIT_MESSAGE );
           GetNewFS();
           clr_status(  EDIT_MESSAGE );
        }
        break;

      case 'g':
        if ( Tst_Sflag( S_SPARES1 ))
        {
          set_status(  EDIT_MESSAGE );
          GetTrimCommands();
          clr_status(  EDIT_MESSAGE );
          SendCardFlags();
          printf("TrimCflags %08lx\n", TrimCflags);
          TrimCmd( TRIMPROCESSCARD, NULL, 0 );
        }
        break;

      case 'H':
        if ( Tst_Sflag( S_SPARES1 ))
        {
           set_status(  EDIT_MESSAGE );
           GetTrimDebug();
           clr_status(  EDIT_MESSAGE );
           printf("\nNew Debug Level %02x\n", TrimDebugLevel);
           SendDebugLevel();
        }
        break;

      case 'h':
        if ( Tst_Sflag( S_BURNIN))
        {
          set_status(  EDIT_MESSAGE );
          memset( buf, 0, sizeof( buf ));
          printf("Hastus %lX\n", FbxCnf.HastusNo );
          putstr("  New: ");
          if( (n=Ngetx(buf, 8)) )
            FbxCnf.HastusNo = (long)_h2l( buf, n );
          printf("\n Hastus %lX\n", FbxCnf.HastusNo );
          clr_status(  EDIT_MESSAGE );
        }
        else
        {
           printf("\nFriendly List\n");
           ascii_hex_dump( (char*)&FriendlyAgy, (int)sizeof(FriendlyAgy) );
        }
        break;

      case 'I':
        printf("clear comm debug stuff\n");
        TrimCmd( TRIMCLEARDEBUG, NULL, 0 );
        memset( (void*)&RS485_rnr, 0, 20 );
        memset( (void*)&trim_RS485_rnr, 0, 20 );
        if ( Tst_Sflag( S_BURNIN ) && !TRIM_EXT_DIAG )
        {
           printf("enable extended debug stuff\n");
           Clist2.daylight |= EXT_TRIM_DIAG;
           Clist2_crc();
           SendStatus();
        }
        else if ( TRIM_EXT_DIAG )
           printf("extended debug enable\n");
        else
           printf("extended debug off\n");
        break;

	   case 'i':
/**** must uncomment in scip.c to allow to send J1708 messages ****
         if ( Tst_Sflag( S_SPARES3 ))
         {
           clrscr();
           printf(" j1708 transmit commands\n");
           printf(" 0  = req position       1 = req time            2 = req date\n");
           printf(" 3  = req time/date      4 = req driver          5 = req route\n");
           printf(" 6  = req driver/route   7 = req mile post       8 = req cbid\n");
           printf(" 9  = req probe type    10 = req probe id       11 = req bus no.\n");
           printf(" 12 = req status        13 = send software ver  14 = send bus no\n");
           printf(" 15 = send prb id/type  16 = send probe type    17 = send probe id\n");
           printf(" 18 = send cbid         19 = send position      20 = send time\n");
           printf(" 21 = send date         22 = send time/date     23 = send driver\n");
           printf(" 24 = send route        25 = send driver/route  26 = send mile post\n");
           printf(" 27 = send dvr log stat 28 = req pass cnt (0)   29 = req pass cnt (1)\n");
           printf(" 30 = req pass cnt(2)   31 = req pass clr (0)   32 = req pass clr (1)\n");
           printf(" 33 = req pass clr(2)   34 = req fs/dir         35 = send fs/dir \n");
           printf(" 36 = req sign          37 = send sign          38 = send Zone \n");
           printf(" 39 = send Hastus       40 = req Hastus                        \n");


           set_status( EDIT_MESSAGE );
           memset( buf, 0, sizeof( buf ));
           if( (j=Ngetn(buf, 2)) )
           {
              i = (word)_a2l( buf, j );
              J1708Request( i );
           }
           clr_status(  EDIT_MESSAGE );
         }
/***************************************************************/
         if ( Tst_Sflag( S_SPARES2 ))
         {
           clrscr();
           printf("Current j1708 debug: %d\r\n", J1708_Debug );
           set_status( EDIT_MESSAGE );
           i = getchar();
           switch ( i )
           {
             case '1':
             case '2':
               printf("J1708 recieve debug on\n");
               J1708_Debug = i & 0x0f;
               break;
             case '3':
             case '4':
               printf("J1708 transmit debug on \n");
               J1708_Debug = i & 0x0f;
               break;
             case '5':
               printf("J1708 recieve & transmit debug on\n");
               J1708_Debug = i & 0x0f;
               break;
             case '0':
             default:
               printf("J1708 debug off\n");
               J1708_Debug = 0;
               break;
           }
           clr_status(  EDIT_MESSAGE );
         }
         break;

      case 'J':
        Display_J1708_Diag();
        break;
      case 'j':
        if ( Tst_Sflag( S_DUMP ))
            i = 0;
        if ( !AutoLoadFlag )
        {
           printf("AutoList not valid\n");
           break;
        }
        if ( Tst_Sflag( S_SPARES1 ) && !Tst_Sflag( S_SPARES2|S_SPARES3 ))
        {
           r = &AutoLoadList;
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           j = 0;
           for ( i = 0; i < AutoListHdr.cnt; i++ )
           {
               printf(" section type       %d\n",    ((BASIC_PACK*)r)->type );
               printf(" data size          %d\n",    ((BASIC_PACK*)r)->sz );
               printf(" record cnt         %lu\n",   ((BASIC_PACK*)r)->ent_cnt );
               printf(" offset             %lx\n",   ((BASIC_PACK*)r)->offset );
               printf(" data address       %p\n\n",  &AutoLoadList+m+(((BASIC_PACK*)r)->offset-1) );
               j += sizeof( BASIC_PACK );
               r = &AutoLoadList+j;
           }
           printf( "Starting data addr  %p\n\n", &AutoLoadList+m );
           i = 0;
        }
        else if ( Tst_Sflag( S_SPARES2 ) && !Tst_Sflag( S_SPARES1|S_SPARES3 ))
        {
           r = &AutoLoadList;
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           f = 0;
           for ( n = 0; n < AutoListHdr.cnt; n++ )
           {
              if ( ((BASIC_PACK*)r)->type == SC_PRD_SINGLE )
                 break;
              f += sizeof( BASIC_PACK );
              r = &AutoLoadList+f;
           }

           if ( n < AutoListHdr.cnt )
           {
             if ( (l = ((BASIC_PACK*)r)->ent_cnt * ((BASIC_PACK*)r)->sz ) < AutoLoadSize )
             {
                 l = ((BASIC_PACK*)r)->ent_cnt;
                 if ( l <= i )
                    i = 0;

                 r = &AutoLoadList+m+(((BASIC_PACK*)r)->offset-1) + ( i * sizeof(SC_PRODUCT_SINGLES));

                 printf("Singles Entry no. %d  of %lu address %lX\n", i+1, l, r );
                 printf( " card type & UID : ");
                 for ( j = 0; j < SMARTCARD_ID_LEN; j++ )
                    printf( "%02x", ((SC_PRODUCT_SINGLES*)r)->card_id[j] );
                 printf( "\n  product         : %02x\n", ((SC_PRODUCT_SINGLES*)r)->product );
                 printf( " designator      : %d \n",  ((SC_PRODUCT_SINGLES*)r)->desig );
                 printf( " load seqeunce   : %u \n",  ((SC_PRODUCT_SINGLES*)r)->load_seq );
                 printf( " load type       : %u \n",  ((SC_PRODUCT_SINGLES*)r)->load_type );
                 printf( " third party code: %u \n",  ((SC_PRODUCT_SINGLES*)r)->tpbc );
                 printf( " fare id         : %u \n",  ((SC_PRODUCT_SINGLES*)r)->fare_id );
                 printf( " value           : %u \n",  ((SC_PRODUCT_SINGLES*)r)->value );
                 ++i;
             }
           }
        }
        else if ( Tst_Sflag( S_SPARES3 ) && !Tst_Sflag( S_SPARES1|S_SPARES2 ))
        {
           r = &AutoLoadList;
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           f = 0;
           for ( n = 0; n < AutoListHdr.cnt; n++ )
           {
              if ( ((BASIC_PACK*)r)->type == SC_PRD_RANGE )
                 break;
              f += sizeof( BASIC_PACK );
              r = &AutoLoadList+f;
           }

           if ( n < AutoListHdr.cnt )
           {
              if ( (l = ((BASIC_PACK*)r)->ent_cnt * ((BASIC_PACK*)r)->sz ) < AutoLoadSize )
              {
                 l = ((BASIC_PACK*)r)->ent_cnt;
                 if ( l <= i )
                    i = 0;

                 r = &AutoLoadList+m+(((BASIC_PACK*)r)->offset-1) + ( i * sizeof(SC_PRODUCT_RANGE));

                 printf("Range Entry no. %d  of %lu address %lX\n", i+1, l, r );
                 printf( " card printed start: %08lx%08lx\n",
                     ((SC_PRODUCT_RANGE*)r)->card_pidl[0], ((SC_PRODUCT_RANGE*)r)->card_pidl[1] );
                 printf( " card printed end  : %08lx%08lx\n",
                   ((SC_PRODUCT_RANGE*)r)->card_pidu[0], ((SC_PRODUCT_RANGE*)r)->card_pidu[1] );
                 printf( " product           : %02x\n",  ((SC_PRODUCT_RANGE*)r)->product );
                 printf( " designator        : %d\n",    ((SC_PRODUCT_RANGE*)r)->desig );
                 printf( " load seqeunce     : %lu\n",   ((SC_PRODUCT_RANGE*)r)->load_seq );
                 printf( " load type         : %d\n",    ( ((SC_PRODUCT_RANGE*)r)->load_type_tpb >> 12) & 0x000f );
                 printf( " tpbc              : %d\n",    ((SC_PRODUCT_RANGE*)r)->load_type_tpb & 0x0fff );
                 printf( " fare id           : %u\n",    ((SC_PRODUCT_RANGE*)r)->fare_id );
                 printf( " value             : %u\n",    ((SC_PRODUCT_RANGE*)r)->value );
                 ascii_hex_dump( (char*)r, (int)sizeof(SC_PRODUCT_RANGE) );
                 ++i;
              }
           }
           else
           {
             printf("no autoload ranges 3\n");
           }
        }
        else if ( Tst_Sflag( S_SPARES1 ) && Tst_Sflag( S_SPARES2 ) && !Tst_Sflag( S_SPARES3 ))
        {
           r = &AutoLoadList;
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           f = 0;
           for ( n = 0; n < AutoListHdr.cnt; n++ )
           {
              if ( ((BASIC_PACK*)r)->type ==  ePAY_USER_TBL )
                 break;
              f += sizeof( BASIC_PACK );
              r = &AutoLoadList+f;
           }

           if ( n < AutoListHdr.cnt )
           {
              if ( (l = ((BASIC_PACK*)r)->ent_cnt * ((BASIC_PACK*)r)->sz ) < AutoLoadSize )
              {
                 l = ((BASIC_PACK*)r)->ent_cnt;
                 if ( l <= i )
                    i = 0;

                 r = &AutoLoadList+m+(((BASIC_PACK*)r)->offset-1) + ( i * sizeof(EPAY_USER_TABLE2));
                 printf("EPay User Table no. %d  of %lu address %lX\n", i+1, l, r );
                 if ( AutoListHdr.version == 1 )
                 {
                    printf( " spare             : %x\n",    ((EPAY_USER_TABLE*)r)->spare );
                    printf( " user type         : %x\n",    ((EPAY_USER_TABLE*)r)->user_type );
                    printf( " fare id           : %x\n",    ((EPAY_USER_TABLE*)r)->fare_id );
                 }
                 else
                 {
                    printf( " user type         : %x\n",    ((EPAY_USER_TABLE2*)r)->user_type );
                    printf( " fare id           : %x\n",    Int( ((EPAY_USER_TABLE2*)r)->fare_id) );
                    printf( " SmartCard type    : %x\n",    ((EPAY_USER_TABLE2*)r)->card_type );
                 }
                 printf( " product type      : %d\n",    ((EPAY_USER_TABLE2*)r)->prod_type );
                 printf( " designator        : %d\n",    ((EPAY_USER_TABLE2*)r)->desig );
                 ++i;
              }
           }
           else
           {
             printf("no ePay USER Profle Table 8\n");
           }
        }
        else if ( Tst_Sflag( S_SPARES2 ) && Tst_Sflag( S_SPARES3 ) && !Tst_Sflag( S_SPARES1 ))
        {
           r = &AutoLoadList;
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           f = 0;
           for ( n = 0; n < AutoListHdr.cnt; n++ )
           {
              if ( ((BASIC_PACK*)r)->type == ePAY_FARE_TBL )
                 break;
              f += sizeof( BASIC_PACK );
              r = &AutoLoadList+f;
           }

           if ( n < AutoListHdr.cnt )
           {
              if ( (l = ((BASIC_PACK*)r)->ent_cnt * ((BASIC_PACK*)r)->sz ) < AutoLoadSize )
              {
                 l = ((BASIC_PACK*)r)->ent_cnt;
                 if ( l <= i )
                    i = 0;

                 r = &AutoLoadList+m+(((BASIC_PACK*)r)->offset-1) + ( i * sizeof(EPAY_FARE_TABLE));

                 printf("EPAY fare table Entry no. %d  of %lu address %lX\n", i+1, l, r );
                 printf( " fare id           : %x\n",    ((EPAY_FARE_TABLE*)r)->fare_id );
                 printf( " fare type         : %d\n",    ((EPAY_FARE_TABLE*)r)->fare_type & 0x0f );
                 printf( " product type      : %d\n",    (((EPAY_FARE_TABLE*)r)->fare_type>>4) & 0x03 );
                 printf( " active/bad        : %s/%s\n", (((EPAY_FARE_TABLE*)r)->fare_type & 0x40 ? "yes" : "no " ),
                                                         (((EPAY_FARE_TABLE*)r)->fare_type & 0x80 ? "yes" : "no " ));
                 printf( " designator        : %d\n",    ((EPAY_FARE_TABLE*)r)->desig );
                 printf( " price             : %lu\n",   ((EPAY_FARE_TABLE*)r)->price );
                 printf( " value             : %u\n",    ((EPAY_FARE_TABLE*)r)->value );
                 printf( " details           : %d\n",    ((EPAY_FARE_TABLE*)r)->flag );
                 printf( " exp type          : %d\n",    ((EPAY_FARE_TABLE*)r)->exp_type );
                 printf( " start date        : %u\n",   ((EPAY_FARE_TABLE*)r)->start_date );
                 printf( " end date          : %u\n",   ((EPAY_FARE_TABLE*)r)->end_date );
                 printf( " add time          : %u\n",   ((EPAY_FARE_TABLE*)r)->add_time );
                 ++i;
              }
           }
           else
           {
             printf("no ePay Fare Table 9\n");
           }
        }
        else
        {
           i = 0;
           printf( "\n Auto Load header file \n");
           printf( "crc                %lX\n", AutoListHdr.crc32 );
           printf( "signiture crc      %lX\n", *(long*)&AutoLoadList[AutoLoadSize] );
           printf( "category           %c\n",  AutoListHdr.cat );
           printf( "type               %u\n",  AutoListHdr.type );
           printf( "version            %u\n",  AutoListHdr.version  );
           printf( "flag               %x\n",  AutoListHdr.flag );
           printf( "platform src       %c\n",  AutoListHdr.platform_src );
           printf( "platform dest      %c\n",  AutoListHdr.platform_target );
           printf( "platform id        %lu\n", AutoListHdr.platform_id );
           printf( "seqence no.        %lu\n", AutoListHdr.seq );
           printf( "time created       %lX\n", AutoListHdr.t );
           tm = gmtime( &AutoListHdr.t );
           printf("  time: %02d:%02d:%02d  date: %02d/%02d/%02d\n",
                   tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_mon+1, tm->tm_mday, tm->tm_year );
           printf( "Total lists        %u\n",  AutoListHdr.cnt );
           printf( "Data properties    %s\n",  AutoListHdr.sec_flag ? "MOTOROLA" : "INTEL" );
           printf( "Total List size    %lu\n", AutoLoadSize );
           printf( "Signiture address  %p\n",  &AutoLoadList[AutoLoadSize] );
           printf( "AutoLoadList flag  %c\n",  AutoLoadFlag ? 'G' : 'B' );
           m = AutoListHdr.cnt * (sizeof(BASIC_PACK));
           printf( "Starting data addr %p\n", &AutoLoadList+m );
        }
        break;

      case 'K':
          /*
          printf("request OCU status\n");
          OCU_Msg( OID_ALL, OM_REQSTATUS, NULL, 0 );
          */
          if( Tst_Sflag( S_SPARES3 ) )
          {
            OTIprintf( OTI_DO, 0, 0, "Hello World" );
            OTIprintf( OTI_DO, 0, 0, "Hello World" );
            OTIprintf( OTI_DO, 0, 0, "Hello World" );
          }
		  break;

      case 'k':
		  if ( Tst_Sflag( S_SPARES3 ))
		  {
           printf( "iniate shutdown\n");
           Stop_Tmr( AUTO_DUMP_TMR );
           Stop_Tmr( IDLE_MISC_TMR );
           OCU_BlankDisplay( 0, 0, 0, NULL );
           Init_Tmr_Fn( AUTO_LOG_TMR, AutoDriverLogOff, SECONDS( 15 ) );
		  }/*
        else if ( Tst_Sflag( S_SPARES1 ))
        {
           OpenRemoteDoor( Door_time );
        }*/
        break;

      case 'L':
        if ( Tst_Sflag( S_BURNIN ))
        {
          i = 0;   /* init for driver list display */
        }
        else
        {
          if ( Tst_Sflag( S_SPARES3 ))
          {
            set_status(  EDIT_MESSAGE );
            clrscr();
            printf("MemClrPW Code %lu\n", Clist2.MemClrPW );
            memset( buf, 0, sizeof( buf ));
            if( (n=Ngetn(buf, 6)) )
            {
              Clist2.MemClrPW = _a2l( buf, n );
              ConfigureEntry( 1 );
              printf("\nMemClrPW code %lu\n", Clist2.MemClrPW );
            }
            clr_status(  EDIT_MESSAGE );
          }
          break;
        }
      case 'l':
        if ( Tst_Sflag( S_BURNIN ) )
        {
          if ( i >= DVR_SIZE )
            i = 0;
          for ( n = 0; n < 20; n++ )
          {
            printf( "Driver %4d ID", i );
            for ( x = 0; x < 5; x++ )
            {
              printf( "  %6lu", DriverList.id[i] );
              if ( ++i >= DVR_SIZE )
              {
                i = x = 0;
                putchar('\n');
              }
            }
            putchar('\n');
          }
        }
        else
        {
          if ( Tst_Sflag( S_SPARES1 ) )
             i = 0;

          if( !OF6_ROUTE_MATRIX )
          {
            if ( i >= RT_SIZE )
              i = 0;
            for ( n = 0; n < 20; n++ )
            {
              printf( "Route %4d ID", i );
              for ( x = 0; x < 5; x++ )
              {
                printf( "  %6lu", RouteList.no[i] );
                if ( ++i >= RT_SIZE )
                  i = 0;
              }
              putchar('\n');
            }
          }
          else
          {
            if ( i >= RouteLength )
              i = 0;

            d = (char*)&RouteList;
            for ( n = 0; n < 20; n++ )
            {
              printf( "Route %4d ", i );
              for ( x = 0; x < 20; x++ )
              {
                printf( " %02x", *(d+i) );
                if ( ++i >= RouteLength )
                {
                   i = 0;
                   n = x = 20;
                }
              }
              putchar('\n');
            }
          }
        }
   	  break;

      case 'M':
        if( Tst_Sflag( S_SPARES3 ))
        {
          Clist2.BillsTaken = 0;
          Clist2_crc();
        }
        else
        {
          char  *d = buf;
          Printed_ID[0] = Printed_ID[1] = 0;
          memset( buf, 0, sizeof( buf ));
          printf( "convert ascii string to hex \n");
          set_status(  EDIT_MESSAGE );

          if( (n=Ngets( d, 16)) )
             StoDouble( d, &Printed_ID, n );
          clr_status(  EDIT_MESSAGE );
          printf("%lx%lx\n", Printed_ID[0], Printed_ID[1] );
        }
        break;

      case 'm':
         if ( Tst_Sflag( S_BURNIN ))
         {
            set_status(  EDIT_MESSAGE );
            clrscr();
            menu();
            clrscr();
            clr_status(  EDIT_MESSAGE );
            if ( Tst_Aflag( A_WIPORT_UPDATE ))
              Fetch_MAC_address();
         }

         if( Tst_Sflag( S_SPARES3 ))
         {
            /*ReqTrimSCInfo();*/

         }
         break;

      case 'N':
         if ( IRMA_Poll )
         {
            printf("disable IRMA poll\n");
            IRMA_Poll = FALSE;
         }
         else
         {
            printf("enable IRMA poll\n");
            IRMA_Poll = TRUE;
         }
        break;

      case 'n':
        printf("bill validator status mode %d: ", Mode );
        if ( Tst_Sflag( S_BILL_OOS|S_BILL_JAM ))
        {
           if ( Tst_Sflag( S_BILL_OOS ) )
              printf( " BILL OOS" );
           if ( Tst_Sflag( S_BILL_JAM ) )
              printf( " BILL FEED" );
           printf("\n");
        }
        else
           printf(" O.K.\n");
        break;

      case 'O':
         if ( Tst_Sflag( S_SPARES3 ))
         {
             printf("OCU retry %d  Retry Max %d nak %d\n", RetryCount232, RetryMax232, NakCount232);
             tm = gmtime( &StartTime232 );
             printf("%02d:%02d:%02d  %02d/%02d\n",
                 tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_mon+1, tm->tm_mday );
         }
         break;

	  case 'o':
         clrscr();
          if ( Tst_Sflag( S_BURNIN ))
          {
            if ( i >= RUN_SIZE )
              i = 0;
            if ( i == 0 )
               printf( "Run crc %04x\n", RunList.crc );
            for ( n = 0; n < 20; n++ )
            {
              printf( "Run %4d ID ", i );
              for ( x = 0; x < 4; x++ )
              {
                printf( "  %6lu", RunList.no[i] );
                if ( ++i >= RUN_SIZE )
                  i = 0;
              }
              putchar('\n');
            }
          }
          else if ( Tst_Sflag( S_SPARES1 ))
          {
            if ( i >= TRIP_SIZE )
              i = 0;
            if ( i == 0 )
               printf( "Trip crc %04x\n", TripList.crc );
            for ( n = 0; n < 20; n++ )
            {
              printf( "Trip %4d ID", i );
              for ( x = 0; x < 4; x++ )
              {
                printf( "  %6lu", TripList.no[i] );
                if ( ++i >= TRIP_SIZE )
                  i = 0;
              }
              putchar('\n');
            }
          }
          else
          {
            if ( i > 3 )
               i = 0;

            switch( i )
            {
               case 0:
                 p = (long*)&RangeList.dvr;
                 printf( "Driver Ranges crc %04x\n", RangeList.crc );
                 break;
              case 1:
                 p = (long*)&RangeList.rte;
                 printf( "Route Ranges crc %04x\n", RangeList.crc );
                 break;
              case 2:
                 p = (long*)&RangeList.run;
                 printf( "Run Ranges crc %04x\n", RangeList.crc );
                 break;
              case 3:
              default:
                 p = (long*)&RangeList.trip;
                 printf( "Trip Ranges crc %04x\n", RangeList.crc );
                break;
            }
            for ( j = 0; j < 16; j++ )
            {
              for ( x = 0; x < 4; x++, p++ )
                printf( "  %6lu", *p );
              putchar('\n');
            }
            ++i;
          }
/*
         x = _LEFT;
         y = 2;

         xyprintf( x, y++, "    FAREBOX" );
         xyprintf( x, y++, "rs485_rnr       %6u", RS485_rnr );
         xyprintf( x, y++, "rs485_rej       %6u", RS485_rej );
         xyprintf( x, y++, "rs485_lrc       %6u", RS485_lrc );
         xyprintf( x, y++, "rs485_nak       %6u", RS485_nak );
         xyprintf( x, y++, "rs485_sabm      %6u", RS485_rnr );
         xyprintf( x, y++, "rs485_seq       %6u", RS485_seq );
         xyprintf( x, y++, "rs485_character %6u", RS485_sto );
         xyprintf( x, y++, "rs485_packet    %6u", RS485_rto );
         xyprintf( x, y++, "rs485_of_fr     %6u", RS485_err );
         xyprintf( x, y++, "rs485_lost      %6u", RS485_lost );

         x = 28;
         y = 2;
         xyprintf( x, y++, "    TRIM" );
         xyprintf( x, y++, "rs485_rnr       %6u", trim_RS485_rnr );
         xyprintf( x, y++, "rs485_rej       %6u", trim_RS485_rej );
         xyprintf( x, y++, "rs485_lrc       %6u", trim_RS485_lrc );
         xyprintf( x, y++, "rs485_nak       %6u", trim_RS485_nak );
         xyprintf( x, y++, "rs485_sabm      %6u", trim_RS485_rnr );
         xyprintf( x, y++, "rs485_seq       %6u", trim_RS485_seq );
         xyprintf( x, y++, "rs485_character %6u", trim_RS485_sto );
         xyprintf( x, y++, "rs485_packet    %6u", trim_RS485_rto );
         xyprintf( x, y++, "rs485_of_fr     %6u", trim_RS485_err );
         xyprintf( x, y++, "rs485_lost      %6u", trim_RS485_lost );

         printf("\n\n");

         TrimCmd( TRIM_DIAG_REC, NULL, 0 );
*/
         break;

      case 'P':
          i = 0;
      case 'p':
          for ( n = 0; n < 20; n++ )
          {
             if ( i >= PBQSIZE )
                 i = 0;
             q = (byte*)&Pbq[i];
             printf("pass back que %d: ", i );
             for ( j = 0; j < CCSNS; ++j, ++q )
                 printf("%02x ", *q );
             printf("\n" );
             ++i;
          }
          break;

      case  'Q':
         IntializeBillDiagCounts();
         printf("Init bill diagnostic counters\n");
         /* fall through to display counts */
      case  'q':
         if ( Tst_Sflag( S_SPARES3))
         {
            ascii_hex_dump( (char*)&ZoneTable, ZoneLength );
         }
         else
            debug_display_comm_error_counters();

         break;

      case 'r':
         if ( Tst_Sflag(S_BURNIN))
         {
            ascii_hex_dump( (char*)&wiport, (int)sizeof(wiport) );
         }
         else
         {
           if ( Chopper_Test_Count && tmr_expired( BILL_ELEVATOR_TMR ) )
              Bill_Transport( 20 );
         }
         break;
      case 'R':
         if ( Tst_Sflag( S_BURNIN ))
         {
           set_status( EDIT_MESSAGE );
           printf("Probe watchdog time " );

           memset( buf, 0, sizeof( buf ));
           if( (j=Ngetn(buf, 4)) )
           {
              l = _a2l( buf, j );
           }
           printf("\n" );
           time( &t );
           Clist2.ProbeWatchDog = l;
           ProbeTime = t + l;

           clr_status(  EDIT_MESSAGE );
         }
         else
         {
            time( &t );
            printf( "current time %ld probe time %ld\n",
               t, ProbeTime );
         }
         break;

      case 'S':
         i = 0;
      case 's':
         WatchDog( _WDT_SERVICE );
         if ( Tst_Sflag(S_SPARES3))
         {
           for ( x = 0; x < 16;  x++ )
           {
             q = (byte*)&Dlist.BadListRanges[x];
             printf("serial range %d: ", x );
		       for ( j = 0; j < MSNS; ++j, ++q )
               printf("%02x ", *q );
             printf(" -  ");
             for ( j = 0; j < MSNS; ++j, ++q )
               printf("%02x ", *q );
             printf("\n" );
           }
         }
         else if ( Tst_Sflag(S_SPARES2) && !Tst_Sflag(S_SPARES1))
         {
           n = 100;
           if ( i < 0 || i >= 100 )
             i = 0;
           q = (byte*)&Dlist.BadList[i];

           for ( x = 0; x < 20;  x++, i++  )
           {
              if ( i >= n  )
              {
                 i = 0;
                 q = (byte*)&Dlist.BadList[i];
              }
              printf("BBserial %d: ", i );
		        for ( j = 0; j < MSNS; ++j, ++q )
                printf("%02x ", *q );
              printf("\n" );
           }
         }
         else if ( Tst_Sflag( S_SPARES1 ) && !Tst_Sflag( S_SPARES2 ))
         {
           if ( Tst_Pflag( P_BIG_BADLIST ))
             n = 10000;
           else
             n = 2500;

		     if ( i < 0 || (i+3) >= n )
		       i = 0;

		     index = (word*)&BBIndex[i];

           for ( x = 0; x < 20;  x++ )
           {
              if ( (i+3) >= n  )
              {
                i = 0;
                index = (word*)&BBIndex[i];
              }
              if ( Tst_Pflag( P_BIG_BADLIST ))
      		    printf("Big BIndex");
              else
                printf("Bindex");

              printf(" %04d: %04d  %04d  %04d  %04d  %04d\n",
                      i, *index, *(index+1), *(index+2), *(index+3), *(index+4) );
              i+=5;
              index +=5;
           }
           printf("\n" );

         }
         else if ( Tst_Sflag( S_SPARES1 ) && Tst_Sflag( S_SPARES2 ))
         {
           if ( Tst_Pflag( P_BIG_BADLIST ))
             n = 10000;
           else
             n = 2500;

		     if ( i < 0 || i >= n )
		       i = 0;
           q = (char*)&BBlist.BadList[BBIndex[i]];

           for ( x = 0; x < 20;  x++, i++  )
           {
              if ( i >= n  )
              {
                i = 0;
              }
              q = (byte*)&BBlist.BadList[BBIndex[i]];

              if ( Tst_Pflag( P_BIG_BADLIST ))
      		    printf("Really BBserial %d: ", i );
              else
   		       printf("BBserial %d: ", i );
		        for ( j = 0; j < CCSNS; ++j, ++q )
			       printf("%02x ", *q );
	           printf("\n" );
           }
           printf("\n" );

         }
         else
         {
           if ( Tst_Pflag( P_BIG_BADLIST ))
             n = 10000;
           else
             n = 2500;

		     if ( i < 0 || i >= n )
		       i = 0;

           q = (byte*)&BBlist.BadList[i];

           for ( x = 0; x < 20;  x++, i++  )
           {
              if ( i >= n  )
              {
                i = 0;
                q = (byte*)&BBlist.BadList[i];
              }

              if ( Tst_Pflag( P_BIG_BADLIST ))
                printf("Really BBserial %d: ", i );
              else
                printf("BBserial %d: ", i );

		        for ( j = 0; j < CCSNS; ++j, ++q )
			       printf("%02x ", *q );
	           printf("\n" );
           }
           printf("\n" );
         }
         break;

      case 't':
         if ( Tst_Sflag( S_BURNIN ))
         {
           set_status( EDIT_MESSAGE );
           printf("Montreal leds " );

           memset( buf, 0, sizeof( buf ));
           if( (j=Ngetn(buf, 3)) )
           {
              ch = (byte)_a2l( buf, j );
              Canada_Lamps( ch );
              Stop_Tmr( GREEN_LAMP_TMR );
           }
           printf("\n" );
           clr_status(  EDIT_MESSAGE );
         }
         break;

      case 'T':
         if ( Tst_Sflag( S_BURNIN ))
         {
           set_status( EDIT_MESSAGE );
           printf("Sound select " );

           memset( buf, 0, sizeof( buf ));
           if( (j=Ngetn(buf, 2)) )
           {
              i = (word)_a2l( buf, j );
           }
           printf("\n" );
           clr_status(  EDIT_MESSAGE );
           if ( i <= SoundEnd )
           {
             switch( i )
             {
               case DOUBLE_BEEP:
                 DoubleBeep;
                 break;
               case TRIPPLE_BEEP:
                 TripleBeep;
                 break;
               default:
                 Sound( i );
                 break;
             }
           }
           else
           {
             printf("bad sound %d\n", i );
           }
         }
         else if ( Tst_Sflag( S_SPARES3 ))
         {
            if ( Sound_Debug )
            {
               Sound_Debug = FALSE;
               printf("Disable Sound Debug\n");
            }
            else
            {
               Sound_Debug = TRUE;
               printf("Enable Sound Debug\n");
            }
         }
         break;

      case 'U':
         i = 0;
      case 'u':
         if ( i > SoundEnd )
           i = 0;
         printf("voice %d\n", i);
         switch( i )
         {
           case DOUBLE_BEEP:
             DoubleBeep;
             break;
           case TRIPPLE_BEEP:
             TripleBeep;
             break;
          default:
             Sound( i );
             break;
         }

         i++;
         break;

      case 'V':
         i += 100;
         if ( Tst_Pflag( P_BIG_BADLIST ))
            n = 10000;
         else
            n = 2500;
		   if ( i < 0 || i >= n )
		   	i = 0;
         printf("Bad list index at %d\n", i );
         break;

      case 'v':
         if ( Tst_Sflag( S_BURNIN ))
         {
            if ( VOICE_ENABLED )
            {
                printf("SOUND\n");
                Clist2.OOS_coin_bill &= ~C_VOICE;
            }
            else
            {
                printf("VOICE\n");
                Clist2.OOS_coin_bill |= C_VOICE;
            }
            Clist2_crc();
         }
         break;

      case 'W':
         if ( Tst_Sflag( S_SPARES1 ))
         {
            for( i = 0, j = 0; i < 20; i++ )
            {
               if ( Ml.evnt_c < MAX_EV )
               {
                  NewEv( ++j, 0 );
                  if ( j > 10 )
                     j = 0;
               }
               else
                  break;
            }
            for( i = 0, j = 0; i < 20; i++ )
            {
                if ( j%4 )
                   T_route_change( j );
                else if ( j%3  )
                   T_run_change( j );
                else if ( j%2 && j != 0 )
                   T_driver_change( FbxCnf.driver );
                else
                   T_trip_change( j );
                j++;
            }
         }
         else if ( Tst_Sflag( S_SPARES2 ))
         {
            for( i = 0, j = 0; i < 50; i++ )
            {
               if ( Ml.evnt_c <  MAX_EV )
               {
                  NewEv( ++j, 0 );
                  if ( j > 10 )
                     j = 0;
               }
               else
                  break;
            }

            for( i = 0; i < 200; i++ )
            {
               T_Special_Pass( (void*)&SW_Card, 0 );
            }
         }
         else if ( Tst_Sflag( S_SPARES3 ))
         {
            switch( PrbDebug )
            {
              case 0:
                printf("DO NOT CLEAR after probing\n");
                PrbDebug = TRUE;
                break;
              case 1:
                printf("CLEAR after probing and show ACS stuff\n");
                PrbDebug = 2;
                break;
              default:
                printf("turn OFF probing debug \n");
                PrbDebug = FALSE;
                break;
            }
         }
         break;

      case 'w':
         if ( Tst_Sflag( S_SPARES2 ))
           TrimCmd( TRIM_POWER_DOWN_CMD, NULL, 0 );
         else
         {
           if ( i > 5 )
             i = 0;

           j = 128 + ( i*20 );
           for( n = 0; n < 20; j++, n++ )
             buf[n] = j;
           LCDprintf( 0, 0, buf );
           for( n = 0; n < 20; j++, n++ )
             buf[n] = j;
           LCDprintf( 0, 1, buf );
           for( n = 0; n < 20; j++, n++ )
             buf[n] = j;
           LCDprintf( 0, 2, buf );
           for( n = 0; n < 20; j++, n++ )
             buf[n] = j;
           LCDprintf( 0, 3, buf );
           ++i;
           LCDIdleMsg( SECONDS( 60 ) );

         }
         break;

      case 'x':
         if ( Tst_Sflag( S_BURNIN ))
         {
            if( TrimStatusLED )
            {
               Clist2.logon &= ~TRIM_STAT_LED;
               printf( "turn off trim watchdog led\n");
            }
            else
            {
               Clist2.logon |= TRIM_STAT_LED;
               printf( "flash trim watchdog led\n");
            }
            Clist2_crc();
         }

         if ( IRMAP_Check )
         {
            GPSReqPassComp( 0 );
            printf("request people counter component info\n");
         }
         break;
      case 'X':
         printf(" Background sort bad list\n");
         Background_Sort();
/*         if ( IRMAP_Check )
         {
            GPSReqPassDiag( 0 );
            printf("request people counter diag\n");
         }
*/
         break;
      case 'y':
           set_status( EDIT_MESSAGE );
           printf("set index " );

           memset( buf, 0, sizeof( buf ));
           if( (j=Ngetn(buf, 4)) )
           {
              i = (word)_a2l( buf, j );
           }
           clr_status(  EDIT_MESSAGE );

/*         if ( IRMAP_Check )
         {
            GPSReqPassCnt( 0 );
            printf("request people count\n");
         }
*/
         break;
      case 'Y':
         if ( IRMAP_Check )
         {
            GPSReqPassClr( 0 );
            printf("reset people count\n");
         }
         TrimPrintString( "\C0310Test Print Message" );
         break;

      case 'z':

         printf( "PrbOvrErr = %u, PrbFrmErr = %u, PrbBrkErr = %u\n",
             PrbOvrErr, PrbFrmErr, PrbBrkErr );

         printf( "PrbNack = %u, PrbSeqErr = %u, PrbLrcErr = %u PrbRecFull = %u PrbRetransmit = %u\n",
             PrbNack, PrbSeqErr, PrbLrcErr, PrbRecFull, PrbRetransmit );

         printf("ProbeTime =%ld\n", PrbTimer2 );

         if ( IRMAP_Check )
         {
            GPSReqPassSoftware( 0 );
            printf("request people counter software info\n");
         }
         break;

      case 'Z':
         if ( Tst_Sflag( S_SPARES1 ))
         {
         }
         else
         {
           printf( "reset comm counters\n" );
           PrbOvrErr = PrbFrmErr = PrbBrkErr = 0;
           PrbNack = PrbSeqErr = PrbLrcErr = PrbRecFull = PrbRetransmit= 0;
           PrbTimer = PrbTimer2 = 0;

           if ( IRMAP_Check )
           {
              clrscr();
              printf( "IRMA make        %s\n", PatronCnt.make );
              printf( "No. of Doors     %d\n", PatronCnt.model );
              printf( "IRMA sn          %s\n", PatronCnt.sn );
              printf( "IRMA Software    %s %s\n", PatronCnt.software1,
                 PatronCnt.software2 );
              printf( "IRMA diag        %s\n", PatronCnt.mdiag );
              if ( PatronCnt.sensor_cnt )
              {
                 for ( i = 0; i < PatronCnt.sensor_cnt; i++ )
                    printf( "     sensor %d   %x\n", i, PatronCnt.sensor[0] );
              }
              printf( "Entries          %d\n", PatronCnt.entry );
              printf( "Exits            %d\n", PatronCnt.exit );
           }
         }
         break;

      case ')':
         if( AES_key_version_index == DESFIRE_ACTUAL_CARD_KEY_VERSION_INDEX )
         {
            printf("AES key version index = TEST CARD\n");
            AES_key_version_index = DESFIRE_TEST_CARD_KEY_VERSION_INDEX;
         }
         else
         {
            printf("AES key version index = ACTUAL CARD\n");
            AES_key_version_index = DESFIRE_ACTUAL_CARD_KEY_VERSION_INDEX;
         }
         break;

      case  '?':
         if( AES_key_version_index == DESFIRE_ACTUAL_CARD_KEY_VERSION_INDEX )
         {
            printf( "AES key version index = ACTUAL CARD\n" );
         }
         else if( AES_key_version_index == DESFIRE_TEST_CARD_KEY_VERSION_INDEX )
         {
            printf( "AES key version index = TEST CARD\n" );
         }
         else
         {
            printf( "AES key version index = UNKNOWN (%d)\n",
                    AES_key_version_index );
         }
         break;

      case  TESTSET_CHAR:
         break;

      default:
         if ( !Tst_Sflag( S_TESTSET ) )
            putchar( ch );
         break;
      }
   }
}

static
void Issue_New_( void )
{
   int      i;

   if ( ++TrimCard.SeqNo > 0xffffff )
     TrimCard.SeqNo = 0;

   if ( TrimCard.FirstUse )
     TrimCard.StartDate = ROLLING_START;

   PutCard( &TrimCard, 2 );
   TrimEncodeData( &MagStripeData, REVERSE_DATA, 0 );

   printf( "\n New Mag Data ( MagStripeData ): \n" );
   for( i=0; i<sizeof(MagStripeData); i++ )
      printf( "%02X ", MagStripeData[i] );
   putchar( '\n' );

}

void SetPrinterBaudRate( void )
{
   int i;

   clrscr();
    printf("Current Printer Baudrate: ");
    switch( PrntrBaud )
    {
      case B_19200:
        printf( "19200\n" );
        break;
      case B_38400:
        printf( "38400\n" );
        break;
      case B_57600:
        printf( "57600\n" );
        break;
      case B_115200:
        printf( "115200\n" );
        break;
      case B_9600:
        printf( "9600\n" );
        break;
      default:
        PrntrBaud = B_9600;
        printf( " fix to 9600\n" );
        break;
    }

    printf("Select Printer Baudrate: \n");
    printf("   1) 9600\n   2) 19200\n   3) 38400\n   4) 57600\n   5) 115200\n");
    i = getchar();
    if ( (i >= '1') && (i<= '5') )
    {
       switch( i )
       {
         case '1':
           PrntrBaud = B_9600;
           break;
         case '2':
           PrntrBaud = B_19200;
           break;
         case '3':
           PrntrBaud = B_38400;          /* set to 38400 */
           break;
         case '4':
           PrntrBaud = B_57600;          /* set to 57600 */
           break;
         case '5':
           PrntrBaud = B_115200;         /* set to 115200 */
           break;
        }
        printf("New Printer Baudrate: ");
        switch( PrntrBaud )
        {
          case B_19200:
            printf( "19200\n" );
            break;
          case B_38400:
            printf( "38400\n" );
            break;
          case B_57600:
            printf( "57600\n" );
            break;
          case B_115200:
            printf( "115200\n" );
            break;
          case B_9600:
            printf( "9600\n" );
            break;
          default:
            printf("what ???\n");
            break;
      }

      if ( OF12_BARCODE_PRINT && NO_TRIM )
         InitializePrinterProtocol();
    }
}

static
void CurrentInfo()
{
/*   static int  x, y, i, end, top_end, bf; */
/*  static long cf; */
static int  x, y, i, top_end;
   time_t t;
   char buff[22];

  clrscr();

   x = _U_LEFT;
   y = _UPPER;

  xyprintf( x, y++, "Agency ...: %04d %04d", MyAgency, MyOldAgency   );

  if( Tst_Aflag( A_CARDQUEST ) ) /* v1.65 - AAT */
     xyprintf( x, y++, "CardQuest : %ld",     FbxCnf.fbxno                );
  else
     xyprintf( x, y++, "Farebox ..: %ld",     FbxCnf.fbxno                );
  xyprintf( x, y++, "TRiM No ..: %-8ld",   (long*)Ml.trim_n            );

  if( Tst_Aflag( A_CARDQUEST ) ) /* v1.65 - AAT */
     xyprintf( x, y++, "BillVal ..: N/A" );
  else
     xyprintf( x, y++, "BillVal ..: %ld\n",   BillValSerial               );

  xyprintf( x, y++, "SmartCard : %ld\n",   SC_ReaderSerial             );

  if( Tst_Aflag( A_CARDQUEST )) /* v1.65 - AAT */
    xyprintf( x, y++, "CBID .....: N/A" );
  else
    xyprintf( x, y++, "CBID .....: %ld",     Ml.cbx_n                    );

  xyprintf( x, y++, "Probe ID .: %-8ld",   Prb_Req_Parms.prbID         );

  xyprintf( x, y++, "RF MAC....: %-s",     MAC_address );

  xyprintf( x, y++, "WPOFF Tmr.: %lu\n", Get_remaining_Tmr_time( WI_PORT_OFF_TMR ) );

  x = _CENTER;
  y = 2;

  if( Tst_Aflag( A_CARDQUEST ) ) /* v1.65 - AAT */
     xyprintf( x, y++, "CQ  D25718: %-5ld-%-8ld",(long*)Clist.PRIVATE.PartNo,
                                               (long*)Mlist.Config.FareBoxVer );
  else
     xyprintf( x, y++, "Fbx D25718: %-5ld-%-8ld",(long*)Clist.PRIVATE.PartNo,
                                               (long*)Mlist.Config.FareBoxVer );

  xyprintf( x, y++, "OCU ......: %-5ld-%-8d", OCU_Part_No, OCU_ver);

  if( Tst_Sflag( S_TRIMOFFLINE ) )
  {
     if( OF12_BARCODE_PRINT )
     {
        if ( Tst_S2flag( S2_PRINTER_ONLINE ) )
        {
           xyprintf( x, y++, "Printer ..: %s-%s", OmniPrnt_modle, OmniPrnt_ver );
        }
        else
           xyprintf( x, y++, "TRiM .....: OFF LINE" );
     }
     else
        xyprintf( x, y++, "TRiM .....: OFF LINE" );
  }
  else
  {
    xyprintf( x, y++, "TRiM .....: %-d-%-8ld",  Trim_Diag.partnum,
                                                  (long*)Ml.trim_v);
    if ( Tst_Aflag( A_SMART_TRIM ))
    {
      if( Tst_Tflag( TRIM_SC_ONLINE ) )
      {
        xyprintf( x, y++, "TRiM SCard: %-d.%d", TSC_base, TSC_ver   );
      }
      else
      {
         xyprintf( x, y++, "TRiM SCard: offline" );
      }
    }
  }
  if( SC_READER != DISABLE_READER && SC_READER != TRI_READER )
  {
    if ( !Tst_Sflag( S_SC_OFFLINE ) )
      xyprintf( x, y++, "Smart Card: %-d.%d", SSC_base, SSC_ver   );
    else
      xyprintf( x, y++, "Smart Card: OFF LINE" );
  }

  if( Tst_Aflag( A_CARDQUEST )) /* v1.65 - AAT */
     xyprintf( x, y++, "Bill Val..: N/A" );
  else
     xyprintf( x, y++, "Bill Val..: %-d.%d", bv_model_number, bv_revision_number  );

  xyprintf( x, y++, "Data Sys .: %-d",    Prb_Req_Parms.ver       );

   if ( OF6_RF_PORT )
   {
      xyprintf( x, y++, "RF .......: %-s", RF_Software_Ver );
      xyprintf( x, y++, "RF pwr/acc: %s/%s",
            (Tst_Aflag( A_WIPORT_POWER ) ? " No" : "Yes" ), (Radio_Check() ? " No" : "Yes") );
      xyprintf( x, y++, "IP: %03u.%03u.%03u.%03u",
          wiport.ip0, wiport.ip1, wiport.ip2, wiport.ip3 );
   }
   else
   {
      xyprintf( x, y++,   "RF .......: disabled" );
   }


   if ( y > top_end )
      top_end = y;

   x = _U_RIGHT;
   y = _UPPER;

   xyprintf( x, y++, "Lock: ");
  for ( i = 0; i < 5; i++ )
    printf( "%c", Dlist.LockCode[i]);

  /*
  printf( "    Old Lock: ");
  for ( i = 0; i < 5; i++ )
    printf( "%c", OldLockCode[i] );
   */
   xyprintf( x,    y++, "Events ...: %d", MaxEv );
   xyprintf( x+17, y,   "LED Tm .: %usec", Clist2.LEDTime );
   xyprintf( x,    y++, "Passback .: %0dmin", Clist.PRIVATE.PassBackDuration );
   xyprintf( x+17, y,   "Dump Tm.: %02dsec", Clist.PRIVATE.AutoDumpTmo  );
   xyprintf( x,    y++, "BillsTaken: %02X", Clist2.BillsTaken );
   xyprintf( x+17, y,   "Ev_Time: %02d", Ev_Timer);

  xyprintf( x, y, "Baudrates: "  );
  xyprintf( x+16, y++, "  IR ....: " );
  switch( Clist2.BaudRate )
  {
  case B9600:
      printf( "9600" );
      break;
  case B19200:
      printf( "19200" );
      break;
  case B38400:
      printf( "38400" );
      break;
  case B57600:
      printf( "57600" );
      break;
  case B115200:
      printf( "115200" );
      break;
  case B230400:
      printf( "230400" );
      break;

  }
  xyprintf( x, y, "  OCU .: " );
  switch( RS232Baud )
  {
  case B_9600:
      printf( "9600" );
      break;
  case B_19200:
      printf( "19200" );
      break;
  case B_38400:
      printf( "38400" );
      break;
  case B_57600:
      printf( "57600" );
      break;
  }
  xyprintf( x+16, y++, "  TRiM ..: " );
  switch( Trim_Baud )
  {
  case B_9600:
      printf( "9600" );
      break;
  case B_19200:
      printf( "19200" );
      break;
  case B_57600:
      printf( "57600" );
      break;
  default:
      printf( "9600" );
      break;
  }
  xyprintf( x, y++, "  RS422: " );
  switch( RS422Baud )
  {
  case B_9600:
      printf( " 9600" );
      break;
  case B_19200:
      printf( " 19200" );
      break;
  case B_38400:
      printf( " 38400" );
      break;
  case B_57600:
      printf( " 57600" );
      break;
  case B_115200:
      printf( " 115200" );
      break;
  default:
      printf( "*19200" );
      RS422Baud = B_19200;
      break;
  }
  xyprintf( x, y++, "  SC ..: " );
  switch( SC_Baud )
  {
  case B_4800:
      printf( "4800" );
      break;
  case B_9600:
      printf( "9600" );
      break;
  case B_19200:
      printf( "19200" );
      break;
  case B_38400:
      printf( "38400" );
      break;
  case B_57600:
      printf( "57600" );
      break;
  case B_115200:
      printf( "115200" );
      break;
  }
  switch( SC_READER )
  {
    case OTI_SCI3000:
      printf( " - OTI 3000" );
      break;
    case OTI_SCI6000:
      printf( " - OTI 6000" );
      break;
    case GEM_PROX:
      printf( " - GemProx" );
      break;
    case HID_5553:
      printf( " - HID 5553" );
      break;
    case TRI_READER:
      printf( " - Cubic TR2" );
      break;
    case ASCOM_READER:
    default:
      printf( " - No Reader %d", SC_READER );
      break;
  }
  if( Tst_Sflag( S_SC_OFFLINE ) )
  {
    xyprintf( x+16, y++, "(Off Line)" );
  }
  else
    y++;

/*
** LOWER HALF OF SCREEN
*/
/*  ptitle(11, "CONFIGURATION INFORMATION");
*/
  x = _LEFT;
  y = 12;

   xyprintf( x, y++, "Audio: %s  BurnIn: %3d   SwipeXfers: %s ",
            (VOICE_ENABLED ? "VOICE" : "SOUND"),  BurnInTest,  (SWIPE_XFERS ? "ENB" : "DIS") );

   xyprintf( x, y++, "J1708: %s    MID   : %3d   Events    : %3d",
            (J1708 ?  "ENB" : "DIS"), Clist.PUBLIC.J1708Mid, MaxEv );

   xyprintf( x, y++,                "TRiM Information: "              );
   xyprintf( x, y++, AUTO_TRIMBYP ? "  AutoByp: ENB":"  AutoByp: DIS" );
   printf( MANUAL_TRIM ? "  Feed: MAN " :"  Feed: AUTO"               );

   if ( OF15_SC_PRINT_ON || Tst_S2flag( S2_SC_PRINT_ENB ))
      printf( "  Print SC: ENB" );
   else
      printf( "  Print SC: DIS" );

   xyprintf( x, y++, FB_CONTROL ? "  CapCtrl: FBX, "  :"  CapCtrl: DS, " );
   if ( FB_CONTROL )
       printf( CAPTURE ? "CAPTURE, ":"RETURN, "                       );
   if ( PUT_EXP && PUT_X )
   {
      printf(" no ride " );
   }
   else if ( PUT_X )
   {
      printf( " PUT 'X'  " );
   }
   else if ( PUT_EXP )
   {
      printf( " 'EXPIRED'" );
   }
   else
   {
      printf( " NO PRINT " );
   }
   printf( "   TRiM Header: ");
   if ( crc16((char*)&TrimCnf, sizeof(TRIMCNF)) )
      printf( "CRC");
   else if( !DS_LAYOUT )
      printf( "DIS");
   else
      printf( "ENB");


   memset( buff, 0 , sizeof(buff));
   memcpy( buff, VErr_BusPrnt.VErr_Print, sizeof( VErr_BusPrnt.VErr_Print));
   xyprintf( x, y++, "  VErr Prt: X = %02d, Y = %02d, P/O = %s",
            VErr_BusPrnt.VErr_X_Coord, VErr_BusPrnt.VErr_Y_Coord, buff );
   memset( buff, 0, sizeof(buff));
   memcpy( buff, Layout.options, sizeof(Layout.options) );
   xyprintf( x, y++, "  Issue Count: %03d,  Low Stock Count: %03d", Ticket_Issue_Count, Low_Feed_Count );
   xyprintf( x, y++, "  Low Limit Prnt: %d,  Layout: %s",OF7_LOWLIMIT_PRT,buff );

   xyprintf( x, y++, "SecurityCodes: Mag C-%02d F-%02d, SC C-%02d F-%02d",
      CurrentMagSecurity,FutureMagSecurity,CurrentSCardSecurity,FutureSCardSecurity );
   xyprintf( x, y++, "FSMask ......: %x   Future Fare: %lX", Clist2.FSMask, FutureFare);

   time( &t );
   xyprintf( x, y++, "PrbTime .....: %0lX %lX %lX", Clist2.ProbeWatchDog,
                                               ProbeTime, t );
   xyprintf( x, y++, "Dallas ......: " );
      for ( i = 0; i < 9; i++ )
        printf("%02x", DallasTM[i]);

   xyprintf( x, y++, "ChngThresh...: %5.2f  BillEscTm.: %02d",
                      (float)(ChangeCardThreshold)/100.00, Clist2.BillTimeOut );

   xyprintf( x, y++, "Neg.: %4.2f  max val.: %5.2f  max rd.: %d",
                      (float)(NegativeThreshold)/100.00, (float)(ValueLimit)/100.00, RideLimit );
   xyprintf( x, y++, "MemClr Password:  %lu", Clist2.MemClrPW );
   xyprintf( x, y,   "Private Opflag :  %04x", Clist.PRIVATE.OpFlags );


/*
** Right side of screen
*/
   x = _RIGHT2;
   y = 12;

   xyprintf( x, y++, "OpFlags  1-3: %04x %04x %04x",
                      Clist2.OptionFlags1, Clist2.OptionFlags2, Clist2.OptionFlags3 );
   xyprintf( x, y++, "OpFlags  4-6:   %02x %04x %04x",
                      Clist2.OptionFlags4, Clist2.OptionFlags5, Clist2.OptionFlags6 );
   xyprintf( x, y++, "OpFlags  7-9: %04x %04x %04x",
                      Clist2.OptionFlags7, Clist2.OptionFlags8, Clist2.OptionFlags9 );
   xyprintf( x, y++, "OpFlags10-12: %04x %04x %04x",
                      Clist2.OptionFlags10, Clist2.OptionFlags11, Clist2.OptionFlags12 );
   xyprintf( x, y++, "OpFlags13-15: %04x %04x %04x",
                      Clist2.OptionFlags13, Clist2.OptionFlags14, Clist2.OptionFlags15 );
   xyprintf( x, y++, "OpFlags16-18: %04x %04x %04x",
                      Clist2.OptionFlags16, Clist2.OptionFlags17, Clist2.OptionFlags18 );
   xyprintf( x, y++, "OpFlags19-21: %04x %04x %04x",
                      Clist2.OptionFlags19, Clist2.OptionFlags20, Clist2.OptionFlags21 );
   xyprintf( x, y++, "OpFlags22-24: %04x %04x %04x",
                      Clist2.OptionFlags22, Clist2.OptionFlags23, Clist2.OptionFlags24 );
   xyprintf( x, y++, "OpFlags25-27: %04x %04x %04x",
                      Clist2.OptionFlags25, Clist2.OptionFlags26, Clist2.OptionFlags27 );
   xyprintf( x, y++, "OpFlags28-30: %04x %04x %04x",
                      Clist2.OptionFlags28, Clist2.OptionFlags29, Clist2.OptionFlags30 );
   xyprintf( x, y++, "OpFlags31-32: %04x %04x",
                      Clist2.OptionFlags31, Clist2.OptionFlags32);

   xyprintf( x, y++, "LaMetroOp.: %02x   OSCoinBil.: %02x",
                      Clist2.LA_MetroOption, Clist2.OOS_coin_bill );
   xyprintf( x, y++, "J1708Poll.: %02x   RevStatus : %02x",
                      Clist2.J1708Poll, Clist2.cash_transact_time );
   xyprintf( x, y++, "TranPrtOp.: %02x",
                      Clist2.PrintOption );
   xyprintf( x, y++, "MemFull  .: %02d   Coin Full.: %02d",
                      Clist2.MemNearFull, Clist2.CoinFull );
   xyprintf( x, y++, "Bill Full.: %02d   Logon.....: %02x",
                      Clist2.BillFull, Clist2.logon );
   xyprintf( x, y++, "BillsTaken: %02x   DLS FLG...: %02x",
                      Clist2.BillsTaken, Clist2.daylight );
   xyprintf( x, y++, "Hourly ...: %02x   Passback..: %02x",
                      Clist2.hourlyevent, Clist.PRIVATE.PassBackDuration);
   putchar('\n');
}

void GetCurrentInfo( void )
{
   CurrentInfo();
   printf( "Press any key to return to menu\n" );
   getchar();
}


static
void HelpScreen()
{
/*
    static int    x,y;

    clrscr();

      ptitle(1, "COMMANDS TO/FROM FAREBOX" );

      x = _LEFT;
      y = 2;

      xyprintf( x, y++, "Request TRiM Diags ...a" );
      xyprintf( x, y++, "Configuration Info ...A" );
      xyprintf( x, y++, "Data Base Status .....c" );
      xyprintf( x, y++, "Dallas chip debug ....d" );
      xyprintf( x, y++, "Status Flags .........F" );
      xyprintf( x, y++, "Toggle US/Canada .....f" );
      xyprintf( x, y++, "Help screen ........H/h" );
      xyprintf( x, y++, "Clear RS485 dbug cnts.I" );
      xyprintf( x, y++, "J1708 xmit commands...i" );
      xyprintf( x, y++, "J1708 Init diagnostic.J" );
      xyprintf( x, y++, "J1708 diagnostics.....j" );
    	if ( Tst_Sflag( S_BURNIN ))
         xyprintf( x, y++, "Security Code.........l" );

      x = _CENTER_H;
      y = 2;

    	if ( Tst_Sflag( S_BURNIN ))
        xyprintf( x, y++, "Break bad list.........L" );
      xyprintf( x, y++, "PC Card low mem select.M" );
      xyprintf( x, y++, "FAREBOX SETUP MENUS....m" );
      xyprintf( x, y++, "Bill validtor status...n" );
      xyprintf( x, y++, "Init port for OCU .....O" );
      xyprintf( x, y++, "RS485 debug counters...o" );
      xyprintf( x, y++, "init disp passback que.P" );
      xyprintf( x, y++, "step throu pbque.......p" );
      xyprintf( x, y++, "Init bill diag counts..Q" );
      xyprintf( x, y++, "disp bill diag counts..q" );

      x = _RIGHT_H;
      y = 2;

      xyprintf( x, y++, "Initialize BL Display .S" );
      xyprintf( x, y++, "Step through BAD LIST .s" );
      xyprintf( x, y++, "Play Sounds............T" );
      xyprintf( x, y++, "Auto TRiM bypas select.t" );
      xyprintf( x, y++, "Init step throu sound..U" );
      xyprintf( x, y++, "play next sound........u" );
      xyprintf( x, y++, "Bump Bad list display..V" );
      xyprintf( x, y++, "Select Sound/Voice ....v" );
      xyprintf( x, y++, "on/off trim wdog led...x" );
    	if ( Tst_Sflag( S_BURNIN ))
         xyprintf( x, y++, "break bad list.........+" );

	  if ( Tst_Sflag( S_SPARES1 ))
	  {
		  ptitle(15, "( Additional commands with jumper 1 )" );

		  x = _LEFT;
		  y = 16;


		  x = _CENTER_H;
		  y = 16;


		  x = _RIGHT_H;
		  y = 16;

	  }
	  else if ( Tst_Sflag( S_SPARES2 ))
	  {
		  ptitle(15, "( Additional commands with jumper 2 )" );

		  x = _LEFT;
		  y = 16;

		  xyprintf( x, y++, "Bill Validator on/off.b" );
		  xyprintf( x, y++, "Read new DALLAS ......C" );
		  xyprintf( x, y++, "Coin Validator on/off.c" );

		  x = _CENTER_H;
		  y = 16;

		  xyprintf( x, y++, "Automatic log-off .....K" );
		  xyprintf( x, y++, "OCU Comm errors .......O" );

		  x = _RIGHT_H;
		  y = 16;

		  xyprintf( x, y++, "Add 20 events .........w" );
		  xyprintf( x, y++, "Clear Probe errors ....Z" );
		  xyprintf( x, y++, "Probing Comm Errors ...z" );
	  }
	  else if ( Tst_Sflag( S_SPARES3 ))
	  {
		  ptitle(15, "( Additional commands with jumper 3 )" );

		  x = _LEFT;
		  y = 16;

		  xyprintf( x, y++, "Bill Debug On/Off......1" );
		  xyprintf( x, y++, "CardRead Debug On/Off..2" );
		  xyprintf( x, y++, "Coin Debug On/Off......3" );
		  xyprintf( x, y++, "OCU Debug On/Err/Off...4" );
		  xyprintf( x, y++, "SSC Debug On/Off.......5" );

		  x = _CENTER_H;
		  y = 16;

		  xyprintf( x, y++, "TRiM Debug On/Off......6" );
		  xyprintf( x, y++, "Transact Debug On/Off..8" );
		  xyprintf( x, y++, "BurnIn Debug On/Off....9" );
		  xyprintf( x, y++, "Tlog On/Off............0" );

		  x = _RIGHT_H;
		  y = 16;

		  xyprintf( x, y++, "Enable sound debug.....T" );

	  }
      ptitle(22, " ");
*/
}

static
void UTime()
{
  time_t  t;

  time( &t );
  printf("Time (dec) %ld\n", t );
  printf("Time (hex) %lx\n", t );
  printf("unix time hex %lx dec %lu\n", t/DaySec, t/DaySec );
}


static
void  GetTrimCommands()
{
  long   i;

  clrscr();
  printf("Current Process Flags: %08lx\r\n", TrimCflags);
  i = GetProcessCmds(5);
  if ( i >= 0 && i < 65536  )
  {
    TrimCflags = 0L;
    TrimCflags = (word)i;
  }
}


static
void  GetTrimDebug()
{
  long  i;

  clrscr();
  printf("Current Debug Setting: %02x\r\n", TrimDebugLevel);
  i = GetProcessCmds(5);
  if( i >= 0 && i < 65536 )
  {
    TrimDebugLevel = 0L;
    TrimDebugLevel = (word)i;
  }
}

static
word GetProcessCmds( int size )
{
   char buf[10];
   int  n;

   memset( buf, 0, sizeof( buf ));
   putstr("    New: ");
   if( (n=Ngetx(buf, size)) )
      return (long)_h2l( buf, n );
   else
      return;
}

void convert_unix_time( int x)
{
  char         buf[10];
  int          n;
  time_t       t;
  struct  tm   *tm;

   putstr("\nUnix Date: ");
   memset( buf, 0, sizeof( buf ));
   if( (n=Ngetx(buf, 8)) )
   {
      t = (long)_h2l( buf, n);
      if ( x )
        t = t * DaySec;

      tm = gmtime( &t );
      printf("\nDate: %02d/%02d/%02d Time: %02d:%02d:%02d\n",
         tm->tm_mon+1, tm->tm_mday, (tm->tm_year%100), tm->tm_hour, tm->tm_min, tm->tm_sec);
      printf(" day %lu\n", t/DaySec );
   }
}

void  Display_J1708_Diag( void )
{
   int  n;

   if ( Tst_Sflag( S_SPARES1 ))
   {
     memset( (char*)&J1708Diag, 0, sizeof(J1708DIAG) );
     J1708Response = 0;
   }
   J1708Diag.J1708Poll   = Clist2.J1708Poll;
   J1708Diag.J1708Pid502 = Clist2.logon;
   J1708Diag.J1708Config = Clist2.OptionFlags3;
   J1708Diag.J1708lat    = FbxCnf.latitude;
   J1708Diag.J1708long   = FbxCnf.longitude;
   J1708Diag.J1708stop   = FbxCnf.stop;
   printf("\nJ1708 Debug data:\n");
   printf("J1708 message sent       %lu\n", J1708Diag.J1708MessSnt );
   printf("J1708 total message rec  %lu\n", J1708Diag.J1708MessCnt );
   printf("J1708 fb message rec     %lu\n", J1708Diag.J1708MessRec );
   printf("j1708 idle line detect   %lu\n", J1708Diag.J1708LrcTO );
   printf("j1708 char timeout       %u\n",  J1708Diag.J1708ErrCnt );
   printf("j1708 lrc  error         %u\n",  J1708Diag.J1708LrcErr );
   printf("j1708 collision          %u\n",  J1708Diag.J1708Collision );
   printf("j1708 response timeout   %u\n",  J1708Diag.J1708RespErr );
   printf("j1708 message errors     %u\n",  J1708Diag.J1708MessErr );
   printf("Lat long messages rec    %u\n",  J1708Diag.J1708Lat_Long );
   printf("Stop messages rec        %u\n",  J1708Diag.J1708Stop );
   printf("Time/date messages rec   %u\n",  J1708Diag.J1708Time );
   printf("Time/date errors         %u\n",  J1708Diag.J1708TimeErr );
   printf("J1708 Idle Error         %u\n",  J1708Diag.J1708IdleErr );
   printf("Message Pend Error       %u\n",  J1708Diag.IdleMessPend );
   printf("Message needing response %4x\n", J1708Response );
   printf("Latitude/Longitude       %lX / %lX\n", J1708Diag.J1708lat, J1708Diag.J1708long );
   printf("Current Stop             %ld\n",  J1708Diag.J1708stop );
   printf("TxQ pending              %u\n",   J1708TxQCnt() );
   printf("J1708 poll config        %02x\n", J1708Diag.J1708Poll );
   printf("J1708 Pid 502 config     %02x\n", J1708Diag.J1708Pid502 );
   printf("J1708 Configuration      %04x\n", J1708Diag.J1708Config );
   if ( IRMAP_Check )
   {
      n = sizeof(PATRON_CNTR);
      printf("J1708 Patron size        %d\n", n );
   }
}

void Display_Trim_Diag( void )
{
   word crc;

   printf( "\nREQ TRIM DIAG\n" );
   TrimCmd( TRIM_DIAG_REC, NULL, 0 );
   TrimDiag( NULL, 0 );
   crc =  ((word)Trim_EDiag.spare[0]<<8) |Trim_EDiag.spare[1];
   printf("Differential\n");
   printf( "dev no. %ld ver        %d\n", Trim_Diag.devno, Trim_Diag.version );
   printf( "reads  %d  misreads   %d\n", Trim_Diag.read, Trim_Diag.misread );
   printf( "writes %d  badverify  %d\n", Trim_Diag.write, Trim_Diag.badverify );
   printf( "print  %d  issued     %d\n", Trim_Diag.print, Trim_Diag.issue );
   printf( "jam    %d               \n", Trim_Diag.jam );
   printf( "cold   %d   warm      %d\n", Trim_Diag.coldstart, Trim_Diag.warmstart );
   printf("Cumulative\n");
   printf( "reads  %ld  misreads  %ld\n", Trim_Diag.trim_reads, Trim_Diag.trim_misread );
   printf( "writes %ld  badverify %ld\n", Trim_Diag.trim_writes, Trim_Diag.trim_badVerif );
   printf( "print  %ld  issued    %ld\n", Trim_Diag.trim_print, Trim_Diag.trim_issue );
   printf( "jam    %ld  cycle     %ld\n", Trim_Diag.trim_jam, Trim_Diag.trim_cycle );

   if( Trim_Diag.version >= 181 )
   {
     printf( "pwr dn %d   resets    %d\n", Trim_EDiag.powerdown, Trim_EDiag.resets );
     printf( "SC_offline  %u\n",  crc );
     printf( "HOST                   %s\n", HOST_TRIM ? "YES":" NO");
   }
}

void Display_Auto_Que( int i )
{
   printf( "Auto_Oue.Cnt                 %d of %d\n",  (i+1), Auto_Que.cnt );
   printf( "Auto_Que.q.pkg_load_seq_id   %lu\n", Auto_Que.q[i].pkg_load_seq );
   printf( "Auto_Que.q.card_load_seq_id  %lu\n", Auto_Que.q[i].card_load_seq );
   printf( "Auto_Que.q.tpbc              %u\n",  Auto_Que.q[i].tpbc );
   printf( "Auto_Que.q.fare_id           %u\n",  Auto_Que.q[i].fare_id );
   printf( "Auto_Que.q.al_value          %u\n",  Auto_Que.q[i].al_value );
   printf( "Auto_Que.q.value             %u\n",  Auto_Que.q[i].value );
   printf( "Auto_Que.q.pkg_single_seq    %u\n",  Auto_Que.q[i].pkg_single_seq );
   printf( "Auto_Que.q.card_single_seq   %u\n",  Auto_Que.q[i].card_single_seq );
   printf( "Auto_Que.q.load_type         %u\n",  Auto_Que.q[i].load_type );
   printf( "Auto_Que.q.prod_type         %u\n",  Auto_Que.q[i].prod_type );
   printf( "Auto_Que.q.desig             %u\n",  Auto_Que.q[i].desig );
   printf( "Auto_Que.q.pkg_prod_id       %u\n",  Auto_Que.q[i].pkg_prod_id );
   printf( "Auto_Que.q.card_prod_id      %u\n",  Auto_Que.q[i].card_prod_id );
   printf( "Auto_Que.q.list_type         %u\n",  Auto_Que.q[i].list_type );
   printf( "Auto_Que.q.pend_period       %u\n",  Auto_Que.q[i].pend_period );
   printf( "Auto_Que.q.code              %u\n",  Auto_Que.q[i].code );
   printf( "Auto_Que.q.start_date        %u\n",  Auto_Que.q[i].start_date );
   printf( "Auto_Que.q.end_date          %u\n",  Auto_Que.q[i].end_date );
   printf( "Auto_Que.q.add_time          %u\n",  Auto_Que.q[i].add_time );
   printf( "Auto_Que.q.epay              %u\n",  Auto_Que.q[i].epay );
   printf( "Auto_Que.q.details           %u\n",  Auto_Que.q[i].details );
   printf( "Auto_Que.q.exp_type          %u\n\n",  Auto_Que.q[i].exp_type );
}

static
void  DataStatus( void )
{
   word       crc;
   ulong      big_crc;
   time_t     t;

   struct tm  *tm;


/*         printf( "ktfares   %u\n ", Mlist.System.Cnts[0][0]); */
   printf( "TS flags: %08lX      S  flags: %08lX      S2 flags: %08lX\n", TrimSflags,  Sflags, S2flags );
   printf( "P  flags: %08lX      A  flags: %08lX      A2 flags: %08lX\n", Pflags, Aflags, A2flags );
   printf( "232 baud: %02x            led time: %u\n", RS232Baud, Clist2.LEDTime  );
   printf(" Ev_Timer %d currenrt timer %d\n",
     Ev_Timer,Ev_Timer_Counter );
   if ( BadClock )
     printf("possible bad clock %x\n", BadClock );
   if ( BadBatteryDetect )
     printf("possible bad battery %x\n", BadBatteryDetect );

   crc = crc16( (char*)&FriendlyAgy, sizeof(FriendlyAgy)-2 );
   swab( (char*)&crc, (char*)&crc, 2);
   printf("FRIENDLY cal crc %X real crc %X %s\n",
         crc, FriendlyAgy.crc, Tst_S2flag( S2_FRIENDLY_OK ) ? "enabled":"disabled" );
   crc = crc16( (char*)&Dlist, sizeof(DownLoadList)-2 );
   swab( (char*)&crc, (char*)&crc, 2);
   if ( Tst_Sflag( S_BURNIN ) && !Tst_Aflag( A_V2_FTBLE ) && Prb_Req_Parms.ver > 7260 )
   {
      Set_Aflag( A_V2_FTBLE );
   }

   if ( Tst_Aflag( A_V2_FTBLE ))
   {
      printf( "NEW Dlist ver %d crc %X cal %X\n",
          Dlist.ParameterTableVersion, Dlist.CRC16, crc );
      tm = gmtime( &FutureFare );

      printf("Future time %02d:%02d:%02d  %02d/%02d/%02d\n",
              tm->tm_hour, tm->tm_min, tm->tm_sec,
              tm->tm_mon+1, tm->tm_mday, tm->tm_year%100 );

      crc = crc16( (char*)&FDlist, sizeof(DownLoadList)-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf( "Future FDlist ver %d crc %X cal %X\n",
        FDlist.ParameterTableVersion, FDlist.CRC16, crc );
   }
   else
     printf( "OLD Dlist ver %d crc %X cal %X\n",
        Dlist.ParameterTableVersion, Dlist.CRC16, crc );

   WatchDog( _WDT_SERVICE );
   if ( Tst_Pflag( P_BIG_BADLIST ) )
   {
      crc = crc16( (char*)&BBlist, sizeof(BBList)-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf( "Real BBList ver %d crc %04x cal %04x\n",
         BBlist.BBL_ver, BBlist.BBL_crc, crc );
      WatchDog( _WDT_SERVICE );
   }
   else
   {
      crc = crc16( (char*)&BBlist, (BBL_SSIZE*10)+2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf( "BBList ver %d crc %04x cal %04x\n",
         BBlist.BBL_ver, BBlist.BBL_crc, crc );
   }
   WatchDog( _WDT_SERVICE );

   if ( Tst_Pflag( P_CLIST2 ) )
   {
      crc = crc16( (char*)&Clist2, sizeof(GFIconfig2)-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf( "Big Clist2 ver %d crc %04x cal %04x\n",
         Clist2.ver, Clist2.crc, crc );
   }
   else
   {
      t = Clist2.ProbeWatchDog;
      memcpy( (char*)&Clist2.ProbeWatchDog, (char*)&Clist2.ver, 4 );
      crc = crc16( (char*)&Clist2, OLD_CLIST2-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf( "Small Clist2 ver %d crc %04x cal %04x\n",
         Clist2.ver, Clist2.crc, crc );
      Clist2.ProbeWatchDog = t;
   }
   crc = crc16( (char*)&DriverList, sizeof(DriverList)-2 );
   swab( (char*)&crc, (char*)&crc, 2);
   if ( crc != DriverList.crc && Tst_Sflag( S_SPARES2 ))    /* fix if Driver crc broken */
   {
     printf("fixed DriverList.crc\n");
     DriverList.crc = crc;
   }

   if ( DriverChkEnb() )
   {
     if ( crc == DriverList.crc )
     {
       printf( "Driver check list enabled and is good %04x\n",
          DriverList.crc );
     }
     else
       printf( "Driver check enabled but crc bad crc %04x cal %04x\n",
         DriverList.crc, crc );
   }
   else
   {
     if ( crc == DriverList.crc )
       printf( "Driver check list disabled\n");
     else
       printf( "Driver check list disabled and crc bad\n");
   }

   if ( !OF6_ROUTE_MATRIX )
   {
     crc = crc16( (char*)&RouteList, sizeof(RouteList)-2 );
     swab( (char*)&crc, (char*)&crc, 2);
     if ( crc != RouteList.crc && Tst_Sflag( S_SPARES2 ))    /* fix if Driver crc broken */
     {
       printf("fixed RouteList.crc\n");
       RouteList.crc = crc;
     }

     if ( RouteChkEnb() )
     {
       if ( crc == RouteList.crc )
       {
         printf( "Route check list enabled and is good %04x\n",
            RouteList.crc );
       }
       else
         printf( "Route check enabled but crc bad crc %04x cal %04x\n",
           RouteList.crc, crc );
     }
     else
     {
       if ( crc == RouteList.crc )
         printf( "Route check list disabled\n");
       else
         printf( "Route check list disabled and crc bad\n");
     }
   }
   else
   {
     if ( RouteLength > sizeof(RouteList)-2 )
     {
       if ( RouteLength == sizeof(RouteList) )
       {
         crc = crc16( (char*)&RouteList, sizeof(RouteList)-2 );
         swab( (char*)&crc, (char*)&crc, 2);
         if ( crc == RouteList.crc )
         {
           printf( "Matrix Route check list enabled but with old route table and is good %04x\n",
              RouteList.crc );
         }
         else
         {
           printf( "Matrix Route check enabled but crc bad crc %04x cal %04x\n",
             RouteList.crc, crc );
         }
       }
       else
       {
         printf( "Matrix Route check enabled but too big\n");
       }
     }
     else
     {
       crc = crc16( (char*)&RouteList, RouteLength-2 );
       swab( (char*)&crc, (char*)&crc, 2);

       if ( RouteChkEnb() )
       {
         printf("RouteLength %u\n", RouteLength );
         if ( crc == RouteList.crc )
         {
           printf( "Matrix Route check list enabled and is good %04x\n",
              RouteList.crc );
         }
         else
           printf( "Matrix Route check enabled but crc bad crc %04x cal %04x\n",
             RouteList.crc, crc );
       }
       else
       {
         if ( crc == RouteList.crc )
           printf( "Matrix RouteDriver check list disabled\n");
         else
           printf( "Matrix Route check list disabled and crc bad\n");
       }
     }
   }

   crc = crc16( (char*)&RunList, sizeof(RunList)-2 );
   swab( (char*)&crc, (char*)&crc, 2);

   if ( RunChkEnb() )
   {
     if ( crc == RunList.crc )
     {
       printf( "Run check list enabled and is good %04x\n",
          RunList.crc );
     }
     else
       printf( "Run check enabled but crc bad crc %04x cal %04x\n",
         RunList.crc, crc );
   }
   else
   {
     if ( crc == RunList.crc )
       printf( "Run check list disabled\n");
     else
       printf( "Run check list disabled and crc bad\n");
   }

   crc = crc16( (char*)&TripList, sizeof(TripList)-2 );
   swab( (char*)&crc, (char*)&crc, 2);

   if ( TripChkEnb() )
   {
     if ( crc == TripList.crc )
     {
       printf( "Trip check list enabled and is good %04x\n",
          TripList.crc );
     }
     else
       printf( "Trip check enabled but crc bad crc %04x cal %04x\n",
         TripList.crc, crc );
   }
   else
   {
     if ( crc == TripList.crc )
       printf( "Trip check list disabled\n");
     else
       printf( "Trip check list disabled and crc bad\n");
   }

   crc = crc16( (char*)&RangeList, sizeof(RangeList)-2 );
   swab( (char*)&crc, (char*)&crc, 2);

   if ( RangeChkEnb() )
   {
     if ( crc == RangeList.crc )
     {
       printf( "Range check list enabled and is good %04x\n",
          RangeList.crc );
     }
     else
       printf( "Range check enabled but crc bad crc %04x cal %04x\n",
         RangeList.crc, crc );
   }
   else
   {
     if ( crc == RangeList.crc )
       printf( "Range check list disabled\n");
     else
       printf( "Range check list disabled and crc bad\n");
   }

   crc = crc16( (char*)&SpecCard, sizeof(SpecCard)-2 );
   swab( (char*)&crc, (char*)&crc, 2);
   if ( crc != SpecCard.crc && Tst_Sflag( S_SPARES2 ))    /* fix if Driver crc broken */
   {
     printf("fixed SpeCard_crc\n");
     SpecCard.crc = crc;
   }


   crc = crc16( (char*)&SpecCard, sizeof(SpecCard)-2 );
   swab( (char*)&crc, (char*)&crc, 2);
   if ( crc != SpecCard.crc && Tst_Sflag( S_SPARES2 ))    /* fix if Driver crc broken */
   {
     printf("fixed SpeCard_crc\n");
     SpecCard.crc = crc;
   }

   if ( SpecCardTbl() )
   {
     if ( crc == SpecCard.crc )
     {
       printf( "SpecCard check list enabled and is good %04x\n",
          SpecCard.crc );
     }
     else
       printf( "SpecCard enabled but crc bad crc %04x cal %04x\n",
         SpecCard.crc, crc );
   }
   else
   {
     if ( crc == SpecCard.crc )
       printf( "SpecCard check list disabled but crc OK\n");
     else
       printf( "SpecCard check list disabled and crc bad crc %04x cal %04x\n",
         SpecCard.crc, crc );
   }

   if( ZoneLength < 4 || ZoneLength > ZONE_SIZE )
   {
      printf( "ZoneTable size bad\n" );
   }
   else
   {
      crc = crc16( &ZoneTable, ZoneLength-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      printf("ZoneTable crc %4x cal %04x size %u\n",
        (ZoneTable[ZoneLength-2]<<8|ZoneTable[ZoneLength-1]), crc, ZoneLength );
   }

   if ( !FutureZone )
   {
      printf("NO Future Zone \n");
   }
   else
   {
      tm = gmtime( &FutureZone );

      printf("Future Zone time %02d:%02d:%02d  %02d/%02d/%02d\n ",
              tm->tm_hour, tm->tm_min, tm->tm_sec,
              tm->tm_mon+1, tm->tm_mday, tm->tm_year%100 );

     if( FZoneLength < 4 || FZoneLength > ZONE_SIZE )
     {
        printf( "Future ZoneTable size bad\n" );
     }
     else
     {
        crc = crc16( &FZoneTable, FZoneLength-2 );
        swab( (char*)&crc, (char*)&crc, 2);
        printf(" Future ZoneTable crc %4x cal %04x size %u\n",
          (FZoneTable[FZoneLength-2]<<8|FZoneTable[FZoneLength-1]), crc, FZoneLength );
     }
   }


   if (  Clist.PRIVATE.PartNo == D27207 )
   {
      WatchDog( _WDT_SERVICE );
      if ( AutoLoadSize >= sizeof( AUTO_LST_HDR ) && AutoLoadSize < AUTOLOAD_SZ )
      {
         big_crc = crc32( (char*)&AutoListHdr.cat, (AutoLoadSize-4) );
         if ( big_crc == AutoListHdr.crc32 )
         {
            memcpy( (char*)&AutoLoadList[AutoLoadSize], (char*)&AutoListHdr.crc32, 4 );
            AutoLoadFlag = TRUE;
         }
      }
      else
      {
         big_crc = 0;
      }
      printf("AutoLoadTable crc %lX cal %lX size %lu Flag %c\n",
              AutoListHdr.crc32, big_crc, AutoLoadSize, AutoLoadFlag ? 'G' : 'B' );
   }
   else
   {
      big_crc = 0;
   }


   if ( DLS_TIME )
   {
     printf("dls times %x/%x (%d) std time %x/%x (%d)\n",
        Clist2.DST[0], Clist2.DST[1], DayLight_Day,  Clist2.STD[0], Clist2.STD[1], STD_Day );
   }
   else
   {
     printf("dls times disabled\n");
   }
   printf( "nodes %d node address %d\n", FbxCnf.nodes, FbxCnf.node_addr );
   printf("warm %lu  cold %lu  WI Port Reset %u\n", Ml.warm_c, Ml.cold_c, FbxCnf.WIPortReset );
}

int Debug_Menu( void )
{
   return tst_status( EDIT_MESSAGE );
}


static void  ATlog_Debug_Settings( void )
{
   switch (ATlog_Debug)
   {
      case 0:
         ATlog_Debug = GEN_DBG;                                                     /* 0x0001 */
         printf("ATlog Debug ( 0x%08lX ) - General Debug\n", ATlog_Debug);
         break;
      case GEN_DBG:
         ATlog_Debug = BILL_DBG;                                                    /* 0x0002 */
         printf("ATlog Debug ( 0x%08lX ) - Bill Validator Debug\n", ATlog_Debug);
         break;
      case BILL_DBG:
         ATlog_Debug = SWIPE_DBG;                                                   /* 0x0004 */
         printf("ATlog Debug ( 0x%08lX ) - Swipe Reader Debug\n", ATlog_Debug);
         break;
      case SWIPE_DBG:
         ATlog_Debug = COIN_DBG;                                                    /* 0x0008 */
         printf("ATlog Debug ( 0x%08lX ) - Coin Validator Debug\n", ATlog_Debug);
         break;
      case COIN_DBG:
         ATlog_Debug = OCU_DBG;                                                     /* 0x0010 */
         printf("ATlog Debug ( 0x%08lX ) - OCU Debug\n", ATlog_Debug);
         break;
      case OCU_DBG:
         ATlog_Debug = TRIM_DBG;                                                    /* 0x0020 */
         printf("ATlog Debug ( 0x%08lX ) - TRIM Debug\n", ATlog_Debug);
         break;
      case TRIM_DBG:
         ATlog_Debug = PRINT_DBG;                                                   /* 0x0040 */
         printf("ATlog Debug ( 0x%08lX ) - Printer Debug\n", ATlog_Debug);
         break;
      case PRINT_DBG:
         ATlog_Debug = BCR_DBG;                                                     /* 0x0080 */
         printf("ATlog Debug ( 0x%08lX ) - Barcode Reader Debug\n", ATlog_Debug);
         break;
      case BCR_DBG:
         ATlog_Debug = MBTK_DBG;                                                    /* 0x0100 */
         printf("ATlog Debug ( 0x%08lX ) - Mobile Ticketing Debug\n", ATlog_Debug);
         break;
      case MBTK_DBG:
         ATlog_Debug = SCARD_DBG;                                                   /* 0x0200 */
         printf("ATlog Debug ( 0x%08lX ) - Smart Card Reader Debug (GENERAL)\n", ATlog_Debug);
         break;
      case SCARD_DBG:
         ATlog_Debug = SCDATA_DBG;                                                  /* 0x0400 */
         printf("ATlog Debug ( 0x%08lX ) - Smart Card Reader Debug (DATA)\n", ATlog_Debug);
         break;
      case SCDATA_DBG:
         ATlog_Debug = SC_FULL_DBG;                                                 /* 0x0600 */
         printf("ATlog Debug ( 0x%08lX ) - Smart Card Reader Debug (GENERAL + DATA)\n", ATlog_Debug);
         break;
      case SC_FULL_DBG:
         ATlog_Debug = DESFIRE_DBG;                                                 /* 0x0800 */
         printf("ATlog Debug ( 0x%08lX ) - Smart Card Reader Debug  (DESFire)\n", ATlog_Debug);
         break;
      case DESFIRE_DBG:
         ATlog_Debug = PVAL_DBG;                                                    /* 0x1000 */
         printf("ATlog Debug ( 0x%08lX ) - Platform Validator Debug\n", ATlog_Debug);
         break;
      case PVAL_DBG:
         ATlog_Debug = M_TVM_DBG;                                                   /* 0x2000 */
         printf("ATlog Debug ( 0x%08lX ) - 'Mini TVM' Debug\n", ATlog_Debug);
         break;
      case M_TVM_DBG:
         ATlog_Debug = IR_DBG;                                                      /* 0x4000 */
         printf("ATlog Debug ( 0x%08lX ) - IR Probe Debug\n", ATlog_Debug);
         break;
      case IR_DBG:
         ATlog_Debug = WIFI_DBG;                                                    /* 0x8000 */
         printf("ATlog Debug ( 0x%08lX ) - Wi-Fi Debug\n", ATlog_Debug);
         break;
      case WIFI_DBG:
         ATlog_Debug = RELAY_DBG;                                                   /* 0x10000 */
         printf("ATlog Debug ( 0x%08lX ) - Door/Gate Debug\n", ATlog_Debug);
         break;
      case RELAY_DBG:
      default:
         ATlog_Debug = 0;
         printf("ATlog Debug = OFF\n");
         break;
   }
}

/*------------------------------------------------\
|  Function:   ProgramPCcard();                   |
|  Purpose :   Program pc card with flash program |
|  Synopsis:   void  ProgramPCcard( void );       |
|  Input   :   None.                              |
|  Output  :   None.                              |
|  Comments:   Called via Process_Dbg or Menu     |
\------------------------------------------------*/
void ProgramPCcard( int action )
{
   long        i,m;
   int         j,k,ConfigCard_Size;
   ulong       acc;
   ulong       *src, *dst;
   int         *s, *d;

   ClrLed(A21);                       /* select lower pc card memory */

   switch( action )
   {
      case CODE:
         src        =  (ulong*)0x0008000;   /* point to code in flash  */
         PCcard.id  =  PCMCIA_PGMID;        /* program image ID        */
         PCcard.len =  0x00f0000;           /* length -- max. possible */
         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
            swsr.b.swsr = 0x55;
            swsr.b.swsr = 0xaa;
         }
         PCcard.cs    =  acc;
         puts( "program image loaded into PCMCIA" );
         break;

      case FARE_TABLE:
         memset( (char*)0x00800000, 0, 0x8000 );
         src          =  (ulong*)&Test;      /* point to Test() */
         PCcard.id    =  PCMCIA_EXEID;       /* executable code ID */
         PCcard.len   =  ((char*)&PlaceHolder - (char*)&Test);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;
         puts( "executable loaded into PCMCIA" );
         printf( "size = %ld bytes\n", PCcard.len );

         for( i=0, src=(ulong*)&Dlist, dst=(ulong*)0x00804000;
              i<sizeof(DownLoadList)/4L;
              i++ )
        {
            dst[i] = src[i];
         }
         puts( "farestructure loaded into PCMCIA" );
         printf( "size = %ld bytes\n", sizeof(DownLoadList) );
         break;

      case FRIENDLY_LIST:
         memset( (char*)0x00800000, 0, 0x8000 );
         src          =  (ulong*)&Test2;      /* point to Test2() */
         PCcard.id    =  PCMCIA_EXEID;       /* executable code ID */
         PCcard.len   =  ((char*)&Place_Holder - (char*)&Test2);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;

         for( j=0, acc = 0, s=(int*)&FriendlyAgy, d=(int*)0x00804000;
              j<sizeof(FRIENDLYAGY)/2; j++ )
         {
            d[j] = s[j];
            acc += s[j];
         }
         d[j] = (int)acc;
         printf("saved cksum %d cal cksum %d\n", d[j], (int)acc );

         for( j=0, k=0, s=(int*)0x00804000;
              k<sizeof(FRIENDLYAGY); k++ )
         {
            j += s[k];
         }
         printf("cksum %d save cs %d size  %d\n", j, s[k], k );
         puts( "friendly agency loaded into PCMCIA" );
         break;

      case BOOT_BLOCK:
         memset( (char*)0x00800000, 0, 0x8000 );
         src          =  (ulong*)&Boot_Code; /* point to start  */
         PCcard.id    =  PCMCIA_EXEID;       /* executable code ID */
         PCcard.len   =  ((char*)&Boot_CodeEnd - (char*)&Boot_Code);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;

         for( i=0, acc=0, src=(ulong*)0, dst=(ulong*)0x00802000; i<0xfff; i++ )
         {
            dst[i] = src[i];
            acc      += src[i];   /* calculate checksum */
         }
         dst[i] = acc;            /* save checksum at end of boot block */
         puts( "executable of boot block update loaded into PCMCIA\n" );
         break;

      case CONFIGURATION:
         memset( (char*)0x00800000, 0, 0x80000 );
         src          =  (ulong*)&Test3;
         PCcard.id    =  PCMCIA_EXEID;
         PCcard.len   =  ((char*)&Place_Holder3 - (char*)&Test3);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;
/*
         puts( "executable loaded into PCMCIA" );
         printf( "size = %ld bytes\n\n", PCcard.len );

         printf( "Farestructure address %08lX\n", &Dlist);
*/
         for( j=0, acc = 0, s=(int*)&Dlist, d=(int*)0x00804000;
              j<sizeof(DownLoadList)/2; j++ )
         {
            d[j] = s[j];
            acc += s[j];
         }
         ConfigCard_Size = sizeof(DownLoadList)/2;
/*
         puts( "farestructure loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", j, ConfigCard_Size );

         printf( "GFIconfig address %08lX\n", &Clist);
*/
         for( k=0, s=(int*)&Clist; k<sizeof(GFIconfig)/2; k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += sizeof(GFIconfig)/2;
/*
         puts( "GFIconfig loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "GFIconfig2 address %08lX\n", &Clist2);
*/
         for( k=0, s=(int*)&Clist2; k<sizeof(GFIconfig2)/2; k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += sizeof(GFIconfig2)/2;
/*
         puts( "GFIconfig2 loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "Friendly address %08lX\n", &FriendlyAgy);
*/
         for( k=0, s=(int*)&FriendlyAgy; k<sizeof(FRIENDLYAGY)/2; k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }

         ConfigCard_Size += sizeof(FRIENDLYAGY)/2;
/*
         puts( "Friendly Agency List loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "TrimCnf address %08lX\n", &TrimCnf);
*/
         for( k=0, s=(int*)&TrimCnf; k<sizeof(TRIMCNF)/2; k++, j++ )
         {
             d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += sizeof(TRIMCNF)/2;
/*
         puts( "TRiM Header information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "Prb_Req_Parms address %08lX\n", &Prb_Req_Parms);
*/
         for( k=0, s=(int*)&Prb_Req_Parms; k<sizeof(PRB_REQ_PARMS)/2; k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += sizeof(PRB_REQ_PARMS)/2;
/*
         puts( "Probe Parameter information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

/*.add Special List tables (Special Cards,Drivers,Routes) to configuration */
/*
         printf( "SpecCard address %p\n", &SpecCard);
*/
         for( k=0, s=(int*)&SpecCard; k<(sizeof(SpecCard)/2); k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += sizeof(SpecCard)/2;
/*
         puts( "Special Card Table information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "Driver List %p\n", &DriverList);
*/
         for( k=0, s=(int*)&DriverList; k<(sizeof(DriverList)/2); k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += (sizeof(DriverList)/2);
/*
         puts( "\Driver List information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );

         printf( "Route List %p\n", &RouteList);
*/
         for( k=0, s=(int*)&RouteList; k<(sizeof(RouteList)/2); k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += (sizeof(RouteList)/2);
/*
         puts( "Route List information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );
*/
/*.add Special List tables (Special Cards,Drivers,Routes) to configuration */

/*
      printf( "Key List %p\n", &KeyList);
*/
         for( k=0, s=(int*)&KeyList; k<(sizeof(KeyList)/2); k++, j++ )
         {
            d[j] = s[k];
            acc += s[k];
         }
         ConfigCard_Size += (sizeof(KeyList)/2);
/*
         puts( "Key List information loaded into PCMCIA" );
         printf("Module Size %d, Total size %d\n\n", k, ConfigCard_Size );
*/
/*.add Key List tables (Key, Special Cards,Drivers,Routes) to configuration */

         d[j] = (int)acc;
/*
         printf("saved cksum %X  cal cksum %X  size %d\n\n", d[j], (int)acc,
                ConfigCard_Size );

         for( j=0, k=0, s=(int*)0x00804000;
              k<ConfigCard_Size; k++ )
         {
            j += s[k];
         }

         printf("vcksum %X vsave cksum %X vsize  %d\n", j, s[k], k );
         puts( "configuration card loaded" );
*/
         break;

      case BAD_LIST_TABLE:
         memset( (char*)0x00800000, 0, 0x20000 );
         src          =  (ulong*)&BadList;   /* point to BadList() */
         PCcard.id    =  PCMCIA_EXEID;       /* executable code ID */
         PCcard.len   =  ((char*)&BL_PlaceHolder - (char*)&BadList);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;
         puts( "executable loaded into PCMCIA" );
         printf( "size = %ld bytes\n", PCcard.len );
         m = (sizeof(BBList) + sizeof(BBIndex) + 2)/2;
         for( i=0, acc = 0, s=(int*)&BBlist, d=(int*)0x00804000;
              i<m; i++ )
         {
            d[i] = s[i];
            acc += s[i];
         }
         d[i] = (int)acc;

         puts( "Bad List loaded into PCMCIA" );
         printf( "size = %ld bytes chksum %x\n", (m*2), d[i] );
         break;

      case BAD_LIST_FS_TABLE:
         memset( (char*)0x00800000, 0, 0x20000 );
         src          =  (ulong*)&BadList_FS; /* point to BadList() */
         PCcard.id    =  PCMCIA_EXEID;        /* executable code ID */
         PCcard.len   =  ((char*)&BL_FS_PHolder - (char*)&BadList_FS);

         for( i=0, acc=0; i<PCcard.len/4L; i++ )
         {
            PCcard.fn[i] =  src[i];
            acc      += src[i];
         }
         PCcard.cs    =  acc;
         puts( "executable loaded into PCMCIA" );
         printf( "size = %ld bytes\n", PCcard.len );

         for( i=0, m = 0, acc = 0, s=(int*)&BBlist, d=(int*)0x00804000;
              i<sizeof(BBList)/2; i++, m++ )
         {
            d[m] = s[i];
            acc += s[i];
         }

         for( i=0, s=(int*)&Dlist; i<sizeof(DownLoadList)/2; i++, m++ )
         {
            d[m] = s[i];
            acc += s[i];
         }
         d[m] = (int)acc;

         puts( "Bad List/ Fare table loaded into PCMCIA" );
         printf( "size = %ld bytes chksum %x\n", (m*2), d[m] );
         break;
      default:
         break;
   }
}
