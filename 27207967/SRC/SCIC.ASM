*-----------------------------------------------------------------------------+
*  Src File:   scic_v11.asm                                                   |
*  Authored:   01/11/97, tgh                                                  |
*  Function:   SCI (J1708 port) low level.  connector J1 pins 17 & 18.        |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/97, tgh  -  Initial design.                              |
*      2.00:   01/11/97, tgh  -  Make genereic to support PPS.                |
*      3.00:   02/24/97, tgh  -  Modify to allow for a transceiver which has  |
*                                receive always enabled.  Eventually an rx    |
*                                state will be added to disable receive when  |
*                                the first byte or two transmitted is received|
*      3.01:   10/08/97, sjb  -  increase baud rate to 19200.                 |
*      3.02:   07/27/99, sjb  -  disable rxd/txd if probing detected.         |
*      3.03:   10/03/00, sjb  -  change baud rate for j1708 to 9600.          |
*      3.08:   10/25/00, sjb  -  bugger up for j1708                          |
*      3.09:   06/13/01, sjb  -  bugger up for j1708                          |
*      3.10:   06/13/01, sjb  -  check for idle line. It appears idle line    |
*                                should be checked first before rxd or txd to |
*                                process idle line detect interrupt           |
*      3.11    02/27/02, aat  -  corrected leaving j1708 on all the time      |
*                                                                             |
*           Copyright (c) 1993-1997  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc
        include comm.inc

*---------------------------------------------------------------------------+
*                       Local Equates                                       |
*---------------------------------------------------------------------------+

        .RS485_TX:      equ     .bit6           bit 2 (transmit - active high)

* Bits associated with SCI registers
*   SCI control register 0 (SCCR0) baud rate
*   with 16.777MH crystal
*        B9600:          equ     55
*        B19200:         equ     27
*        B38400:         equ     14
*        B57600:         equ      9
*        B76800:         equ      7              really 74898 baud
*        B500000:        equ      1

* Bits associated with SCI registers
*   SCI control register 0 (SCCR0) baud rate
*   with 20.972MH crystal
         B9600:          equ     68
*        B19200:         equ     34
*        B38400:         equ     17
*        B57600:         equ     11


* Bits associated with SCI registers
*   SCI control register 0 (SCCR0) baud rate
*   with 25.166MH crystal
*        B9600:          equ     82
*        B19200:         equ     41
*        B38400:         equ     20
*        B57600:         equ     14              really 80281 baud

* Bits associated with SCI registers
*   SCI control register 0 (SCCR0) baud rate
*   with 25.690MH crystal
*        B9600:          equ     84
*        B19200:         equ     42
*        B38400:         equ     21
*        B57600:         equ     14              really 80281 baud
*        B76800:         equ     10

        BAUD_RATE:      equ     B9600           operating baudrate

*   SCI control register 1 (SCCR1)

        .sbk:           equ     0               send break
        .rwu:           equ     1               receiver wake-up
        .re:            equ     2               receiver enable
        .te:            equ     3               transmitter enable
        .ilie:          equ     4               idle-line interrupt enable
        .rie:           equ     5               receiver interrupt enable
        .tcie:          equ     6               tx complete interrupt enable
        .tie:           equ     7               tx interrupt enable
        .wake:          equ     8               wakeup by address mark
        .m:             equ     9               mode select
        .pe:            equ     10              parity enable
        .pt:            equ     11              parity type
        .ilt:           equ     12              idle-line detect type
        .woms:          equ     13              wired-OR mode for SCI pins
        .loops:         equ     14              LOOP mode


*   SCI status register (SCSR)

        .pf:            equ     0               parity error flag
        .fe:            equ     1               framing error flag
        .nf:            equ     2               noise error flag
        .or:            equ     3               overrun error flag
        .idle:          equ     4               idle-line detected flag
        .raf:           equ     5               receiver active flag
        .rdrf:          equ     6               receive data register full flag
        .tc:            equ     7               transmit complete flag
        .tdre:          equ     8               transmit data reg. empty flag


* queue equates :

        RX_BUF_SIZE:    equ     256
        QUEUE_SIZE:     equ     1024

*---------------------------------------------------------------------------+
*                       Local Macros                                        |
*---------------------------------------------------------------------------+
                        nolist                  turn off listing

*       Common Exit
        exit_interrupt: macro
                        jmp     irq_exit
                        endm

*       Zero flag set if tx is idle.
        tx_idle:        macro
                        move.l  a0,-(sp)        save register used
                        movea.l tx_id,a0        check state
                        cmpa.l  #tx_disable,a0  .
                        move.l  (sp)+,a0        restore register used
                        endm
                        
*       Zero flag set if rx is idle.
        rx_idle:        macro
                        movea.l a0,-(sp)        save register
                        movea.l rx_id,a0        check state
                        cmpa.l  #getc,a0        should be equal to this
                        movea.l (sp)+,a0        restore register
                        endm

*       Define the next transmit state
        next_tx:        macro
                        lea.l   \1,a1
                        move.l  a1,tx_id
                        endm

*       Define the next receive state
        next_rx:        macro
                        lea.l   \1,a1
                        move.l  a1,rx_id
                        endm

*       Set rx & tx to default states
        default_states: macro
                        next_rx getc
                        next_tx tx_disable
                        endm

*       Read a duart register leaving result in reg. d0
*       usage: getreg reg
*       reg   - register offset
        getreg:         macro
                        move.w  \1,d0
                        endm

*       Write the contents on reg. d0 to a duart register.
*       usage: putreg reg,value
*       reg   - register offset
*       value - byte to send
        putreg:         macro
                ifnc '\2',''
                        move.w  \2,d0
                endc
                        move.w  d0,\1
                        endm


*       Transmit a byte
*       usage: transmit value
        transmit:       macro
                        putreg  scdr,\1
                        endm

*       Enable the transmitter
        enable_tx:      macro
                        move.l  d0,-(sp)
                        txmode
                        move.w  sccr1,d0
                        bset    #.tcie,d0
                        move.w  d0,sccr1
                        move.l  (sp)+,d0
                        endm

*       Disable the transmitter
        disable_tx:     macro
                        move.l  d0,-(sp)
                        move.w  sccr1,d0
                        bclr    #.tcie,d0
                        move.w  d0,sccr1
                        rxmode
                        move.l  (sp)+,d0
                        endm

*       Copy n bytes into queue
*       a0 -> source
*       d1 -  holds count
        _ncpy:          macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,(a1)+     move to destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@       no, continue
                        movea.l #q,a1           else, wrap-around
                no_wrap\@:
                        dbra    d1,next\@       update/check byte counter
                        move.l  a1,q_front      save pointer
                        endm

*       Copy NULL terminated string into queue
*       a0 -> source
        _cpy:           macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,d0        get source byte
                        bne.s   not_null\@
                        jmp     exit\@          NULL, get out
                not_null\@:
                        move.b  d0,(a1)+        save in destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@
                        movea.l #q,a1
                no_wrap\@:
                        jmp     next\@
                exit\@:
                        movea.l a1,q_front
                        endm

*       Pointer is in "a1"
*       usage: _bump_ptr <ptr>
        _bump_ptr:      macro  ptr
                        addq.l  #1,a1           bump a1
                        cmpa.l  #q_end,a1       check for wrap-around
                        blo.s   ok\@
                        movea.l #q,a1           wrap-around
                ok\@:   move.l  a1,\1
                        endm

*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
* Half duplex (rs485) specific macros
*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*       Line-turn-around (transmit)
        txmode:         macro
                        move.l  d0,-(sp)        save register used
                        move.w  scsr,d0         clear status reg.
                        move.b  qpdr,d0         get current state
                        bset    #.RS485_TX,d0   set bit (active high)
                        move.b  d0,qpdr         and actual port value
                        next_rx getc_tx
                        bra     exit_tx\@                

               exit_tx\@:
                        move.l  (sp)+,d0        restore register used
                        endm

*       Line-turn-around (receive)
        rxmode:         macro
                        movem.l d0-d1,-(sp)     save registers used
                        move.w  #500,d1         timeout counter
            wait\@:     move.w  scsr,d0         check for tx complete flag
                        btst    #.tc,d0
                        dbne    d1,wait\@
                        move.b  qpdr,d0         get current state
                        bclr    #.RS485_TX,d0   clear bit
                        move.b  d0,qpdr         and update port value

                        move.w  sccr1,d0        enable receiver
                        bset    #.re,d0
                        move.w  d0,sccr1

                        movem.l (sp)+,d0-d1     restore registers used
                        endm

                        list                    turn off listing
*---------------------------------------------------------------------------+
*                       Data Segment (local data)                           |
*---------------------------------------------------------------------------+
*                section udata,,"data"           uninitialized data
                section xdata,,"xdata"           uninitialized data

*                       ds.b    0  word align

        q_front:        ds.l    1               tx queue front pointer
        q_back:         ds.l    1               tx queue back pointer
        q:              ds.b    QUEUE_SIZE      tx queue
        q_end:

        tx_id:          ds.l    1               tx state

        rx_id:          ds.l    1               rx state
        rx_buf:         ds.b    RX_BUF_SIZE     receive buffer
        rx_ndx:         ds.l    1               index into rx buffer
        rx_bck:         ds.l    1               pointer to back of buffer
        rx_cnt:         ds.w    1               char. count in buffer
                        ds.b    1  word align
        rx_tx_collide:  ds.b    1

*---------------------------------------------------------------------------+
*                       External References                                 |
*---------------------------------------------------------------------------+
* external data
        xref    _Sflags,_Aflags
        xref    _Idle_Line

* external functions

*---------------------------------------------------------------------------+
*                       Code Segment                                        |
*---------------------------------------------------------------------------+
                section S_sci_eh,,"code"

        _fnct   _sci_eh                         SCI exception handler

                movem.l d0-d7/a0-a6,-(sp)       save registers used

                getreg  scsr                    check status reg. for errors
                move.w  d0,d1                   copy in d1
                andi.b  #00000000b,d1           check OR, FE, NF & P errors
                beq.s   irq_id                  no errors, process interrupt

                getreg  scdr                    else clear out receive reg.
                exit_interrupt                  and get out.

        irq_id:
                btst    #.idle,d0               Idle Line Interrupt ?
                beq.s   check_rx
                getreg  scdr                    clear char from holding reg.
                jmp     idle_line               and perform next function

        check_rx:
                btst    #.rdrf,d0               Receive Interrupt ?
                beq.s   check_tx
                getreg  scdr                    get char from holding reg.
                movea.l rx_id,a1                get the current receive state
                jmp     (a1)                    and perform next function

        check_tx:
                btst    #.tdre,d0               Transmit Interrupt ?
                beq.s   check_end
                movea.l tx_id,a1                get the current receive state
                jmp     (a1)                    and perform next function

        check_end:
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Idle line States                                    |
*---------------------------------------------------------------------------+

        idle_line:
                jsr  _Idle_Line               idle line detected
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Receive States                                      |
*---------------------------------------------------------------------------+

*  this section for J1708 collision detection  *

        getc_tx:
                cmp.b   rx_tx_collide,d0        is what we sent what we received ? 
                bne.s   get_collide             
                move.w  sccr1,d1                yes, disable receiver
                bclr    #.re,d1
                move.w  d1,sccr1
                clr.b   rx_tx_collide           clear collision
                next_rx getc                    set next receive state
                bra     getc

* collision detected, kill transmitter * 

        get_collide:                             
                move.b  qpdr,d1                 get current state
                bclr    #.RS485_TX,d1           set bit (active high)
                move.b  d1,qpdr                 and actual port value
       
                move.w  sccr1,d1                disable transmitter
                bclr    #.tcie,d1
                move.w  d1,sccr1
                movea.l q_back,a1               get pointer
                movea.l a1,q_front              make the same as front pointer
                clr.b   rx_tx_collide           clear collision
                next_rx getc                    set next receive state

*  this section for J1708 collision detection  *

        getc:
                movea.l rx_ndx,a1               get index
                move.b  d0,(a1)+                store character
                move.l  a1,rx_ndx               store index
                move.w  rx_cnt,d0               get count
                addq.w  #1,d0                   bump count
                cmpi.w  #RX_BUF_SIZE,d0         check range
                bls.s   rx_cnt_ok               ok, continue
                movea.l rx_bck,a1               get back pointer
                addq.l  #1,a1                   bump count
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1  check it
                blo.s   rx_bck_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
           rx_bck_ok:
                move.l  a1,rx_bck
                move.w  #RX_BUF_SIZE,d0         rx_cnt = maximum value
           rx_cnt_ok:
                move.w  d0,rx_cnt
                movea.l rx_ndx,a1               check index
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1
                blo.s   rx_ndx_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
                move.l  a1,rx_ndx
           rx_ndx_ok:
                _flag_comm_rx COMM_SCI
           exit_rx_irq: 
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Transmit States                                     |
*---------------------------------------------------------------------------+
   qputs:
                movea.l q_back,a1               get pointer
                cmpa.l  q_front,a1              any data ?
                bne.s   q_data_present
                clr.b   rx_tx_collide 
                jmp     qputs_fini

        q_data_present:
                move.b  (a1),d0                 get byte it points to and
                move.b  d0,rx_tx_collide        save temp for collision detect
                transmit
                _bump_ptr q_back                update pointers
                cmpa.l  q_front,a1              check for finish
                beq.s   qputs_fini
                exit_interrupt

        qputs_fini:
                next_tx tx_disable


*-----------------------------------------------> Perform a line-turn-around
   rx_mode:
                disable_tx                      turn it off
                next_tx tx_disable              next tx state
                exit_interrupt

*-----------------------------------------------> Perform a line-turn-around
   tx_disable:
                disable_tx

        irq_exit:
                movem.l (sp)+,d0-d7/a0-a6       restore registers used

             rte

*---------------------------------------------------------------------------+
*   Function:   Transmit a NULL terminated string without adding new line.  |
*   Synopsis:   int error = SciPuts( char *s )                              |
*   Input   :   char *s    - points to NULL terminated string.              |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _SciPuts
                link    a6,#0                   set up frame
                movem.l a0/a1,-(sp)             save registers used
                movea.l 8(a6),a0                a0 -> source
                _cpy                            copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go
                clr.w   d0                      return ok
                bra.s   puts_exit               exit

           puts_err:
                move.w  #-1,d0                  return error

        puts_exit:
                movem.l (sp)+,a0/a1             restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit an N byte string without adding new line.          |
*   Synopsis:   int error = SciNputs( char *s, int n )                      |
*   Input   :   char *s    - points to NULL terminated string.              |
*           :   int  n     - number of bytes to transmit.                   |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _SciNputs
                link    a6,#0                   set up frame
                movem.l d1/a0/a1,-(sp)          save registers used
                movea.l 8(a6),a0                a0 -> source
                move.w  12(a6),d1               d1 -  number of bytes
                subq.w  #1,d1                   ajust byte count
                bmi.l   nputs_err               and check range
                _ncpy                           copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go
                clr.w   d0                      return ok
                bra.s   nputs_exit              exit

           nputs_err:
                move.w  #-1,d0                  return error

        nputs_exit:
                movem.l (sp)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit a character (low level routine)                    |
*   Synopsis:   int error = SciPutc( char c )                               |
*   Input   :   char c     - character to transmit.                         |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _SciPutc
                link    a6,#0                   setup stack frame
                movem.l a1,-(a7)                save registers used
                movea.l q_front,a1              get pointer
                move.b  9(a6),(a1)              store character in queue
                _bump_ptr q_front               update pointers
                next_tx qputs                   update transmit state
                enable_tx                       and let'em go
                movem.l (a7)+,a1                restore registers used
                move.b  4(a6),d0                return character to caller
                unlk    a6                      restore frame
             rts


*---------------------------------------------------------------------------+
*   Function:   Get a character (low level routine)                         |
*   Synopsis:   int error = SciGetc( void )                                 |
*   Input   :   None.                                                       |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:   Sytem timers are scanned within this function.              |
*---------------------------------------------------------------------------+
        _fnct   _SciGetc

                movem.l a0,-(a7)                save registers used

           getc_wait:
                tst.w   rx_cnt                  check for character(s)
                bne.s   getc_ready              character ready
                move.b  #0,d0                   return NULL
                bra.s   getc_exit               keep waiting

           getc_ready:
                move.w  rx_cnt,d0               check count
                movea.l rx_bck,a0               get index
                move.b  (a0)+,d0                get character
                cmpa.l  #rx_buf+RX_BUF_SIZE,a0  check pointers
                blo.s   s_g_rx_bck_ok           ok, continue
                movea.l #rx_buf,a0              else, wrap-around

           s_g_rx_bck_ok:
                move.l  a0,rx_bck               store back pointer
                subi.w  #1,rx_cnt               update count

           getc_exit:
                movem.l (a7)+,a0                restore registers used

             rts


*---------------------------------------------------------------------------+
*   Function:   Check to see if a character is pending.                     |
*   Synopsis:   int n = SciCnt( void )                                      |
*   Input   :   None.                                                       |
*   Output  :   int  n     - returns the number of characters pending.      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _SciCnt
                move.w  rx_cnt,d0               return count in d0
             rts


*---------------------------------------------------------------------------+
*   Function:   Set up receiver state.                                      |
*   Synopsis:   Setup receiver after transmit time out of J1708             |
*   Input   :   None.                                                       |
*   Output  :                                                               |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _SciRec

                move.w  sr,-(sp)                save current state
                ori.w   #0700h,sr               disable interrupts
                next_rx getc                    set next receive state
                move.w  sccr1,d0                
                btst    #.re,d0                 check if receiver enabled
                beq.s   scirec_end
                bclr    #.re,d0                 disable receiver
                move.w  d0,sccr1

             scirec_end:
                move.w  (sp)+,sr                restore state
             rts

*---------------------------------------------------------------------------+
*   Function:   Initialize the debug port (stdin / stdout).                 |
*   Synopsis:   int error = InitializeSciComm( baud )                       |
*   Input   :   j1708/PPS baud rate select  (1 == j1708 )                   |
*   Output  :   int error  - returns 0 (OK)                                 |
*   Comments:   19200 baud                                                  |
*---------------------------------------------------------------------------+
        _fnct   _InitializeSciComm

                movem.l a0/a1,-(a7)             save registers used

                clr.w   rx_cnt                  clear rx byte count
                movea.l #rx_buf,a0              setup rx queue pointers
                move.l  a0,rx_ndx
                move.l  a0,rx_bck

                movea.l #q,a0                   setup tx queue pointers
                move.l  a0,q_front
                move.l  a0,q_back

                default_states                  ; set rx & tx states

                move.w  scsr,d0                 clear status reg.
                move.w  scdr,d0                 clear data reg.
                rxmode                          receive mode
                move.w  #BAUD_RATE,sccr0        j1708 at 9600
                move.w  #0001000000111100b,sccr1 rx, rx irq, idle & tx enabled
*                                                long idle line detect
                movem.l (a7)+,a0/a1               restore registers used

                clr.w   d0                      return zero

             rts
