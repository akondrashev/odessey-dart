*-----------------------------------------------------------------------------+
*  Src File:   q0a.asm                                                        |
*  Version :   2.00                                                           |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   Low level bill mech comm. quart 0 port A. ( CashCode )         |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/96, tgh  - Initial release.                              |
*      1.01:   06/18/97                                                       |
*      1.02:   06/18/97, sjb  - modify Bill_MOTOR_OFF function, interrupts are|
*                               killed and restrored in bvp__vxx              |
*      1.04:   06/28/01, jks  - bill validator (BV07) has a different baud    |
*                               rate then the old bill validator              |
*      1.05:   09/18/01, jks  - change bill validator (BV07) to 7 bytes       |
*                               with even parity                              |
*      1.06:   02/19/04, sjb  - modify to run quart at full speed             |
*      1.07:   03/17/05, sjb  - check THR before exiting irq to see if ready  |
*                               for another character to send                 |
*      2.00:   06/13/05, sjb  - Initial release using new xr16L784 quad uart  |
*      2.00:   08/18/05, sjb  - Fix reading signal of CTS in Bill_Init()      |
*                               inverted as compared to old uarts             |
*                                                                             |
*           Copyright (c) 1993-2001  GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc
        include comm.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

*       Hardware dependant equates :

        uart_base:     equ     uart0base       base address
        uarta:         equ     0               define as 1 if port A
        uartb:         equ     1               define as 1 if port B
        uartc:         equ     0               define as 1 if port C
        uartd:         equ     0               define as 1 if port D

  ifne uarta
        imr_value:  equ     uart_base+uart_port_a location of imr value
        uart:       equ     1  
  endc
  ifne uartb
        imr_value:  equ     uart_base+uart_port_b location of imr value
        uart:       equ     2  
  endc
  ifne uartc
        imr_value:  equ     uart_base+uart_port_c location of imr value
        uart:       equ     4  
  endc
  ifne uartd
        imr_value:  equ     uart_base+uart_port_d location of imr value
        uart:       equ     8  
  endc
        rhr:   equ  imr_value+RHR   receive holding register LRC[7] = 0  (r)
        thr:   equ  imr_value+THR   transmit holding register LRC[7] = 0 (w)
        dll:   equ  imr_value+DLL   div latch low byte LRC[7] = 1        (r/w)
        dlm:   equ  imr_value+DLM   div latch hi byte  LRC[7] = 1        (r/w)
        ier:   equ  imr_value+IER   interrupt enable register LRC[7] = 1 (r/w)
        isr:   equ  imr_value+ISR   interrupt status register            (r)
        fcr:   equ  imr_value+FCR   fifo control register                (w)
        lcr:   equ  imr_value+LCR   line control Regester                (r/w)
        mcr_:  equ  imr_value+MCR   modem control Regester               (r/w)
        lsr:   equ  imr_value+LSR   line status Regester                 (r)
        msr:   equ  imr_value+MSR   modem status Regester                (r)
        rs485: equ  imr_value+RS485 rs485 turn around delay Regester     (w)
        spr:   equ  imr_value+SPR   scratch pad register                 (r/w)

*       enhanced registers

        fctr:  equ  imr_value+FCTR  feature control register             (r/w)
        efr:   equ  imr_value+EFR   enhansed function register           (r/w)
        txcnt: equ  imr_value+TXCNT xmit fifo level counter              (r)
        txtrg: equ  imr_value+TXTRG xmit fifo trigger level              (w)
        rxcnt: equ  imr_value+RXCNT receive fifo level counter           (r)
        rxtrg: equ  imr_value+RXTRG receive fifo trigger level           (w)
        xoff1: equ  imr_value+XOFF1 xoff character 1                     (w)
        xchar: equ  imr_value+XCHAR xchar        xon,xoff receive flags  (r)  
        xoff2: equ  imr_value+XOFF2 xoff character 2                     (w)
        xon1:  equ  imr_value+XON1  xon character 1                      (w)
        xon2:  equ  imr_value+XON2  xon character 2                      (w)


* default baud rate
        BAUDRATE:       equ     B_600
        BV07_BAUDRATE:  equ     B_9600
        RX_BUF_SIZE:    equ     128
        QUEUE_SIZE:     equ     512


*-----------------------------------------------------------------------------+
*                       Local Macros                                          |
*-----------------------------------------------------------------------------+
*       Common Exit
        exit_interrupt: macro
                        jmp     irq_exit
                        endm

*       Zero flag set if tx is idle.
        tx_idle:        macro
                        move.l  a0,-(sp)        save register used
                        movea.l tx_id,a0        check state
                        cmpa.l  #tx_disable,a0  .
                        move.l  (sp)+,a0        restore register used
                        endm
                        
*       Define the next transmit state
        next_tx:        macro
                        lea.l   \1,a1
                        move.l  a1,tx_id
                        endm

*       Define the next receive state
        next_rx:        macro
                        lea.l   \1,a1
                        move.l  a1,rx_id
                        endm

*       Set rx & tx to default states
        default_states: macro
                        next_rx getc
                        next_tx tx_disable
                        endm

*       Read a quart register leaving result in reg. d0
*       usage: getreg reg
*       reg   - register offset
        getreg:         macro
                        move.b  \1,d0
                        endm

*       Write the contents on reg. d0 to a uart register.
*       usage: putreg reg,value
*       reg   - register offset
*       value - byte to send
        putreg:         macro
                ifnc '\2',''
                        move.b  \2,d0
                endc
                        move.b  d0,\1
                        endm


*       Get this uart's imr register contents
        get_imr:        macro
                        move.b  ier,d0
                        endm

*       Put this uart's imr register contents and update common memory
*       usage: put_imr value
        put_imr:        macro
                        move.b  d0,ier
                        endm

*       Transmit a byte
*       usage: transmit value
        transmit:       macro
                        putreg  thr,\1
                        endm


*       Enable the transmitter
        enable_tx:      macro
                        get_imr
                        bset    #.tx_irq,d0
                        put_imr
                        endm

*       Disable the transmitter
        disable_tx:     macro
                        get_imr
                        bclr    #.tx_irq,d0
                        put_imr
                        endm

*       Copy n bytes into queue
*       a0 -> source
*       d1 -  holds count
        _ncpy:          macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,(a1)+     move to destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@       no, continue
                        movea.l #q,a1           else, wrap-around
                no_wrap\@:
                        dbra    d1,next\@       update/check byte counter
                        move.l  a1,q_front      save pointer
                        endm

*       Copy NULL terminated string into queue
*       a0 -> source
        _cpy:           macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,d0        get source byte
                        bne.s   not_null\@
                        jmp     exit\@          NULL, get out
                not_null\@:
                        move.b  d0,(a1)+        save in destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@
                        movea.l #q,a1
                no_wrap\@:
                        jmp     next\@
                exit\@:
                        movea.l a1,q_front
                        endm

*       Pointer is in "a1"
*       usage: _bump_ptr <ptr>
        _bump_ptr:      macro  ptr
                        addq.l  #1,a1           bump a1
                        cmpa.l  #q_end,a1       check for wrap-around
                        blo.s   ok\@
                        movea.l #q,a1           wrap-around
                ok\@:   move.l  a1,\1
                        endm


*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
*                section udata,,"data"           uninitialized data
                section xdata,,"xdata"           uninitialized data

                        ds.b    0  word align

        isr_temp:       ds.b    1

        q_front:        ds.l    1               tx queue front pointer
        q_back:         ds.l    1               tx queue back pointer
        q:              ds.b    QUEUE_SIZE      tx queue
        q_end:

        tx_id:          ds.l    1               tx state

        rx_id:          ds.l    1               rx state
        rx_buf:         ds.b    RX_BUF_SIZE     receive buffer
        rx_ndx:         ds.l    1               index into rx buffer
        rx_bck:         ds.l    1               pointer to back of buffer
        rx_cnt:         ds.w    1               char. count in buffer

*---------------------------------------------------------------------------+
*                       External References                                 |
*---------------------------------------------------------------------------+
* external data
*        xref    quart0_imr1_value
*        xref    quart0_imr2_value
         xref    _Check_Bill_CTS
*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_ProcessQ0AComm,,"code"


        _fnct   ProcessQ0AComm                  called from "quarts.asm"

                getreg  isr                     get interrupt source
                move.b  d0,isr_temp             temp save to process later
                andi.b  #rx_err,d0                .
                cmpi.b  #rx_err,d0                
                bne     irq_id                  any errors ?
                getreg  lsr                     clear errors, process interrupt 
                exit_interrupt                  and get out.

        irq_id:
                move.b  isr_temp,d0
                btst    #.rx_irq,d0             Receive Interrupt ?
                beq.s   check_tx
                getreg  rhr                     get char from holding reg.
                movea.l rx_id,a1                get the current receive state
                jmp     (a1)                    and perform next function

        check_tx:
                move.b  isr_temp,d0
                btst    #.tx_irq,d0             Transmit Interrupt ?
                beq.s   check_end
                movea.l tx_id,a1                get the current receive state
                jmp     (a1)                    and perform next function

        check_end:
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Receive States                                      |
*---------------------------------------------------------------------------+
        getc:
                movea.l rx_ndx,a1               get index
                move.b  d0,(a1)+                store character
                move.l  a1,rx_ndx               store index
                move.w  rx_cnt,d0               get count
                addq.w  #1,d0                   bump count
                cmpi.w  #RX_BUF_SIZE,d0         check range
                bls.s   rx_cnt_ok               ok, continue
                movea.l rx_bck,a1               get back pointer
                addq.l  #1,a1                   bump count
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1  check it
                blo.s   rx_bck_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
           rx_bck_ok:
                move.l  a1,rx_bck
                move.w  #RX_BUF_SIZE,d0         rx_cnt = maximum value
           rx_cnt_ok:
                move.w  d0,rx_cnt
                movea.l rx_ndx,a1               check index
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1
                blo.s   rx_ndx_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
                move.l  a1,rx_ndx
           rx_ndx_ok:
                getreg  lsr                     more data ?
                btst    #.bit0,d0               Receive Interrupt ?
                beq.s   rx_flag_data
                getreg  rhr                     get char from holding reg.
                bra     getc                    and queue it up

           rx_flag_data:
                _flag_comm_rx COMM_Q0A
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Transmit States                                     |
*---------------------------------------------------------------------------+
   qputs:
                movea.l q_back,a1               get pointer
                cmpa.l  q_front,a1              any data ?
                beq.s   qputs_fini
                transmit (a1)                   transmit data it points to
                _bump_ptr q_back                update pointers
                cmpa.l  q_front,a1              check for finish
                beq.s   qputs_fini
                getreg  lsr                     more data ?
                btst    #.bit5,d0               Receive Interrupt ?
                bne.s   qputs                   branch if yes
                exit_interrupt

        qputs_fini:
                next_tx tx_disable

        tx_disable:
                disable_tx

        irq_exit:
             rts

*---------------------------------------------------------------------------+
*   Function:   Transmit a NULL terminated string without adding new line.  |
*   Synopsis:   int error = Q0APuts( char *s )                              |
*   Input   :   char *s    - points to NULL terminated string.              |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Q0APuts
                link    a6,#0                   set up frame
                movem.l a0/a1,-(sp)             save registers used
                movea.l 8(a6),a0                a0 -> source
                _cpy                            copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go
                clr.w   d0                      return ok
                bra.s   puts_exit               exit

           puts_err:
                move.w  #-1,d0                  return error

        puts_exit:
                movem.l (sp)+,a0/a1             restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit an N byte string without adding new line.          |
*   Synopsis:   int error = Q0ANputs( char *s, int n )                      |
*   Input   :   char *s    - points to NULL terminated string.              |
*           :   int  n     - number of bytes to transmit.                   |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Q0ANputs
                link    a6,#0                   set up frame
                movem.l d1/a0/a1,-(sp)          save registers used
                movea.l 8(a6),a0                a0 -> source
                move.w  12(a6),d1               d1 -  number of bytes
                subq.w  #1,d1                   ajust byte count
                bmi.s   nputs_err               and check range
                _ncpy                           copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go

                clr.w   d0                      return ok
                bra.s   nputs_exit              exit

           nputs_err:
                move.w  #-1,d0                  return error

        nputs_exit:
                movem.l (sp)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit a character (low level routine)                    |
*   Synopsis:   int error = Q0APutc( char c )                               |
*   Input   :   char c     - character to transmit.                         |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Q0APutc
                link    a6,#0                   setup stack frame
                movem.l a1,-(a7)                save registers used
                movea.l q_front,a1              get pointer
                move.b  9(a6),(a1)              store character in queue
                _bump_ptr q_front               update pointers
                next_tx qputs                   update transmit state
                enable_tx                       and let'em go
                movem.l (a7)+,a1                restore registers used
                move.b  4(a6),d0                return character to caller
                unlk    a6                      restore frame
             rts


*---------------------------------------------------------------------------+
*   Function:   Get a character (low level routine)                         |
*   Synopsis:   int error = Q0AGetc( void )                                 |
*   Input   :   None.                                                       |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:   Sytem timers are scanned within this function.              |
*---------------------------------------------------------------------------+
        _fnct   _Q0AGetc

                movem.l a0,-(a7)                save registers used

           getc_wait:
                tst.w   rx_cnt                  check for character(s)
                bne.s   getc_ready              character ready
                move.b  #0,d0                   return NULL
                bra.s   getc_exit               keep waiting

           getc_ready:
                move.w  rx_cnt,d0               check count
                movea.l rx_bck,a0               get index
                move.b  (a0)+,d0                get character
                cmpa.l  #rx_buf+RX_BUF_SIZE,a0  check pointers
                blo.s   s_g_rx_bck_ok           ok, continue
                movea.l #rx_buf,a0              else, wrap-around

           s_g_rx_bck_ok:
                move.l  a0,rx_bck               store back pointer
                subi.w  #1,rx_cnt               update count

           getc_exit:
                movem.l (a7)+,a0                restore registers used

             rts


*---------------------------------------------------------------------------+
*   Function:   Check to see if a character is pending.                     |
*   Synopsis:   int n = Q0ACnt( void )                                      |
*   Input   :   None.                                                       |
*   Output  :   int  n     - returns the number of characters pending.      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Q0ACnt
                move.w  rx_cnt,d0               return count in d0
             rts

*---------------------------------------------------------------------------+
*   Function:   Send bill validator data                                    |
*   Synopsis:   int n = Q0ACnt( void )                                      |
*   Input   :   None.                                                       |
*   Output  :   None                                                        |
*   Comments:                                                               |
*---------------------------------------------------------------------------+

        _fnct   _Bill_Send_Data

                move.w  sr,-(sp)                     save current state
                ori.w   #0700h,sr                    disable interrupts

                getreg  mcr_                 more data ?
                bset    #.bit1,d0            
                putreg  mcr_

                move.w  (sp)+,sr                     restore state

             rts


*---------------------------------------------------------------------------+
*   Function:   diasable bill validator rts                                 |
*   Synopsis:   int n = Q0ACnt( void )                                      |
*   Input   :   None.                                                       |
*   Output  :   None.                                                       |
*   Comments:                                                               |
*---------------------------------------------------------------------------+

        _fnct   _Bill_Send_Off

                move.w  sr,-(sp)                     save current state
                ori.w   #0700h,sr                    disable interrupts

                getreg  mcr_
                bclr    #.bit1,d0              
                putreg  mcr_

                move.w  (sp)+,sr                     restore state

             rts

*---------------------------------------------------------------------------+
*   Function:   Bill_Init                                                   |
*   Synopsis:   check if data pending from bill validator                   |
*   Input   :   None.                                                       |
*   Output  :   int  - 0 no message, -1 message pending                     |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Bill_Init

                link    a6,#0            set up frame
                getreg  msr                     read modem status data
                andi.w  #0010h,d0
                beq     init_pend        any data ?
                move.w  #-1,d0           yes
                bra.s   Bill_Init_exit   exit

           init_pend:
                move.w  #0,d0            return no data

        Bill_Init_exit:
                unlk    a6               restore frame
             rts                         return to caller


*---------------------------------------------------------------------------+
*   Function:   Initialize the bill validator port                          |
*   Synopsis:   int error = InitializeQ0AComm( void )                       |
*   Input   :   None.                                                       |
*   Output  :   int error  - returns 0 (OK)                                 |
*   Comments:   600 baud                                                    |
*---------------------------------------------------------------------------+
        _fnct   _InitializeQ0AComm

                link    a6,#0
                movem.l d1-d2/a0/a1,-(a7)          save registers used

                clr.w   rx_cnt                  clear rx byte count
                movea.l #rx_buf,a0              setup rx queue pointers
                move.l  a0,rx_ndx
                move.l  a0,rx_bck

                movea.l #q,a0                   setup tx queue pointers
                move.l  a0,q_front
                move.l  a0,q_back

                default_states                  ; set rx & tx states

                move.b  #uart,d1                reset selected uart
                move.b  d1,uart0base+URESET

                move.b  #BAUDRATE,d2            load BV05 default baud
                move.w  8(a6),d0                fetch baudrate argument
                beq     check_baud
                move.b  #BV07_BAUDRATE,d2       load BV07 default baud

          check_baud:
                move.b  #uart,d1                reset selected uart
                not.b   d1
                move.b  uart0base+MODEX8,d0     get sampling rate to mode16
                and.b   d1,d0
                move.b  d0,uart0base+MODEX8     set sampling rate to mode16

          baudrate_setup:
                move.b  #10000000b,lcr          enable divisor for BRG                
                cmpi.b  #B_600,d2
                bne.s   bv07
                move.b  d2,dlm                  set hi byte of BRG
                move.b  #0,dll                  set low byte 
                move.b  #00000011b,lcr          char, 8, none, 1 stop bit
                bra.s   finish

          bv07: 
                move.b  d2,dll                  set low byte of BRG
                move.b  #0,dlm                  set hi byte 
                move.b  #00011010b,lcr          char, 7, even, 1 stop bit
finish:
                move.b  #00000101b,ier          enable receiver

                movem.l (a7)+,d1-d2/a0/a1       restore registers used
                unlk    a6
                clr.w   d0                      return zero

             rts
