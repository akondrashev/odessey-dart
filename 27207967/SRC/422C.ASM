*-----------------------------------------------------------------------------+
*  Src File:   422c.asm                                                       |
*  Version :   2.00                                                           |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   Low level RS422 comm. connector                                |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/96, tgh  -  Initial release.                             |
*      1.01:   10/30/98, sjb  -  modified to talk to dcu                      |
*      1.02:   11/02/98, sjb  -  modified to talk to dcu & coin mech          |
*      1.03:   02/19/04, sjb  -  modify to run quart at full speed            |
*      1.04:   03/23/04, sjb  -  remove read of scsr register in txmode       |
*      1.05:   03/17/05, sjb  -  check THR before exiting irq to see if ready |
*                                for another character to send                |
*      2.00:   06/13/05, sjb  -  Initial release using new xr16L784 quad uart |
*      2.01:   11/24/08, sjb  -  RX_BUF_SIZE & QUE_SIZE                       |
*                                                                             |
*           Copyright (c) 1993-1996  GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc
        include comm.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

*       Hardware dependant equates :

        uart_base:     equ     uart1base       base address
        uarta:         equ     0               define as 1 if port A
        uartb:         equ     0               define as 1 if port B
        uartc:         equ     0               define as 1 if port C
        uartd:         equ     0               define as 1 if port D
        uarte:         equ     0               define as 1 if port E
        uartf:         equ     0               define as 1 if port F
        uartg:         equ     1               define as 1 if port G
        uarth:         equ     0               define as 1 if port H

  ifne uarta
        imr_value:  equ     uart_base+uart_port_a location of imr value
        uart:       equ     1
  endc
  ifne uartb
        imr_value:  equ     uart_base+uart_port_b location of imr value
        uart:       equ     2
  endc
  ifne uartc
        imr_value:  equ     uart_base+uart_port_c location of imr value
        uart:       equ     4
  endc
  ifne uartd
        imr_value:  equ     uart_base+uart_port_d location of imr value
        uart:       equ     8
  endc
  ifne uarte
        imr_value:  equ     uart_base+uart_port_e location of imr value
        uart:       equ     10h
  endc
  ifne uartf
        imr_value:  equ     uart_base+uart_port_f location of imr value
        uart:       equ     20h
  endc
  ifne uartg
        imr_value:  equ     uart_base+uart_port_g location of imr value
        uart:       equ     40h
  endc
  ifne uarth
        imr_value:  equ     uart_base+uart_port_h location of imr value
        uart:       equ     80h
  endc

        rhr:   equ  imr_value+RHR   receive holding register LRC[7] = 0  (r)
        thr:   equ  imr_value+THR   transmit holding register LRC[7] = 0 (w)
        dll:   equ  imr_value+DLL   div latch low byte LRC[7] = 1        (r/w)
        dlm:   equ  imr_value+DLM   div latch hi byte  LRC[7] = 1        (r/w)
        ier:   equ  imr_value+IER   interrupt enable register LRC[7] = 1 (r/w)
        isr:   equ  imr_value+ISR   interrupt status register            (r)
        fcr:   equ  imr_value+FCR   fifo control register                (w)
        lcr:   equ  imr_value+LCR   line control Regester                (r/w)
        mcr_:  equ  imr_value+MCR   modem control Regester               (r/w)
        lsr:   equ  imr_value+LSR   line status Regester                 (r)
        msr:   equ  imr_value+MSR   modem status Regester                (r)
        rs485: equ  imr_value+RS485 rs485 turn around delay Regester     (w)
        spr:   equ  imr_value+SPR   scratch pad register                 (r/w)

*       enhanced registers

        fctr:  equ  imr_value+FCTR  feature control register             (r/w)
        efr:   equ  imr_value+EFR   enhansed function register           (r/w)
        txcnt: equ  imr_value+TXCNT xmit fifo level counter              (r)
        txtrg: equ  imr_value+TXTRG xmit fifo trigger level              (w)
        rxcnt: equ  imr_value+RXCNT receive fifo level counter           (r)
        rxtrg: equ  imr_value+RXTRG receive fifo trigger level           (w)
        xoff1: equ  imr_value+XOFF1 xoff character 1                     (w)
        xchar: equ  imr_value+XCHAR xchar        xon,xoff receive flags  (r)
        xoff2: equ  imr_value+XOFF2 xoff character 2                     (w)
        xon1:  equ  imr_value+XON1  xon character 1                      (w)
        xon2:  equ  imr_value+XON2  xon character 2                      (w)

        BAUDRATE:       equ     B_19200
        BAUD_RATE:      equ     B_38400

        RX_BUF_SIZE:    equ     2048
        QUEUE_SIZE:     equ     2048

        FIFO_SETUP:     equ     11000111b


*       Read a quart register leaving result in reg. d0
*       usage: getreg reg
*       reg   - register offset
        getreg:         macro
                        move.b  \1,d0
                        endm

*       Write the contents on reg. d0 to a uart register.
*       usage: putreg reg,value
*       reg   - register offset
*       value - byte to send
        putreg:         macro
                ifnc '\2',''
                        move.b  \2,d0
                endc
                        move.b  d0,\1
                        endm



*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
*                section udata,,"data"           uninitialized data
                section xdata,,"xdata"           uninitialized data

        q_front:        ds.l    1               tx queue front pointer
        q_back:         ds.l    1               tx queue back pointer
        q:              ds.b    QUEUE_SIZE      tx queue
        q_end:

        rx_buf:         ds.b    RX_BUF_SIZE     receive buffer
        rx_end:
        rx_ndx:         ds.l    1               index into rx buffer
        rx_bck:         ds.l    1               pointer to back of buffer
        rx_cnt:         ds.w    1               char. count in RX buffer
        tx_cnt:         ds.w    1

*---------------------------------------------------------------------------+
*                       External References                                 |
*---------------------------------------------------------------------------+
* external data
*        xref    quart1_imr1_value
*        xref    quart1_imr2_value

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_ProcessRS422Comm,,"code"

        _fnct   ProcessRS422Comm                Called from "quarts.asm"
                getreg  isr                     Get interrupt source
                btst    #2,d0                   Receive interrupt?
                beq.s   check_tx                No
                btst    #1,d0                   Line status?
                bne.s   rcv3                    Yes
        rcv0:   movea.l rx_ndx,a1               Receive interrupt: get index
                move.w  rx_cnt,d2               d2 - current RX counter
        rcv1:   move.b  rhr,(a1)+               store character
                cmpa.l  #rx_end,a1
                blo.s   rcv2                    ok, continue
                movea.l #rx_buf,a1              no, wrap-around
        rcv2:   addq.w  #1,d2                   increment RX counter
                btst.b  #.bit0,lsr              more data ?
                bne.s   rcv1                    Yes, next character
                move.l  a1,rx_ndx               store index
                cmpi.w  #RX_BUF_SIZE,d2         overflow protection
                bls.s   rcv4
                subi.w  #RX_BUF_SIZE,d2
        rcv4:   move.w  d2,rx_cnt               RX counter update
                _flag_comm_rx COMM_422          set flag
                rts                             exit

     check_end: getreg  msr
        rcv3:   getreg  lsr                     clear errors, process interrupt
                btst    #.bit0,d0
                bne.s   rcv0
                rts

      check_tx: btst    #.tx_irq,d0             Transmit Interrupt ?
                beq.s   check_end
         tx0:   move.w  tx_cnt,d1
                beq.s   tx2                     buffer empty
                cmpi.w  #64,d1                  send up to 64 characters
                bls.s   tx5
                move.w  #64,d1
         tx5:   sub.w   d1,tx_cnt
                movea.l q_back,a1               get pointer
                subq.w  #1,d1
         tx6:   move.b  (a1)+,thr
                cmpa.l  #q_end,a1               check for wrap-around
                blo.s   tx1
                movea.l #q,a1                   wrap-around
         tx1:   dbra    d1,tx6
                move.l  a1,q_back
                rts
         tx2:   move.b  #FIFO_SETUP,fcr         reset FIFO
                getreg  rhr                     read last echo byte
                move.b  #00000101b,ier          disable TX interrupt, enable RX interrupt
                rts


*---------------------------------------------------------------------------+
*   Function:   Transmit an N byte string without adding new line.          |
*   Synopsis:   int error = RS422Nputs( char *s, int n )                    |
*   Input   :   char *s    - points to NULL terminated string.              |
*           :   int  n     - number of bytes to transmit.                   |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RS422Nputs
                link    a6,#0                   set up frame
                movem.l d1/a0/a1,-(sp)          save registers used
                move.w  12(a6),d1               d1 -  number of bytes
                beq.s    nputs3
                movea.l 8(a6),a0                a0 -> source
                move.w  d1,d0
                movea.l q_front,a1              a1 -> destination
                subq.w  #1,d1                   ajust byte count
        nputs1: move.b  (a0)+,(a1)+             move to destination buffer
                cmpa.l  #q_end,a1               check for wrap
                blo.s   nputs2                  no, continue
                movea.l #q,a1                   else, wrap-around
        nputs2: dbra    d1,nputs1               update/check byte counter
                add.w   d0,tx_cnt               increment TX counter
                move.l  a1,q_front
                move.b  #00000010b,ier          enable TX interrupts
        nputs3: movem.l (sp)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
                rts                             return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit a character (low level routine)                    |
*   Synopsis:   int error = RS422Putc( char c )                             |
*   Input   :   char c     - character to transmit.                         |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RS422Putc
                 link    a6,#0                   setup stack frame
                movem.l a1,-(a7)                save registers used
                movea.l q_front,a1              get pointer
                move.b  9(a6),(a1)+             store character in queue
                cmpa.l  #q_end,a1               check for wrap
                blo.s   puts1                   no, continue
                movea.l #q,a1                   else, wrap-around
        puts1:  move.l  a1,q_front              save pointer
                addq.w  #1,tx_cnt
                movem.l (a7)+,a1                restore registers used
                move.b  4(a6),d0                return character to caller
                unlk    a6                      restore frame
                move.b  #00000010b,ier          disable RX interrupt, enable TX interrupt
                rts                             return to caller


*---------------------------------------------------------------------------+
*   Function:   Get a character (low level routine)                         |
*   Synopsis:   int error = RS422Getc( void )                               |
*   Input   :   None.                                                       |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:   Sytem timers are scanned within this function.              |
*---------------------------------------------------------------------------+
        _fnct   _RS422Getc
                tst.w   rx_cnt                  check for character(s)
                beq.s   getc2                   no characters
                movem.l a0,-(a7)                save registers used
                movea.l rx_bck,a0               get index
                move.b  (a0)+,d0                get character
                subq.w  #1,rx_cnt
                cmpa.l  #rx_end,a0              check pointers
                blo.s   getc1                   ok, continue
                movea.l #rx_buf,a0              else, wrap-around
        getc1:  move.l  a0,rx_bck               store back pointer
                movem.l (a7)+,a0                restore registers used
        getc2:  rts


        _fnct   _RS422Ngetc
                move.w  rx_cnt,d0               d0 - number of bytes in RX buffer
                beq.s   nget5                   nothing to receive?
                link    a6,#0                   set up frame
                movem.l d1/a0/a1,-(sp)          save registers used
                move.w  12(a6),d1               d1 -  max number number of bytes to receive
                cmp.w   d0,d1                   d1 = min(d1, rx_cnt)
                bls.s   nget1
                move.w  d0,d1
         nget1: move.w  d1,d0                   d0 - how many bytes we are receivieng
                beq.s   nget4                   no buffer?
                movea.l 8(a6),a0                a0 -> return buffer address
                movea.l rx_bck,a1               a1 -> receive buffer
                subq.w  #1,d1                   ajust byte count
         nget2: move.b  (a1)+,(a0)+             move to destination buffer
                cmpa.l  #rx_end,a1              check for wrap
                blo.s   nget3                   no, continue
                movea.l #rx_buf,a1              else, wrap-around
         nget3: dbra    d1,nget2                update/check byte counter
                move.l  a1,rx_bck
                sub.w   d0,rx_cnt
         nget4: movem.l (sp)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
         nget5: rts                             return to caller


*---------------------------------------------------------------------------+
*   Function:   Check to see if a character is pending.                     |
*   Synopsis:   int n = RS422Cnt( void )                                    |
*   Input   :   None.                                                       |
*   Output  :   int  n     - returns the number of characters pending.      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RS422Cnt
                move.w  rx_cnt,d0
                rts


*---------------------------------------------------------------------------+
*   Function:   Initialize the RS422 port.                                  |
*   Synopsis:   int error = InitializeRS422Comm( void )                     |
*   Input   :   baud rate.                                                       |
*   Output  :   int error  - returns 0 (OK)                                 |
*   Comments:   19200 baud                                                  |
*---------------------------------------------------------------------------+
        _fnct   _InitializeRS422Comm
                link    a6,#0                   setup stack frame
                movem.l d1-d2/a0/a1,-(a7)       save registers used

                move.b  #0,ier

                clr.w   rx_cnt                  clear rx byte count
                movea.l #rx_buf,a0              setup rx queue pointers
                move.l  a0,rx_ndx
                move.l  a0,rx_bck

                clr.w   tx_cnt                  clear tx byte count
                movea.l #q,a0                   setup tx queue pointers
                move.l  a0,q_front
                move.l  a0,q_back

                move.b  #uart,d1                reset selected uart
                move.b  d1,uart1base+URESET

                move.w  8(a6),d2                d2 = baudrate argument
                bne     check_baud
                move.b  #BAUDRATE,d2            load default baud

          check_baud:
                move.b  #uart,d1                reset selected uart
                not.b   d1
                move.b  uart1base+MODEX8,d0     get sampling rate to mode16
                and.b   d1,d0
                move.b  d0,uart1base+MODEX8     set sampling rate to mode16

          baudrate_setup:
                move.b  #10000000b,lcr          enable divisor for BRG
                move.b  d2,dll                  set low byte of BRG
                move.b  #0,dlm                  set hi byte

                move.b  #00000011b,lcr          select no parity 8 bit 1 stop
                move.b  #0,efr
                move.b  #00010000b,efr          enable msr modification
                move.b  #01000000b,msr          rs485 line turn around delay
                move.b  #0,efr
                move.b  #00100000b,fctr         setup for rs485, table A
                move.b  #FIFO_SETUP,fcr         enable and clear fifo: RX 14, TX 1

         set1:  getreg  rhr                     read receiver until it is empty
                getreg  lsr                     more data ?
                btst    #.bit0,d0
                bne.s   set1
                move.b  #00000101b,ier          enable receiver

                movem.l (a7)+,d1-d2/a0/a1       restore registers used
                unlk    a6                      restore frame
                clr.w   d0                      return zero
                rts
