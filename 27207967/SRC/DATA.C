/*----------------------------------------------------------------------------\
|  Src File:   data.c                                                         |
|  Version :   6.35                                                           |
|  Authored:   09/17/96, tgh                                                  |
|  Function:   Data/Configuration maintenance.                                |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: data.c,v $                                                           |
|      1.00:   09/17/96, tgh  -  Initial release.                             |
|      2.00:   01/01/97, tgh  -  Added masterlist and event data.             |
|              01/23/97, sjb  -  clear Ev int ClrMlEv                         |
|      3.00:   01/24/97, sjb  -  modify invalid fare structure detect         |
|                             -  add bad memeory event                        |
|      4.00:   02/28/97, sjb  -  convert event records back to Ev for probing |
|                                via ver 4 data system                        |
|      5.00:   02/28/97, sjb  -  check MlEvSigniture1 1st in InitializeCnf    |
|      6.00:   07/30/97, sjb  -  validate selected fareset                    |
|                             -  check if max events to send in ver 4         |
|                             -  don't clear S_BYPASS_P if not full mem clear |
|      6.01:   11/20/98, sjb  -  modify saving cut time                       |
|                             -  correct processing low battery               |
|      6.02:   11/24/98, sjb  -  add Seattle configuration                    |
|                             -  clear warm/cold starts of both Ml & Mlist    |
|      6.10:   04/24/00, sjb  -  limit for events determined when probed by   |
|                                probe request parameters, add MaxEv variable |
|                                here to be checked and limit set in prbp     |
|      6.11:   11/01/00, sjb  -  check transaction data base to see if ok     |
|      6.12:   02/06/00, sjb  -  clear P_BV_RESET & P_RECHARGE flag on memory |
|      6.12:   02/06/00, sjb  -  clear P_BV_OOS_LOG flag on memory clear      |
|      6.14:   07/03/01, sjb  -  clear friendly agency list if configuration  |
|                                detected bad                                 |
|      6.15:   09/18/01, sjb  -                                               |
|      6.16:   10/31/01, sjb  -  add trim configuration                       |
|      6.17:   02/14/01, aat  -  add command to add logic board type to the   |
|                                farebox version ( thousands digit )          |
|      6.18:   05/22/02, sjb  -  clear coin bill full flags ( used for J1708 )|
|      6.19:   11/05/02, sjb  -  add to configuration data base friendly      |
|                                agencies                                     |
|      6.20:   11/12/02, sjb  -  change processing I/O direction for ver 6    |
|                                data system to 1/2 for inbound/outbound      |
|                             -  add Custmer_ID, usd when configured for ver 4|
|                                or ver 6                                     |
|      6.21:   04/14/03, jks  -  CTS DCU (San Diego) modification:            |
|                                 set/clear status bits                       |
|      6.22:   07/02/04, sjb  -  add hooks to handle a really big bad list    |
|      6.23:   07/31/04, sjb  -  add Trim_Buad rate flag                      |
|      6.24:   08/19/04, sjb  -  check and init if bad SMARTCARD recovery     |
|                                & diag buffers                               |
|      6.25:   09/15/04, sjb  -  add hooks to handle a new CLIST2             |
|      6.26:   09/30/04, sjb  -  add hooks to handle Periodic timed event     |
|      6.27:   10/19/04, sjb  -  limit events if setup for version 4          |
|      6.28:   10/19/04, sjb  -  add hooks for sorting index of Realy big bad |
|                                list                                         |
|      6.29:   01/10/05, sjb  -  add structure for key text received from the |
|                                data system                                  |
|                                                                             |
|      6.30: 05/20/05, aat - add clearing of CTS Diag data. Cumulative when   |
|                            when Memory clear and Differential when probed.  |
|      6.31: 06/21/05, sjb - correct saving 500 events.                       |
|      6.32: 07/28/05, sjb - save Crc_lp & Crc_cs in S_MlEv segment.          |
|      6.33: 10/28/05, sjb - Add hooks for multi-garage fare structure        |
|                            restrictions for Montreal                        |
|      6.34: 11/23/05, sjb - init Clist2.Optionflas2 to 1 if configuration    |
|                            detected bad                                     |
|      6.35: 04/11/06, sjb - correct check of valid preset fares              |
|                            initialize Electronic key flag of cnf bad        |
|                                                                             |
|           Copyright (c) 1993-2003  GFI All Rights Reserved                  |
|                                                                             |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include "cnf.h"
#include "data.h"
#include "status.h"
#include "transact.h"
#include "fbd.h"
#include "gen.h"
#include "util.h"
#include "pbq.h"
#include "display.h"
#include "fare.h"
#include "pccard.h"
#include "led.h"
#include "sound.h"
#include "trim.h"
#include "process.h"
#include "coin.h"
#include "misc.h"
#include "probe.h"
#include "scard.h"
#include "ability.h"
#include "params.h"
#include "rf.h"
#include "diag.h"
#include "ability.h"
#include "bill.h"
#include "rs422.h"

/* Define(s)
*/
#define  CNF_SZ   0x1000
#define  MLEV_SZ  0x8000
#define  BAD_TIME 0xa0000000

extern   int      BadClock;

/*---------------------------------------------------------------------------\
|  Reserved space for Flash type data (2 bytes)located at 0x0011fffc         |
\---------------------------------------------------------------------------*/

#pragma  sep_on segment Flash_Type
   word              Flash_Select;
   word              Mfg_ID;
#pragma  sep_off

/*--------------------------------------------------------------------------\
|   Reserved space for masterlist & events (32k) located at 0x00128000      |
\--------------------------------------------------------------------------*/
#pragma  sep_on segment S_MlEv

static   ulong                MlEvSigniture1;

         EV                   Ev;               /* current event */

   /* The following is sent to data system
      verbatim and should not be moved around.
   */

         ML                   Ml;               /* master list */
         char                 Fs_v[3];          /* fare structure version */
static   byte                 E[MAX_EV][EV_SZ]; /* raw events (for data sys.) */
         int                  MaxEv;

static   ulong                MlEvSigniture2;
         int                  SC_READER;
         int                  Trim_Baud;
         int                  EV_Time_fb;       /* 1 = use fb event time */
         int                  FB_EV_time;       /* time at farebox for ev time */
         int                  Ev_Timer;         /* either ds or fb time reference */
         int                  Ev_Timer_Counter;
         int                  Door_time;        /* Door access time (Nashville CQ) */
         ulong                Crc_cs;
         int                  Crc_lp;
static   ulong                CBOXSigniture;
         CASH_BOX             Cbox;

         int                  FB_Auto_Accept;
         ulong                AL_probe_tm;
         ulong                Cnf_probe_tm;
         ulong                Fbd_probe_tm;
         ulong                FFbd_probe_tm;
         ulong                BBList_probe_tm;
#pragma  sep_off
/*---------------------------------------------------------------------------\
|    End of Reserved space for masterlist & events.                          |
\---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------\
|    Reserved space for configuration data (4K)  located at 0x00138000       |
\---------------------------------------------------------------------------*/
#pragma  sep_on segment S_Cnf

static   unsigned long        CnfSigniture1;

   FBXCNF            FbxCnf;                       /* current configuration     */
   DownLoadList      Dlist;                        /* farestructure ...         */
   DownLoadList      FDlist;                       /* farestructure ...         */
   GFIconfig         Clist;                        /* configuration ...         */
   GFIconfig2        Clist2;                       /* configuration ...         */
   LAYOUT            Layout;                       /* TRiM header information   */
   OLD_AGENCY        OldFriendly;
   PRB_REQ_PARMS     Prb_Req_Parms;
   TRIMCNF           TrimCnf;
   FRIENDLYAGY       FriendlyAgy;
   PRB_CNTL_PARMS    Prb_Cntl_Parms;
   KEYLIST           KeyList;
   long              Customer_ID;
   long              Lock_Code[10]; /* 10 4digit lockcodes received from DCU */
   BBList            BBlist;                       /* big bad list              */

static   unsigned long        CnfSigniture2;
   word              BBIndex[BBL_SIZE];
   word              BBL_CS_crc;
   time_t            ProbeTime;
   time_t            FutureFare;       /* activeation of new fare structure */
   TrimDLList        TrimDlist;
   int               ElecKeyFlag;
   int               EnableMemClr;
   DRIVERLIST        DriverList;
   ROUTELIST         RouteList;
   RUNLIST           RunList;
   TRIPLIST          TripList;
   RANGELIST         RangeList;
   SPEC_CARD         SpecCard;
   BUS_NO_PRNT       VErr_BusPrnt;
   time_t            FutureACSParam;  /* activeation of new fare structure */
   WIPORT            wiport;
   time_t            WI_PORT_Tmr;     /* activeation of WI PORT timer */
static   unsigned long    CnfSigniture3;
   RF_DATA           RF_Data;
   word              RouteLength;     /* used to cal crc on new route table */
   word              ZoneLength;      /* used to cal crc on Zone table */
   byte              ZoneTable[ZONE_SIZE];
   word              FZoneLength;
   byte              FZoneTable[ZONE_SIZE];
   time_t            FutureZone;      /* time when future zone to activate */
   int               ZoneTableFlag;
   PRB_REQ_PARMS     Wi_Prb_Req_Parms;

   WIPORT            wp_config;       /* wiport configuration read from wiport */
   word              ValueLimit;      /* value limit for SmartCard value cards */
   word              RideLimit;       /* ride limit for SmartCard */
   byte              DimBackLite;
#pragma  sep_off

/*---------------------------------------------------------------------------\
|  Reserved space for Ascom configuration data (120K) located at 0x0018f000  |
\---------------------------------------------------------------------------*/

#pragma  sep_on segment S_ASCOM
   byte              Acs_parameters[FILEBUF_SIZE];
   byte              F_Acs_parameters[FILEBUF_SIZE];
#pragma  sep_off

/*---------------------------------------------------------------------------\
|    End of Reserved space for configuration                                 |
\---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------\
|    Reserved space for pccard data                                          |
\---------------------------------------------------------------------------*/
#pragma  sep_on segment S_PCcard

PCCARD PCcard;

#pragma  sep_off

/*---------------------------------------------------------------------------\
|     End of Reserved space for pccard data                                  |
\---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------\
|    Reserved space for backup data                                          |
\---------------------------------------------------------------------------*/
#pragma  sep_on segment S_Backup
    int            AutoLoadFlag;           /* used to indicate if AutoList is valid */
    ulong          AutoLoadSize;           /* size of list received when probed     */
    AUTO_LST_HDR   AutoListHdr;
    byte           AutoLoadList[AUTOLOAD_SZ];

#pragma  sep_off

/*---------------------------------------------------------------------------\
|     End of Reserved space for pccard data                                  |
\---------------------------------------------------------------------------*/


/* Prototypes
*/
int   RawToEv     ( EV*, int );

/*----------------------------------------------\
|  Function:   Initialize_Cnf();                |
|  Purpose :   Initialize Cnf & check MlEv      |
|  Synopsis:   int   Initialize_Cnf( void );    |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
int  InitializeCnf()
{
   int    E=0,i;
   word   crc;
/*   long   big_crc; */

   if ( MlEvSigniture1 != SIGNITURE || MlEvSigniture2 != SIGNITURE ||
      DBinit() )
   {
      ClrMlEv( TRUE );                       /* booboo detected, clear all */
      NewEv( Ev_AUT_MEMCLR, 0 );
      E = TRUE;
   }

  /* check cashbox data */
   if ( CBOXSigniture != SIGNITURE )
   {
     memset( (byte*)&Cbox, 0, sizeof(Cbox));
     CBOXSigniture = SIGNITURE;
   }

  /* check cashbox data */
   if ( Unwanted.Signiture != SIGNITURE )
   {
     memset( &Unwanted, 0, sizeof(UNWANTED));
     Unwanted.Signiture = SIGNITURE;
   }

/* int EV time */
   if ( FB_EV_time < 0 || FB_EV_time > 255 )
      FB_EV_time = 0;

   /** AAT - 10/17/08: Door access (Nashville, TN - Card Quest) **/
   if ( Door_time <= 0 || Door_time > 255 )
      Door_time = 1;

   if ( CnfSigniture1 != SIGNITURE || CnfSigniture2 != SIGNITURE )
   {
      CnfSigniture1 = CnfSigniture2 = SIGNITURE;

      if( !Tst_Sflag( S_INVALIDFS ) )
      {
         NewEv( Ev_FTBLE_CHANGE, 0 );
         Set_Sflag( S_INVALIDFS );
         worble;
      }

      Set_Pflag( P_SORT );                    /* make sure bad list sorted */
      ElecKeyFlag = 0;                        /* disable Electronic key */
      ProbeTime = 0;                          /* clear probe timer */

      memset( (byte*)Clist.PUBLIC.AgencyGoodList, 0,
         sizeof(Clist.PUBLIC.AgencyGoodList) );

      memset( (byte*)OldFriendly.OldAgency, 0,
         sizeof(OldFriendly.OldAgency) );

		for ( i = 0; i < MAX_AGENCIES; i++ )
		{
		   Clist.PUBLIC.AgencyGoodList[i].Agency = -1;
         OldFriendly.OldAgency[i].Agency = OLDAGENCY_DISABLED;
		}
      Clist2.hourlyevent &= ~PROBE_ID_REQ;
      Clr_Pflag( P_BIG_BADLIST|CLIST2 );
      Clist2.logon          &= ~CUBIC_DCU;
      Clist.PRIVATE.OpFlags |= C_DATA;
      Clist.PRIVATE.OpFlags &= ~C_VER4;
      Clist2.OptionFlags3 &= ~LANGUAGE_MASK;
      Clist2.OptionFlags2 = 1;
   }
   else
      Clist.PRIVATE.OpFlags &= ALL_CFLAGS;

   if ( !IsGoodDlist( NULL ) )
   {
      if( !Tst_Sflag( S_INVALIDFS ) )
      {
         NewEv( Ev_FTBLE_CHANGE, 0 );
         Set_Sflag( S_INVALIDFS );
         BeepOnce( 5 );
      }
   }
   else
       CreateTrimDlist();

   if ( Tst_Pflag( P_BIG_BADLIST ) )
   {
      crc = crc16( (char*)&BBlist, sizeof(BBList) );
      WatchDog( _WDT_SERVICE );
   }
   else
   {
      crc = crc16( (char*)&BBlist, (BBL_SSIZE*10)+4 );
   }
/*   swab( (char*)&crc, (char*)&crc, 2); */

   if ( crc )
   {
      Clr_Pflag( P_BIG_BADLIST ); /* somethings broken, set to small bad list */
/*      memset( (char*)&BBlist, 0, (BBL_SSIZE*10)+4 );
      memset( (char*)&BBIndex, 0, BBL_SIZE );
      crc = crc16( (char*)&BBlist, (BBL_SSIZE*10)+2 );
      swab( (char*)&crc, (char*)&crc, 2);
      memcpy( (char*)&BBlist.BadList[BBL_SSIZE+2], (char*)&crc, 2 );
      BBlist.BBL_crc = crc;
      BBlist.BBL_ver = 0;
      Set_Pflag( P_SORT );
      Clr_Aflag( A_BL_CKSUM );
*/
      Set_S2flag( S2_BAD_BAD_LIST );
   }
   else
   {
      Clr_S2flag( S2_BAD_BAD_LIST );
   }
   AutoLoadFlag = FALSE;
   if ( Clist.PRIVATE.PartNo == D27207 )
   {
      if ( (AutoLoadSize >= sizeof( AUTO_LST_HDR )) && AutoLoadSize < AUTOLOAD_SZ )
      {
         WatchDog( _WDT_SERVICE );
         if ( memcmp( (char*)&AutoLoadList[AutoLoadSize], (char*)&AutoListHdr.crc32, 4 ) == 0 )
            AutoLoadFlag = TRUE;
/*
      big_crc = crc32( (char*)&AutoListHdr.cat, (AutoLoadSize-4) );
      if ( big_crc ==  Long( &AutoListHdr ) )
         AutoLoadFlag = TRUE;
*/
      }
   }

   if ( CnfSigniture3 != SIGNITURE )
   {
      CnfSigniture3 = SIGNITURE;
      memset( (char*)&RF_Data, 0, sizeof( RF_Data ));
   }

   if ( ZoneLength < 4 || ZoneLength > ZONE_SIZE )
   {
     ZoneTableFlag = FALSE;
   }
   else
   {
      crc = crc16( &ZoneTable, ZoneLength-2 );
      swab( (char*)&crc, (char*)&crc, 2);
      if (  crc == (ZoneTable[ZoneLength-2]<<8|ZoneTable[ZoneLength-1]))
         ZoneTableFlag = TRUE;
      else
         ZoneTableFlag = FALSE;
   }

   if ( SC_Diag.Signiture != SC_SIGNATURE )
   {
      memset( &SC_Diag, 0, sizeof(SC_DIAG));
      SC_Diag.Signiture = SC_SIGNATURE;
   }

   Check_Diag_Selftest_Data();
   ClrLed( LED_ALL );

   if ( AL_probe_tm > BAD_TIME )
   {
      AL_probe_tm = 0;
   }
   if ( BBList_probe_tm > BAD_TIME )
   {
      BBList_probe_tm = 0;
   }
   if ( Cnf_probe_tm > BAD_TIME )
   {
      Cnf_probe_tm = 0;
   }
   if ( Fbd_probe_tm >BAD_TIME )
   {
      Fbd_probe_tm = 0;
   }

   FareDisplay();                                 /* validate fare set */
   return E;

}

/*----------------------------------------------\
|  Function:   ClrMlEv(int all)                 |
|  Purpose :   Clear masterlist & events.       |
|  Synopsis:                                    |
|  Input   :   flag if some or all data cleared |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
int   ClrMlEv( int all )
{
   register unsigned long *p;
   int      i;

   if( all )
   {
      for( p = &MlEvSigniture1; p < &MlEvSigniture2; p++ ) *p = 0L;

      memset( (void*)&Mlist.System, 0, sizeof(Mlist.System) );
      memset( (void*)&Mlist.Maintenance, 0, sizeof(Mlist.Maintenance) );
      memset( (void*)&Mlist.Cumulative,  0, sizeof(Mlist.Cumulative) );
      CashboxCoin = CashboxRevenue = 0L; /* clear cashbox rev     */
      CashboxBill = 0;                   /* clear cashbox bill count   */
      CoinRejectCnt = 0;                 /*  clear reject coin count */
      Clr_Sflag( S_FULLCASHBOX );        /* clear full cashbox         */
      MaxEv = MAX_EV;
      AL_probe_tm     = 0;
      BBList_probe_tm = 0;
      Cnf_probe_tm    = 0;
      Fbd_probe_tm    = 0;

      memset( (char*)&RF_Data, 0, sizeof( RF_Data ));
      Clr_Sflag( S_BYPASS_P );        /* clear old bypass flag */
      Clr_S2flag( S2_OCU_LOCKOUT );
   }
   else
   {

      for( p = (ulong*)&Ml.diff_r; p < (ulong*)&Ml.fbx_v; p++ ) *p = 0L;
      for( p = (ulong*)&Ml.warm_c; p < (ulong*)&Ml.coin_t; p++ ) *p = 0L;
      for( p = (ulong*)&Ml.bill_c; p < (ulong*)&Ml.pass_t; p++ ) *p = 0L;

      memset( (void*)&Ev, 0, sizeof(EV) );      /* init Ev for next event */
      Ml.evnt_c = 0;

      Mlist.System.WarmStart = Ml.cold_c = 0;
      Mlist.System.ColdStart = Ml.warm_c = 0;
      memset( (void*)&Mlist.Maintenance, 0, sizeof(Mlist.Maintenance) );
      memset( (void*)&Mlist.System.Pennies, 0,
            (&Mlist.System.TB - &Mlist.System.Pennies)+1 );

      Mlist.System.Cnts[0][0] = 0;
      if ( AL_probe_tm > BAD_TIME )
      {
         AL_probe_tm = 0;
      }
      if ( BBList_probe_tm > BAD_TIME )
      {
         BBList_probe_tm = 0;
      }
      if ( Cnf_probe_tm > BAD_TIME )
      {
         Cnf_probe_tm = 0;
      }
      if ( Fbd_probe_tm >BAD_TIME )
      {
         Fbd_probe_tm = 0;
      }
   }

   time( &Ev.t );                      /* event time stamp               */

   Ml.bus_n = Mlist.Config.BusNum = FbxCnf.busno;
   Ml.fbx_n = Mlist.Config.FareBoxNum = FbxCnf.fbxno;

/** AAT - Taken care of in CheckCnf() function in Stat.c **
   Ml.fbx_v = Mlist.Config.FareBoxVer = Version;
   Ml.fbx_v += 4000;
/**/

   memset( &Unwanted, 0, sizeof(UNWANTED));
   Unwanted.Signiture = SIGNITURE;

   if ( !Tst_A2flag( A_STAND_ALONE ) )
   {
     if ( DefaultFareEnb && !Tst_Sflag(S_INVALIDFS))
     {
       i = Clist2.FSMask >>10 & 0x000f;
       if ( i < FS && !Dlist.FareTbl[i][0].attrib )
         FbxCnf.fareset   =  i;               /* init fareset  */
       else
         i = 0;
     }
     else
     {
       i = 0;
     }
   }

   if ( !Tst_A2flag( A_STAND_ALONE ))
      FbxCnf.fareset  =  i;
   FbxCnf.dir      = FbxCnf.AVL_Dir = 0;
   FbxCnf.route    =  0L;
   FbxCnf.run      =  0L;
   FbxCnf.driver     =  0L;
   FbxCnf.trip       =  0L;
   FbxCnf.stop       =  0L;
   FbxCnf.city       =  0L;
   FbxCnf.cut[0]     =  24;
   FbxCnf.cut[1]     =  0;
   FbxCnf.latitude   =  0L;
   FbxCnf.longitude  =  0L;
   FbxCnf.old_driver =  0L;
   FbxCnf.old_route  = 0L;
   FbxCnf.old_run      =  0L;
   FbxCnf.old_trip     =  0L;
   FbxCnf.hour_fare_cnt = 0;
   FbxCnf.WIPortReset = 0;
   FbxCnf.HastusNo    = FbxCnf.OldHastus = 0L;

   Ev.fs             =  FbxCnf.fareset+1;      /* fare set - 1 base*/

   BadBatteryDetect  =  0;                     /* init battery flag  */
   BadClock          =  0;                     /* int bad clock flag */
   ElecKeyFlag       =  0;                     /* disable Electronic key */

   Pass_Photo_ID = Prime_ID = Photo_ID_index = 0;

/*   if( all )
      Clr_Sflag( S_BYPASS_P );        /* clear old bypass flag */

   if( !Tst_Sflag( S_TRIMOFFLINE ) )
   {
      if( !Tst_Sflag( S_LOWSTOCK ))
         Set_Pflag( P_RESTOCK );        /* clear low trim stock flag */
      else
         Clr_Pflag( P_RESTOCK );        /* clear low trim stock flag */
   }

   Clr_Sflag( S_LOWSTOCK );           /* clear low trim stock flag */
   Clr_Pflag( P_BV_OOS_LOG );

   Init_BillReset();

   FareDisplay();                     /* validate fare set */
   DBreinit();                        /* init transactions */
   Driver_Rev  = 0;                   /* clear drivers revenue      */
   Magval      = 0;                   /* clear mag rev              */

   MlEvSigniture1 = MlEvSigniture2 = 0xfeedf00d;

   init_pbq( 1 );               /* flush pass back queue */

   Xfer_Key = MAX_TC;                 /* clear xfer pending */
   ClrLed( LED_ALL );
   if( Tst_Sflag( S_TRIMBYPASS ) && AUTO_TRIMBYP && !MANUAL_TRIM )
      TrimCmd( TRIMBYPASSOFF, NULL, 0 );
   memset( (char*)&J1708Diag, 0, sizeof(J1708DIAG) );

   if( all || ( SC_Diag.Signiture != SC_SIGNATURE ) )
     i = sizeof(SC_DIAG);
   else
     i = sizeof(SC_DIAG)-12;

   memset( &SC_Diag.ReadCount, 0, i );
   SC_Diag.Signiture = SC_SIGNATURE;

   IntializeBillDiagCounts();
   T_fareset_change( FbxCnf.fareset+1 );

   if ( OF12_REMOTE_CQ && !Tst_Aflag( A_CARDQUEST ) )
   {
      RemoteSendMemoryClear( 0, all );
      RemoteSendLogin( 0 );
      RemoteSendDateTime( 0 );
   }
   return 0;
}

/*----------------------------------------------\
|  Function:   MlEvPtrs()                       |
|  Purpose :   Get masterlist & events pointers.|
|  Synopsis:   MlEvPtrs( top, bot );            |
|  Input   :   char **top - address to recv ptr |
|              char **bot - address to recv ptr |
|  Output  :   long size of data block.         |
|  Comments:                                    |
\----------------------------------------------*/
long  MlEvPtrs( top, bot )
char          **top,
                   **bot;
{
   long  len;

   len = sizeof(Ml) + sizeof(Fs_v) + Ml.evnt_c * EV_SZ;

   if ( top != NULL )
      *top = (char*)&Ml;

   if ( bot != NULL )
      *bot = (char*)&Ml + len;

   return len;
}

/*----------------------------------------------\
|  Function:   NewEv()                          |
|  Purpose :   Start/store a new event.         |
|  Synopsis:                                    |
|  Input   :   int   t - event type.            |
|              long  n - event number.          |
|  Output  :   int   0 - OK, -1 - error(s)      |
|  Comments:                                    |
\----------------------------------------------*/
int   NewEv( t, n )
int          t;
long            n;
{
   int   E = -1;
/*
   if ( VER_4 )
   {
      if( MyAgency == AGENCY_GCRTA )
         MaxEv = 127;
      else
         MaxEv = 100;
   }
   else if ( MaxEv < 100 || MaxEv > MAX_EV )
*/
   MaxEv = MAX_EV;

   if ( Ml.evnt_c >= MAX_EV )                   /* space ? */
   {
      Ml.evnt_c = MAX_EV;                       /* clamp at max */
      goto function_end;
   }

   MapEv();                                     /* map EV to raw format*/
   Ml.evnt_c++;

   time( &Ev.t );                                /* event time stamp */
   Ev.type      = t;                             /* event type       */
   Ev.evnt_n    = n;                             /* event number     */
   Ev.fs        = FbxCnf.fareset+1;              /* fare set - 1 base*/
   Ev.driver_n  = FbxCnf.driver;                 /* driver no.       */
   Ev.dir       = FbxCnf.dir;
   Ev.route_n   = FbxCnf.route;
   Ev.run_n     = FbxCnf.run;
   if  ( OF9_HASTUS_TO_TRIP )
     Ev.trip_n    = FbxCnf.HastusNo;
   else
     Ev.trip_n    = FbxCnf.trip;
   Ev.stop_n    = FbxCnf.stop;
   Ev.city_n    = FbxCnf.city;
   Ev.cut_t[0]  = FbxCnf.cut[0];
   Ev.cut_t[1]  = FbxCnf.cut[1];
   FbxCnf.hour_fare_cnt = 0;

/*
   printf("Ev type %d\n", Ev.type );
 */
   if( Ev.type == Ev_DRIVER_NO || Ev.type == Ev_DRIVER_CRD )   /* new driver ?     */
   {
      if( FbxCnf.driver == 0L)
      {
         Ev.type  = Ev_DRIVER_OUT;
         if ( NO_LOG && !DriverLogOff )
         {
            Ev.dir          = FbxCnf.dir     =  FbxCnf.AVL_Dir = 0;
            Ev.route_n      = FbxCnf.route   =  0L;
            Ev.run_n        = FbxCnf.run     =  0L;
            FbxCnf.HastusNo = FbxCnf.OldHastus = 0L;
            Ev.trip_n      = FbxCnf.trip    =  0L;
            if ( !J1708 )
               Ev.stop_n   = FbxCnf.stop    =  0L;
            Ev.cut_t[0]    = FbxCnf.cut[0]  =  24;
            Ev.cut_t[1]    = FbxCnf.cut[1]  =  0;
         }
      }
      else if ( FbxCnf.driver == -1 )
      {
         Ev.driver_n = FbxCnf.driver = 0;    /* driver no.       */
         Ev.type     = Ev_BAD_DRIVER_NO;     /* save bad driver number event */
      }
   }

   E = 0;

   function_end:
   return E;
}


/*------------------------------------------------\
|  Function:   MapEv()                            |
|  Purpose :   Map current event to raw format.   |
|  Synopsis:                                      |
|  Input   :   none.                              |
|  Output  :   int   0 - OK, -1 - error(s)        |
|  Comments:   Map EV structure to raw format     |
|              as described below                 |
| old format saved date/time in packed bcd fortmat|
|  byte  0 date[2];     MMDD (binary)             |
|  byte  2 time[2];     hhmm (binary)             |
| new format saves in UNIX time_t                 |
|  ulong 0 time_t;      UNIX time saved           |
|  byte  4 dir_fs;      dir. / fareset (4-bit bin)|
|  byte  5 type;        event type                |
|  byte  6 nmbr[3];     event number (3-byte long)|
|  byte  9 route[3];    route number (3-byte long)|
|  byte 12 run[3];      run   number (3-byte long)|
|  byte 15 trip[3];     trip  number (3-byte long)|
|  byte 18 stop[3];     stop  number (3-byte long)|
|  byte 21 cut[2];      cut time hhmm (binary)    |
|  word 23 curr_r;      revenue                   |
|  word 25 estrev;      estimated revenue         |
|  word 27 magrev;      magnetic revenue          |
|  word 29 uncl_r;      dump revenue              |
|  byte 31 dump_c;      dump count                |
|  byte 32 fare_c;      full fare count           |
|  byte 33 key_c[KEYS]; key counts                |
|  byte 47 ttp_c[TTPS]; ttp counts                |
|  byte 95 spare[4];    spare                     |
|  byte 99 bill_c;      bill count                |
\------------------------------------------------*/
static
int   MapEv()
{
   byte  *p =  E[Ml.evnt_c];                  /* destination pointer */

/*  old time format
   struct tm   *c;
*/
   memset( p, 0, EV_SZ );                     /* init storage area */

/* time_t save in place if old time  in events */
   memcpy( p,  ((byte*)&Ev.t), 4 );  /*  event time      */
   Fs_v[2] = 1;                      /*  set flag to indicate time_t saved */

/*  old time format
   c = gmtime( &Ev.t );
   *(p)   = c->tm_mon+1;
   *(p+1) = c->tm_mday;
   *(p+2) = c->tm_hour;
   *(p+3) = c->tm_min;
*/
   *(p+4) = (( Ev.dir <<4 ) | Ev.fs );        /* dir/fare set */
   *(p+5) = Ev.type;                          /*  event type  */
   memcpy( p+6,  ((byte*)&Ev.evnt_n)+1, 3 );  /*  event no.   */
   memcpy( p+9,  ((byte*)&Ev.route_n)+1, 3 ); /*  rounte no.  */
   memcpy( p+12, ((byte*)&Ev.run_n)+1, 3 );   /*  run no.     */
   memcpy( p+15, ((byte*)&Ev.trip_n)+1, 3 );  /*  trip no.    */
   memcpy( p+18, ((byte*)&Ev.stop_n)+1, 3 );  /*  stop no.    */
   memcpy( p+21, ((byte*)&Ev.cut_t), 2 );     /*  cut time    */
   memcpy( p+23, ((byte*)&Ev.curr_r), 2 );    /*  curr rev    */
   memcpy( p+25, ((byte*)&Ev.est_r), 2 );     /*  est rev     */
   memcpy( p+27, ((byte*)&Ev.mag_r), 2 );     /*  mag rev     */
   memcpy( p+29, ((byte*)&Ev.uncl_r), 2 );    /*  unclass rev */
   *(p+31) = Ev.dump_c;                       /*  dump count  */

   memcpy( p+32, ((byte*)&Ev.fare_c), (&Ev.bill_c - &Ev.fare_c)+1 ); /* all other data */
   memset( (void*)&Ev, 0, sizeof(EV) );       /* init Ev for next event */

   if ( TRANS_Debug )
   {
     int  i;
     printf( "event :\n");
     for ( i = 0; i < EV_SZ; i++)
     {
        printf( " %02x", *(p+i));
        if ( i && ((i+1) % 25) == 0 )
          putchar('\n');
     }
     putchar('\n');
   }

   return 0;
}


/*----------------------------------------------\
|  Function:   EvCnt()                          |
|  Purpose :   Get the number of events stored. |
|  Synopsis:   EvCnt( void );                   |
|  Input   :   None.                            |
|  Output  :   return the number of events.     |
|  Comments:                                    |
\----------------------------------------------*/
int   EvCnt()
{
   return( Ml.evnt_c );
}

/*----------------------------------------------\
|  Function:   EvPtr()                          |
|  Purpose :   Get a pointer to event no. n     |
|  Synopsis:   EvPtr( int );                    |
|  Input   :   int - event number wanted        |
|  Output  :   return pointer to event n.       |
|  Comments:                                    |
\----------------------------------------------*/
EV   *EvPtr( n )
int          n;
{
   static   EV e;

   if ( n < 0 || Ml.evnt_c <= n ) return NULL;  /* check range */

   RawToEv( &e, n );

   return &e;
}

static
int   RawToEv( p, n )
EV            *p;
int               n;
{
/*   old time format */
/*   struct tm   *c;  */
/*   time_t       t;  */
/*   old time format */

   memset( (void*)p, 0, sizeof(EV) );        /* init buffer area */

/*   old time format */
/*   time( &t );                           /* fetch current time */
/*   c = gmtime( &t );                     */
/*   c->tm_sec  = 0;                       /* clear seconds              */
/*   c->tm_min  = E[n][3];                 /* fetch time/date from event */
/*   c->tm_hour = E[n][2];                  */
/*   c->tm_mday = E[n][1];                  */
/*   c->tm_mon  = E[n][0];                  */
/*   if( c->tm_mon == 0 || c->tm_mon > 12 ) */
/*     c->tm_mon = 1;                       */
/*   --c->tm_mon;                           /* zero base    */
/*   p->t       = mktime( c );              /* convert time */
/*   old time format */

/* new time format */
   p->t       = (long)( ((long)E[n][0]<<24) | ((long)E[n][1]<<16) |
                        ((long)E[n][2]<< 8) |  (long)E[n][3]  ); /* convert time */

   p->dir     = ( E[n][4]>>4 ) & 0xf;        /*  dir         */
   p->fs      = ( E[n][4] ) & 0xf;           /*  fareset     */
   p->type    = E[n][5];                     /*  event type  */
   p->dump_c  = E[n][31];                    /*  dump count  */

   /* convert 3 byte fields to longs ( route, run... ect ) */
   p->evnt_n  = (long)( ((long)E[n][6]<<16) | ((long)E[n][7]<<8) |
                      ((long)E[n][8]) );
   p->route_n = (long)( ((long)E[n][9]<<16) | ((long)E[n][10]<<8) |
                      ((long)E[n][11]) );
   p->run_n   = (long)( ((long)E[n][12]<<16) | ((long)E[n][13]<<8) |
                      ((long)E[n][14]) );
   p->trip_n  = (long)( ((long)E[n][15]<<16) | ((long)E[n][16]<<8) |
                      ((long)E[n][17]) );
   p->stop_n  = (long)( ((long)E[n][18]<<16) | ((long)E[n][19]<<8) |
                      ((long)E[n][20]) );

   /* fetch cut time */
   p->cut_t[0] = E[n][21];
   p->cut_t[1] = E[n][22];

   /* convert 2 byte rev fields (current rev mag rev... ect) */
   p->curr_r   = (word)( ((word)E[n][23]<<8) | ((word)E[n][24]) );
   p->est_r    = (word)( ((word)E[n][25]<<8) | ((word)E[n][26]) );
   p->mag_r    = (word)( ((word)E[n][27]<<8) | ((word)E[n][28]) );
   p->uncl_r   = (word)( ((word)E[n][29]<<8) | ((word)E[n][30]) );

   /* fetch all other data (key, ttp... ect)  */
   memcpy( (byte*)&p->fare_c, ((byte*)&E[n][32]), (&Ev.bill_c - &Ev.fare_c)+1 );

   return 0;
}

/*----------------------------------------------\
|  Function:   HourlyEv()                       |
|  Purpose :   store a timed event if data      |
|              non zero.                        |
|  Synopsis:                                    |
|  Input   :   none                             |
|  Output  :   int   0 - don't save event       |
|  Comments:   check if something happened to   |
|              see if event should be stored    |
\----------------------------------------------*/
int  HourlyEv( void )
{
/* int   i, E = 0; */

  int    E = 0;
  if ( MyAgency == AGENCY_AUSTIN )
  {
    E = TRUE;
  }
  else if( Ev.type == Ev_HOURLY_EVENT || Ev.type == Ev_PERIODIC )
  {
    if ( FbxCnf.hour_fare_cnt || (Ev.curr_r + Ev.est_r + Ev.mag_r + Ev.uncl_r) != 0 )
       E = TRUE;

/* replace with counter in CountFare()
    if( Ev.fare_c || ((Ev.curr_r + Ev.est_r + Ev.mag_r + Ev.uncl_r) != 0) )
      E = TRUE;
    else
    {
      for( i = 0; (i < KEYS) && (E == 0); i++ )
        if( Ev.key_c[i] )
          E = TRUE;
      if( E == 0 )
      {
        for( i = 0; (i < TTP) && (E == 0 ); i++ )
          if( Ev.ttp_c[i] )
            E = TRUE;
      }
      if( E == 0 )
      {
        for( i = 0; (i < 4) && (E == 0); i++ )
          if( Ev.spare[i] )
            E = TRUE;
      }
    }
*/
  }
  else
  {
    E = TRUE;
  }

  return( E );
}


byte  Event_Type( int n )
{
  long l;
  if ( n == Ml.evnt_c )
  {
     printf("Event no %d type %d number %ld\n", n, Ev.type , Ev.evnt_n );
     return Ev.type;
  }
  else if ( n < Ml.evnt_c )
  {
     l = (long) ( ((long)E[n][6]<<16) | ((long)E[n][7]<<8) | ((long)E[n][8]) );
     printf("Event no %d type %d number %ld\n", n, E[n][5], l );
     return E[n][5];
  }
  else
  {
     printf("unkown event type/number\n");
     return 255;
  }
}

