*-----------------------------------------------------------------------------+
*  Src File:   misc_v05.asm                                                   |
*  Authored:   02/23/95, tgh                                                  |
*  Function:   Misc. assembler routines.                                      |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   02/23/95, tgh  -  changed header and filename.                 |
*      1.01:   12/14/95, sjb  -  modify ShiftBits                             |
*      1.02:   03/19/97, sjb  -  add reverse bits in byte of array            |
*      1.03:   08/05/98, sjb  -  add new modual to convert 8 bit bytes to 5   |
*      1.04:   07/24/03, jks  -  Replaced ReverseBits & ReverseBitsInBytes    |
*                                with C functions which use a lookup table    |
*                                a lookup table in Card.c                     |
*                                Also removed the unused functions:           |
*                                ReverseBitsInByte & Byte8toByte5             |
*      1.05:   08/26/04, sjb  -  Add Memcpy which is a memcopy bye longs      |
*                                                                             |
*           Copyright (c) 1994 - 2003 GFI All Rights Reserved                 |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc

*---------------------------------------------------------------------------+
*                       Local Equates                                       |
*---------------------------------------------------------------------------+

*---------------------------------------------------------------------------+
*                       Local Macros                                        |
*---------------------------------------------------------------------------+
*       Copy n bytes
*       a0 -> source
*       a1 -> destination
*       d1 -  holds count
        _ncpy:          macro
                next\@: move.b  (a0)+,(a1)+     move to destination buffer
                        dbra    d1,next\@       update/check byte counter
                        endm



*---------------------------------------------------------------------------+
*                       Data Segment (local data)                           |
*---------------------------------------------------------------------------+
                section udata,,"data"           uninitialized data

*---------------------------------------------------------------------------+
*                       External References                                 |
*---------------------------------------------------------------------------+
* external data

* external functions


*---------------------------------------------------------------------------+
*                       Code Segment                                        |
*---------------------------------------------------------------------------+
                section S_Misc,,"code"

*---------------------------------------------------------------------------+
*   Function:   Shift all the bits in an array to the left.                 |
*   Synopsis:   int ShiftBits( char *d, int n );                            |
*   Input   :   char *s    - points to source buffer.                       |
*               int   n    - holds number of bytes.                         |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _ShiftBits
                link    a6,#0                   set up frame
                movem.l d1-d3/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a0               a0 -> source
                move.w  12(a6),d1               d1 -  number of bytes
                andi.l  #0000ffffh,d1           only word part
                cmpi.w  #0,d1                   check range
                ble.s   sb_err                  and check range

                adda.l  d1,a0                   a0 -> end of source buffer

                lsr.w   #1,d1                   divide by 2 and
                bcc.s   sb_loop_end              check for odd no. of bytes

                move.b  -(a0),d0                take care of odd byte
                lsl.b   #1,d0                   always shift in zero
                move.w  ccr,-(sp)               save carry status
                move.b  d0,(a0)                 put byte back
                move.w  (sp)+,ccr               restore carry status
                bra.s   sb_loop_end             

           sb_next:
                roxl.w  -(a0)                   shift word
           sb_loop_end:
                dbra    d1,sb_next              update/check byte counter

                clr.w   d0                      return ok
                bra.s   sb_exit                 exit

           sb_err:
                move.w  #-1,d0                  return error

        sb_exit:
                movem.l (sp)+,d1-d3/a0/a1       restore registers used
                unlk    a6                      restore frame
             rts                                return to caller

*---------------------------------------------------------------------------+
*   Function:   convert 8 bit bytes to 5 bit bytes                          |
*   Synopsis:   int Byte8_5( char *d, char *s, int n );                     |
*   Input   :   char *s    - points to source buffer.                       |
*               char *d    - points to destination buffer.                  |
*               int   n    - holds number of bytes (5 bit).                 |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _Byte8_5
                link    a6,#0                   set up frame
                movem.l d1-d3/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a1               a1 -> destination
                movea.l 12(a6),a0               a0 -> source
                move.w  16(a6),d1               d1 -  number of bytes
                andi.l  #0000ffffh,d1           only word part
                cmpi.w  #0,d1                   check range
                ble     byte_err                and check range

           next_byte:
                move.b  (a0)+,d2                fetch next byte
                move.b  (a0),d3                 fetch next byte

                subq.w  #1,d1
                ble     byte_done               check & branch if done
                roxr.b  #1,d3                   rotate data to carry
                roxr.b  #1,d2                   and combine
                roxr.b  #1,d3
                roxr.b  #1,d2
                lsr.b   #3,d2                   right justify
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 0
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0),d2                 fetch next byte
                roxr.b  #2,d2                   rotate data
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 1
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0)+,d2                fetch next byte
                move.b  (a0),d3                 fetch next byte
                roxl.b  #1,d2
                roxl.b  #1,d3
                andi.b  #1fh,d3                 mask off garbage
                move.b  d3,(a1)+                save byte 2
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0)+,d2                fetch next byte
                move.b  (a0),d3                 fetch next byte
                roxr.b  #1,d3                   rotate data to carry
                roxr.b  #1,d2                   and combine
                lsr.b   #3,d2                   right justify
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 3
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0),d2                 fetch next byte
                roxr.b  #1,d2
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 4
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0)+,d2                fetch next byte
                move.b  (a0),d3                 fetch next byte
                roxl.b  #1,d2                   and combine
                roxl.b  #1,d3                   rotate data to carry
                roxl.b  #1,d2                   and combine
                roxl.b  #1,d3                   rotate data to carry
                andi.b  #1fh,d3                 mask off garbage
                move.b  d3,(a1)+                save byte 5
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0)+,d2                fetch next byte
                roxr.b  #3,d2
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 6
                subq.w  #1,d1
                ble     byte_done               check & branch if done

                move.b  (a0),d2
                andi.b  #1fh,d2                 mask off garbage
                move.b  d2,(a1)+                save byte 7
                subq.w  #1,d1
                ble     byte_done               check & branch if done
                bra     next_byte

          byte_done:
                clr.w   d0                      return ok
                bra.s   exit_Byte8_5            exit

          byte_err:
                move.w  #-1,d0                  return error

          exit_Byte8_5:
                movem.l (sp)+,d1-d3/a0/a1       restore registers used
                unlk    a6                      restore frame
                rts                             return to caller


*---------------------------------------------------------------------------+
*   Function:   check odd parity on 4 bit byte with 5 bit being parity bit  |
*   Synopsis:   int ParityCheck( char *s, int n );                          |
*   Input   :   char *s    - points to source buffer.                       |
*               int   n    - holds number of bytes.                         |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:   n > 1                                                       |
*---------------------------------------------------------------------------+
        _fnct   _ParityCheck
                link    a6,#0                   set up frame
                movem.l d1-d3/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a0               a0 -> source
                move.w  12(a6),d1               d1 -  number of bytes
                subq.w  #1,d1                   sub 1 for loop count
                andi.l  #0000ffffh,d1           only word part
                cmpi.w  #0,d1                   check range
                ble.s   parity_err              and check range

          parity_next:
                move.b  (a0)+,d2                fetch byte
                moveq   #4,d3                   no of bits to count
                moveq   #0,d0                   hi bit counter

          parity_add:
                lsr.b   #1,d2                   shift bit to carry
                bcc     next_bit                hi/low ?
                addq.b  #1,d0                   hi bit, bump hi bit count
          next_bit:
                dbra    d3,parity_add           all bits counted ?

                lsr.b   #1,d0                   check odd parity
                bcc     parity_err              parity ok ?
                dbra    d1,parity_next          parity ok, last byte checked ?

          parity_done:
                clr.w   d0                      return ok
                bra.s   parity_exit             exit

          parity_err:
                move.w  #-1,d0                  return error


          parity_exit:
                movem.l (sp)+,d1-d3/a0/a1       restore registers used
                unlk    a6                      restore frame
             rts                                return to caller

*---------------------------------------------------------------------------+
*   Function:   Clear idata                                                 |
*   Synopsis:   ClearMem;                                                   |
*   Input   :                                                               |
*                                                                           |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _ClearMem
                link    a6,#0                   set up frame
                movem.l d1/a0,-(sp)             save registers used
                movea.l #100000h,a0

           clr_next:
                clr.l   (a0)+                   clear memory
                cmpa.l  #13f000h,a0
                bls     clr_next                 update/check byte counter

                movem.l (sp)+,d1/a0             restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   pack credit card                                            |
*   Synopsis:   CreditPak;                                                  |
*   Input   :   char *s    - points to source buffer.                       |
*               char *d    - points to destination buffer.                  |
*               int   n    - holds number of bytes (5 bit).                 |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+

        _fnct   _CreditPak

                link    a6,#0                   set up frame
                movem.l d1-d3/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a1               a1 -> destination
                movea.l 12(a6),a0               a0 -> source
                move.w  16(a6),d1               d1 -  number of bytes
                andi.l  #0000ffffh,d1           only word part
                cmpi.w  #0,d1                   check range
                ble     credit_err              and check range

           cr_next:
                move.b  (a0)+,d2                fetch hi byte byte
                lsl.b   #4,d2                   rotate data to upper nibble
                subq.w  #1,d1
                bne     cr_low                  update/check byte counter
                or.b    #15,d2
                move.b  d2,(a1)+                save it
                bra     credit_packed

            cr_low:
                move.b  (a0)+,d3                fetch low byte
                and.b   #15,d3                  toss upper nibble
                add.b   d3,d2                   packit
                move.b  d2,(a1)+                save it
                subq.w  #1,d1
                bne     cr_next              update/check byte counter

           credit_packed:
                clr.w   d0                      return ok
                bra.s   exit_CreditPak          exit

           credit_err:
                move.w  #-1,d0                  return error

           exit_CreditPak:
                movem.l (sp)+,d1-d3/a0/a1       restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Memcpy( *s, *d, n )                                         |
*   Synopsis:   memory copy longs                                           |
*   Input   :   char *s    - points to source buffer.                       |
*               char *d    - points to destination buffer.                  |
*               int   n    - holds number of bytes to copy.                 |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+

        _fnct   _Memcpy
                link    a6,#0                   set up frame
                movem.l d1-d3/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a1               a1 -> destination
                movea.l 12(a6),a0               a0 -> source
                move.l  16(a6),d1               d1 -  number of bytes
                move.l  #0,d3
                andi.l  #000fffffh,d1           limit size to about 1 meg
                cmpi.l  #0,d1                   check range
                beq     move_err                branch if no data
                move.l  d1,d3
                andi.l  #3,d3                   save remainder (1-3)
                lsr.l   #2,d1                   divide by 4
                cmpi.l  #0,d1                   any longs to save
                beq.b   leftovers               branch if no

           sav_next:
                move.l  (a0)+,(a1)+             save long
                subq.l  #1,d1
                bne.s   sav_next                update/check byte counter

           leftovers:
                cmpi.w  #0,d3
                beq.s   mem_moved               jump if all done

           save_byte:
                move.b  (a0)+,(a1)+             save byte
                subq.w  #1,d3
                bne.s   save_byte               update/check byte counter

           mem_moved:
                clr.w   d0                      return ok
                bra.s   exit_Memcpy             exit

           move_err:
                move.w  #-1,d0                  return error
           exit_Memcpy:
                movem.l (sp)+,d1-d3/a0/a1       restore registers used
                unlk    a6                      restore frame
           rts


*---------------------------------------------------------------------------+
*   Function:   StoDouble( *d, *s, n )                                      |
*   Synopsis:   finsh conversion of the ms 6 digits of a 16 digit ascii     |
*               numeric string to hex                                       |
*   Input   :   char  *s    - points to source buffer.                      |
*               ulong *d    - points to destination buffer.                 |
*               int   n    - holds number of bytes left to convert          |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+

        _fnct   _StoD

                link    a6,#0                   set up frame
                movem.l d1-d4/a0/a1,-(sp)       save registers used
                movea.l  8(a6),a1               a1 -> destination
                movea.l 12(a6),a0               a0 -> source
                move.w  16(a6),d1               d1 -  number of bytes
                move.l  (a1)+,d3                fetch hex data
                move.l  (a1),d2
                andi.w  #0000fh,d1              only byte part
                cmpi.w  #1,d1
                beq     ten_digits              ten billion
                cmpi.w  #2,d1
                beq     eleven_digits           100 billion
                cmpi.w  #3,d1
                beq     twelve_digits           trillion
                cmpi.w  #4,d1
                beq     thirteen_digits         10 trillian
                cmpi.w  #5,d1
                beq     fourteen_digits         100 trillian
                cmpi.w  #6,d1
                beq     fifteen_digits          quadrillion
                cmpi.w  #7,d1
                bne     cvt_err

* add 10 quadrillian
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_sixteen:
                beq     fifteen_digits
                addi.l  #0a4c68000h,d2
                bcc     hi_sixteen
                addi.l  #1,d3
           hi_sixteen:
                addi.l  #38d7eh,d3
                subq.w  #1,d4
                bra     next_sixteen

* add 1 quadrillion
           fifteen_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_fifteen:
                beq     fourteen_digits
                addi.l  #107a4000h,d2
                bcc     hi_fifteen
                addi.l  #1,d3
           hi_fifteen:
                addi.l  #5af3h,d3
                subq.w  #1,d4
                bra     next_fifteen

* add 100 trillion
           fourteen_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_fourteen:
                beq     thirteen_digits
                addi.l  #4e72a000h,d2
                bcc     hi_fourteen
                addi.l  #1,d3
           hi_fourteen:
                addi.l  #918h,d3
                subq.w  #1,d4
                bra     next_fourteen

* add 10 trillion
           thirteen_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_thirteen:
                beq     twelve_digits
                addi.l  #0d4a51000h,d2
                bcc     hi_thirteen
                addi.l  #1,d3
           hi_thirteen:
                addi.l  #0e8h,d3
                subq.w  #1,d4
                bra     next_thirteen

* add 1 trillion
           twelve_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_twelve:
                beq     eleven_digits
                addi.l  #4876e800h,d2
                bcc     hi_twelve
                addi.l  #1,d3
           hi_twelve:
                addi.l  #17h,d3
                subq.w  #1,d4
                bra     next_twelve

* add 100 billion
           eleven_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_eleven:
                beq     ten_digits
                addi.l  #540be400h,d2
                bcc     hi_eleven
                addi.l  #1,d3
           hi_eleven:
                addi.l  #2,d3
                subq.w  #1,d4
                bra     next_eleven

* add 10 billion
           ten_digits:
                move.b  (a0)+,d4
                andi.b  #0000fh,d4              only byte part
           next_tenth:
                beq     exit_StoD
                addi.l  #3b9aca00h,d2
                bcc     no_10_carry
                addi.l  #1,d3
           no_10_carry:
                subq.w  #1,d4
                bra     next_tenth


           cvt_err:
                move.w  #-1,d0                  return error
           exit_StoD:
                move.l  d2,(a1)                 save resultts
                move.l  d3,-(a1)
                movem.l (sp)+,d1-d4/a0/a1       restore registers used
                unlk    a6                      restore frame

         rts

